import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LoadingStore {
	private readonly isLoading = new BehaviorSubject(false);
	isLoading$ = this.isLoading.asObservable();

	showLoading() {
		this.isLoading.next(true);
	}

	hideLoading() {
		this.isLoading.next(false);
	}
}
