import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class SearchStore {
	private schoolNameToSearch = new BehaviorSubject('');
	schoolNameToSearch$ = this.schoolNameToSearch.asObservable();

	private locationSearch = new BehaviorSubject({} as { provinceCode: string; districtCode: string });
	locationSearch$ = this.locationSearch.asObservable();

	setSearchSchoolNameValue(value: string) {
		this.schoolNameToSearch.next(value);
	}

	setLocationSearchValue(value: { provinceCode: string; districtCode: string }) {
		this.locationSearch.next(value);
	}
}
