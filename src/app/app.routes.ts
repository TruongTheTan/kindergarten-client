import { Routes } from '@angular/router';
import { authRoutes } from './routers/auth.routes';
import { contentRoutes } from './routers/content.routes';
import { publicRoutes } from './routers/public.routes';

export const routes: Routes = [
	{
		path: '',
		redirectTo: '/public/home', // setup default route path when run app here
		pathMatch: 'full'
	},
	...contentRoutes,
	...publicRoutes,
	...authRoutes,
	{
		path: '**',
		loadComponent: () =>
			import('./shared/components/not-found/not-found.component').then((c) => c.NotFoundComponent)
	}
];
