import { Injectable } from '@angular/core';
import { jwtDecode } from 'jwt-decode';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
	providedIn: 'root'
})
export class AuthenticationUtils {
	private expiration = 0;
	private readonly domain = '/';

	private readonly decodedTokenToRoleURL = 'http://schemas.microsoft.com/ws/2008/06/identity/claims/role';
	private readonly decodedTokenToIdURL = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier';

	constructor(private cookieService: CookieService) {}

	/************************************************* JWT Token *************************************/
	getTokenFromCookie() {
		return this.cookieService?.get('token');
	}

	setTokenToCookie(token: string) {
		this.expiration = jwtDecode(token).exp ?? 0;
		this.cookieService?.set('token', token, this.expiration, this.domain, '', false);

		this.setUserIdToCookie(this.getUserIdFromCookie());
		this.setRoleToCookie(this.getRoleFromCookie());
	}

	/************************************************* User Id *************************************/
	getUserIdFromCookie(): string {
		const token = this.getTokenFromCookie();

		if (token) {
			const decodedToken: any = jwtDecode(token);
			return decodedToken.role || decodedToken[this.decodedTokenToIdURL];
		}
		return '';
	}

	setUserIdToCookie(userId: string) {
		this.cookieService?.set('userId', userId, this.expiration, this.domain, '', false);
	}

	/************************************************* Role *************************************/
	getRoleFromCookie(): string {
		const token = this.getTokenFromCookie();

		if (token) {
			const decodedToken: any = jwtDecode(token);
			return decodedToken.role || decodedToken[this.decodedTokenToRoleURL];
		}
		return '';
	}

	setRoleToCookie(role: string) {
		this.cookieService?.set('role', role, this.expiration, this.domain, '', false);
	}

	/************************************************* Full name *************************************/
	getFullNameFromCookie() {
		return this.cookieService?.get('fullName');
	}

	setFullNameToCookie(fullName: string) {
		this.cookieService?.set('fullName', fullName, this.expiration, this.domain, '', false);
	}

	removeAllCookie() {
		this.cookieService?.deleteAll(this.domain);
		localStorage.removeItem('avatar');
	}

	checkTokenIsExpired() {
		const token = this.getTokenFromCookie();
		if (token) {
			const decoded = jwtDecode(token);

			const expirationDate = new Date(0);
			expirationDate.setUTCHours(decoded.exp ?? 0);

			return expirationDate === null ? true : expirationDate < new Date();
		}
		return false;
	}
}
