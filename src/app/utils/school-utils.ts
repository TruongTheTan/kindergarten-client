export class SchoolUtils {
	static getStatusColor(statusName: string) {
		switch (statusName) {
			case 'Published':
				return 'badge badge-success';

			case 'Approved':
				return 'badge badge-warning';

			case 'Deleted':
			case 'Rejected':
				return 'badge badge-danger';

			default:
				return 'badge badge-secondary';
		}
	}

	static getCheckedFacilities(facilityName: string, facilities: string[]) {
		return facilities?.includes(facilityName);
	}

	static getCheckedUtilities(utilitiesName: string, utilities: string[]) {
		return utilities?.includes(utilitiesName);
	}
}
