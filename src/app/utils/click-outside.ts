import { Directive, ElementRef, EventEmitter, Output, Renderer2, OnDestroy, AfterViewInit } from '@angular/core';

@Directive({
	selector: '[clickOutside]',
	standalone: true
})
export class ClickOutsideDirective implements AfterViewInit, OnDestroy {
	@Output() clickOutside = new EventEmitter<void>();
	private unlistener: (() => void) | undefined;

	constructor(private el: ElementRef, private renderer: Renderer2) {}

	ngAfterViewInit() {
		this.unlistener = this.renderer.listen('document', 'click', (event: Event) => {
			const targetElement = event.target as HTMLElement;
			const isClickedInside = this.el.nativeElement.contains(targetElement);
			if (!isClickedInside) {
				this.clickOutside.emit();
			}
		});
	}

	ngOnDestroy() {
		if (this.unlistener) {
			this.unlistener();
		}
	}
}
