import { AbstractControl, ValidatorFn } from '@angular/forms';

export class CustomFormValidation {
	static feeRangeValidator(): ValidatorFn {
		return (controls: AbstractControl) => {
			const feeFrom = controls.get('feeFrom')!;
			const feeTo = controls.get('feeTo')!;

			if (feeTo?.errors && !feeTo.errors['feeRange']) {
				return null;
			}

			if (+feeFrom.value >= +feeTo.value) {
				return { feeRange: true };
			}

			return null;
		};
	}
}
