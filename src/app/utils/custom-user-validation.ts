import { AbstractControl, ValidatorFn } from '@angular/forms';

export class CustomUserFormValidation {
	static checkRepassword(): ValidatorFn {
		return (controls: AbstractControl) => {
			const password = controls.get('password')!;
			const rePassword = controls.get('rePassword')!;

			if (rePassword?.errors && !rePassword.errors['rePassword']) {
				return null;
			}

			if (password.value !== rePassword.value) {
				return {
					rePassword: true,
				};
			}
			return null;
		};
	}
	static CheckUserName() {}
}
