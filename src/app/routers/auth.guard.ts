import { inject } from '@angular/core';
import { CanActivateChildFn, CanActivateFn, Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';
import { map } from 'rxjs';
import { UserService } from '../services/user.service';
import { AuthenticationUtils } from './../utils/authentication-utils';

const roles = {
	admin: 'Admin',
	schoolOwner: 'SchoolOwner',
	parent: 'Parents',
	superAdmin: 'SuperAdmin'
};

export const authenticationGuard: CanActivateFn = (route, state) => {
	const authenticationUtils = inject(AuthenticationUtils);
	const token = authenticationUtils.getTokenFromCookie();
	const userRole = authenticationUtils.getRoleFromCookie();

	if (!token || !userRole) {
		showToastAndRedirect('Un-authenticated', 'Need login to continue', '/auth/login');
		return false;
	}
	return true;
};

export const parentAndGuestGuard: CanActivateFn = (route, state) => {
	const authenticationUtils = inject(AuthenticationUtils);
	const role = authenticationUtils.getRoleFromCookie();

	if (isRole(role, [roles.parent, null, undefined, ''])) {
		return true;
	}
	showToastAndRedirect('Parent and guest access only', 'You do not have permission to continue !');
	return false;
};

export const parentAndGuestChildGuard: CanActivateChildFn = (route, state) => {
	return parentAndGuestGuard(route, state);
};

export const parentGuard: CanActivateFn = (route, state) => {
	return checkRoleAndNotify([roles.parent], 'Un-authorized (Parent access only)');
};

export const adminGuard: CanActivateFn = (route, state) => {
	return checkRoleAndNotify([roles.admin, roles.superAdmin], 'Un-authorized (Admin access only)');
};

export const schoolOwnerGuard: CanActivateFn = (route, state) => {
	return checkRoleAndNotify([roles.schoolOwner], 'Un-authorized (School owner access only)');
};

export const AdminAndSchoolOwnerGuard: CanActivateFn = (route, state) => {
	return checkRoleAndNotify(
		[roles.admin, roles.superAdmin, roles.schoolOwner],
		'Un-authorized (Admin and school owner access only)'
	);
};

export const parentChildGuard: CanActivateChildFn = (route, state) => {
	return checkRoleAndNotify([roles.parent], 'Un-authorized (Parent access only)');
};

export const AdminAndSchoolOwnerChildGuard: CanActivateChildFn = (route, state) => {
	return checkRoleAndNotify(
		[roles.admin, roles.superAdmin, roles.schoolOwner],
		'Un-authorized (Admin and school owner access only)'
	);
};

/**  Check if an admin is trying to access user edit page of other admin  **/
export const editAdminProfileGuard: CanActivateFn = (route, state) => {
	const toast = inject(NgToastService);
	const userId = route.queryParamMap.get('id')!;
	const role = inject(AuthenticationUtils).getRoleFromCookie();

	return inject(UserService)
		.getUserById(userId)
		.pipe(
			map((user) => {
				if (user.roleName === roles.admin && role === roles.admin) {
					toast.warning({
						detail: 'Not allowed',
						summary: 'An admin can not edit other admin',
						duration: 3000
					});

					setTimeout(() => window.history.back(), 1500);
					return false;
				}
				return true;
			})
		);
};

function checkRoleAndNotify(requiredRoles: string[], detail: string): boolean {
	const authenticationUtils = inject(AuthenticationUtils);
	const role = authenticationUtils.getRoleFromCookie();

	if (!role || !requiredRoles.includes(role)) {
		showToastAndRedirect(detail, 'You do not have permission to continue !');
		return false;
	}
	return true;
}

function isRole(role: string, validRoles: (string | null | undefined)[]): boolean {
	return validRoles.includes(role);
}

function showToastAndRedirect(detail: string, summary: string, redirectUrl?: string) {
	const router = inject(Router);
	const toast = inject(NgToastService);
	const role = inject(AuthenticationUtils).getRoleFromCookie();

	if (redirectUrl) {
		toast.warning({
			detail: detail,
			summary: summary,
			duration: 3000
		});

		setTimeout(() => router.navigateByUrl(redirectUrl), 2000);
	} else {
		// Is open new tab (when first run the app and navigate to default route)
		const isNewTab = window.history.length === 1 || window.history.length === 2;

		if (isNewTab && isRole(role, [roles.schoolOwner, roles.admin, roles.superAdmin])) {
			router.navigateByUrl('/content/school/list');
		} else {
			toast.warning({
				detail: detail,
				summary: summary,
				duration: 3000
			});

			setTimeout(() => window.history.back(), 1500);
		}
	}
}
