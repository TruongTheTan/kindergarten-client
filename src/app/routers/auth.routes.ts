import { Routes } from '@angular/router';

export const authRoutes: Routes = [
	{
		path: 'auth',
		loadComponent: () => import('../auth/layout/layout.component').then((c) => c.LayoutComponent),
		children: [
			{
				path: 'login',
				loadComponent: () => import('../auth/pages/login/login.component').then((c) => c.LoginComponent)
			},
			{
				path: 'forgot-password',
				loadComponent: () =>
					import('../auth/pages/forgot-password/forgot-password.component').then(
						(c) => c.ForgotPasswordComponent
					)
			},
			{
				path: 'reset-password',
				loadComponent: () =>
					import('../auth/pages/reset-password/reset-password.component').then(
						(c) => c.ResetPasswordComponent
					)
			},
			{
				path: 'register',
				loadComponent: () =>
					import('../auth/pages/register/register.component').then((c) => c.RegisterComponent)
			},
			{
				path: 'confirm-email/:userId/:code',
				loadComponent: () =>
					import('../shared/components/verify-email/verify-email.component').then(
						(c) => c.VerifyEmailComponent
					)
			}
		]
	}
];
