import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot } from '@angular/router';
import { GetPublicSchoolRequest } from '../models/schools/public-get-school-request';
import { SchoolService } from '../services/school.service';
import { UserService } from '../services/user.service';
import { SearchStore } from './../../store/search-store';

export const schoolDetailPublicResolver: ResolveFn<unknown> = (
	route: ActivatedRouteSnapshot,
	state: RouterStateSnapshot
) => inject(SchoolService).getDetailSchoolById(route.queryParamMap.get('id')!, true);

export const schoolDetailContentResolver: ResolveFn<unknown> = (
	route: ActivatedRouteSnapshot,
	state: RouterStateSnapshot
) => inject(SchoolService).getDetailSchoolById(route.queryParamMap.get('id')!, false);

export const schoolListResolver: ResolveFn<unknown> = (
	route: ActivatedRouteSnapshot,
	state: RouterStateSnapshot
) => inject(SchoolService).getAllSchool('', 5, 1);

export const getPublishedSchoolResolver: ResolveFn<unknown> = (
	route: ActivatedRouteSnapshot,
	state: RouterStateSnapshot
) => {
	const searchStore = inject(SearchStore);
	const getRequest = { pageIndex: 1, pageSize: 4 } as GetPublicSchoolRequest;

	const subName = searchStore.schoolNameToSearch$.subscribe((schoolName) => {
		if (schoolName) {
			getRequest.schoolName = schoolName;
		}
	});

	const subLocation = searchStore.locationSearch$.subscribe((data) => {
		const provinceCode = data.provinceCode;

		if (provinceCode) {
			getRequest.provinceId = provinceCode;
			getRequest.districtId = data.districtCode;
		}
	});

	inject(SchoolService).getPublicSchool(getRequest);

	return () => {
		subName.unsubscribe();
		subLocation.unsubscribe();
	};
};

export const userDetailResolver: ResolveFn<unknown> = (
	route: ActivatedRouteSnapshot,
	state: RouterStateSnapshot
) => inject(UserService).getUserById(route.paramMap.get('id')!);
