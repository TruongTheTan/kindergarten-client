import { Routes } from '@angular/router';
import {
	AdminAndSchoolOwnerChildGuard,
	AdminAndSchoolOwnerGuard,
	adminGuard,
	authenticationGuard,
	editAdminProfileGuard
} from './auth.guard';
import { schoolDetailContentResolver, schoolListResolver, userDetailResolver } from './resolvers.resolver';

export const contentRoutes: Routes = [
	{
		path: 'content',
		loadComponent: () => import('../content/layout/layout.component').then((c) => c.LayoutComponent),
		canActivate: [authenticationGuard, AdminAndSchoolOwnerGuard],
		canActivateChild: [AdminAndSchoolOwnerChildGuard],
		children: [
			{
				path: 'school',
				children: [
					{
						path: '',
						redirectTo: 'list',
						pathMatch: 'full'
					},
					{
						path: 'list',
						resolve: [schoolListResolver],
						loadComponent: () =>
							import('../content/pages/school/school-list/school-list.component').then(
								(c) => c.SchoolListComponent
							)
					},
					{
						path: 'create',
						loadComponent: () =>
							import('../content/pages/school/create-school/create-school.component').then(
								(c) => c.CreateSchoolComponent
							)
					},
					{
						path: 'edit',
						loadComponent: () =>
							import('../content/pages/school/edit-school/edit-school.component').then(
								(c) => c.EditSchoolComponent
							)
					},
					{
						path: 'detail',
						resolve: [schoolDetailContentResolver],
						loadComponent: () =>
							import('../content/pages/school/school-detail/school-detail.component').then(
								(c) => c.SchoolDetailComponent
							)
					},
					{
						path: 'reminders',
						loadComponent: () =>
							import(
								'../content/pages/school/school-review-reminder/school-review-reminder.component'
							).then((c) => c.SchoolReviewReminderComponent),
						canActivate: [adminGuard]
					},
					{
						path: 'reminders/detail',
						resolve: [schoolDetailContentResolver],
						loadComponent: () =>
							import('../content/pages/school/school-detail/school-detail.component').then(
								(c) => c.SchoolDetailComponent
							),
						canActivate: [adminGuard]
					},
					{
						path: 'rating',
						loadComponent: () =>
							import('../content/pages/school/rating/rating-school.component').then(
								(c) => c.RatingSchoolComponent
							)
					}
				]
			},
			{
				path: 'school-request',
				children: [
					{
						path: 'list',
						loadComponent: () =>
							import('../content/pages/school-request/request-list/request-list.component').then(
								(c) => c.RequestListComponent
							)
					},
					{
						path: 'detail',
						loadComponent: () =>
							import('../content/pages/school-request/request-detail/request-detail.component').then(
								(c) => c.RequestDetailComponent
							)
					}
				]
			},
			{
				path: 'counseling-request',
				children: [
					{
						path: 'list',
						loadComponent: () =>
							import(
								'../content/pages/school-request/request-counseling-list/request-counseling-list.component'
							).then((c) => c.RequestCounselingListComponent)
					},
					{
						path: 'detail/:id',
						loadComponent: () =>
							import('../content/pages/school-request/request-detail/request-detail.component').then(
								(c) => c.RequestDetailComponent
							)
					}
				]
			},
			{
				path: 'users',
				children: [
					{
						path: '',
						redirectTo: 'list',
						pathMatch: 'full'
					},
					{
						path: 'list',
						loadComponent: () =>
							import('../content/pages/user/list-user/list-user.component').then(
								(c) => c.ListUserComponent
							),
						canActivate: [adminGuard]
					},
					{
						path: 'create',
						loadComponent: () =>
							import('../content/pages/user/create-user/create-user.component').then(
								(c) => c.CreateUserComponent
							),
						canActivate: [adminGuard]
					},
					{
						path: 'edit',
						loadComponent: () =>
							import('../content/pages/user/edit-user/edit-user.component').then(
								(c) => c.EditUserComponent
							),
						canActivate: [adminGuard, editAdminProfileGuard]
					},
					{
						path: 'detail/:id',
						resolve: [userDetailResolver],
						loadComponent: () =>
							import('../content/pages/user/details-user/details-user.component').then(
								(c) => c.DetailsUserComponent
							),
						canActivate: [AdminAndSchoolOwnerGuard]
					}
				]
			},
			{
				path: 'parent',
				children: [
					{
						path: '',
						redirectTo: 'list',
						pathMatch: 'full'
					},
					{
						path: 'list',
						loadComponent: () =>
							import('../content/pages/parent/parent-list/parent-list.component').then(
								(c) => c.ParentListComponent
							)
					},
					{
						path: 'detail/:id',
						resolve: [userDetailResolver],
						loadComponent: () =>
							import('../content/pages/user/details-user/details-user.component').then(
								(c) => c.DetailsUserComponent
							),
						canActivate: [AdminAndSchoolOwnerGuard]
					}
				]
			}
		]
	}
];
