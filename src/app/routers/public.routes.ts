import { Routes } from '@angular/router';
import { LayoutComponent } from '../public/layout/layout.component';
import {
	authenticationGuard,
	parentAndGuestChildGuard,
	parentAndGuestGuard,
	parentChildGuard,
	parentGuard
} from './auth.guard';
import { getPublishedSchoolResolver, schoolDetailPublicResolver } from './resolvers.resolver';

export const publicRoutes: Routes = [
	{
		path: 'public',
		component: LayoutComponent,
		canActivate: [parentAndGuestGuard],
		canActivateChild: [parentAndGuestChildGuard],
		children: [
			{
				path: 'school',
				children: [
					{
						path: 'list-school',
						resolve: [getPublishedSchoolResolver],
						loadComponent: () =>
							import('./../public/pages/school/list-school/list-school.component').then(
								(c) => c.ListSchoolComponent
							)
					},
					{
						path: 'detail',
						resolve: [schoolDetailPublicResolver],
						loadComponent: () =>
							import('./../public/pages/school/school-detail/school-detail.component').then(
								(c) => c.SchoolDetailComponent
							)
					},
					{
						path: 'rating/:id',
						loadComponent: () =>
							import('../public/pages/school/school-rating/school-rating.component').then(
								(c) => c.SchoolRatingComponent
							),
						canActivate: [authenticationGuard, parentGuard]
					}
				]
			},
			{
				path: 'user',
				canActivate: [authenticationGuard, parentGuard],
				canActivateChild: [parentChildGuard],
				children: [
					{
						path: 'my-request',
						loadComponent: () =>
							import('../public/pages/user/my-requests/my-requests.component').then(
								(c) => c.MyRequestsComponent
							)
					},
					{
						path: 'my-profile',
						loadComponent: () =>
							import('../public/pages/user/my-profile/my-profile.component').then(
								(c) => c.MyProfileComponent
							)
					}
				]
			},
			{
				path: 'home',
				loadComponent: () =>
					import('../public/pages/home-page/home-page.component').then((c) => c.HomePageComponent)
			}
		]
	}
];
