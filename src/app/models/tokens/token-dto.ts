export interface TokenDto {
	userId: string;
	token: string;
	refreshToken: string;
	name: string;
	avatar: string;
}
