export interface NotificationDto {
	id: string;
	receiverId: string;
	senderId: string;
	requestId: string;
	title: string;
	isOpen: boolean;
	content: string;
	date: Date;
	showOptions?: boolean;
}
