export interface SchoolMasterData {
	id?: string;
	name?: string;
}

export const childReceivingAge = [
	{ id: 'A4492E54-AC55-44A5-8876-97B8515E062A', name: '6 Month - 1 Year' },
	{ id: '91654D6C-CA93-4660-B3E5-D360EA2DAC70', name: '1 Year - 3 Year' },
	{ id: '9E973AFB-7182-44DA-B90A-4EF5C0A66BFB', name: '3 Year - 6 Year' }
] as SchoolMasterData[];

export const schoolUtilities = [
	{ id: '19b398e1-f2c6-4a63-b26f-422b33968e87', name: 'Breakfast' },
	{ id: '3c3a672e-8874-4208-9f9c-9427df94cc82', name: 'E-Contact book' },
	{ id: '3e2185cb-5336-4756-b8c3-139a00214191', name: 'Health check' },
	{ id: '847909c0-a688-4a4f-966e-8f9f0fdf678c', name: 'School bus' },
	{
		id: '84fa05d9-60b1-41c2-b48b-dbccb2f6802d',
		name: 'Picnic activities'
	},
	{
		id: 'b32d649c-5fbb-4d2d-b373-ef68a2f5d0ae',
		name: 'After shool care'
	},
	{ id: 'e520dac9-a0a1-4286-8b71-d4f6240684f2', name: 'Saturday class' }
] as SchoolMasterData[];

export const schoolFacilities = [
	{ id: '06e3c3f0-1398-40e9-84df-3e3b0fad4148', name: 'PE room' },
	{ id: '0ae324c2-b646-4718-a02d-28ef728a6c62', name: 'Library' },
	{ id: '2d3176df-d8e6-487f-9912-18ff8b3186f4', name: 'STEM room' },
	{
		id: '2ef94689-dc83-467f-a6b0-a4b0e5750135',
		name: 'Outdoor playground'
	},
	{ id: '3af81412-d837-4999-ab1f-973474a91c2e', name: 'Swimming pool' },
	{ id: '91625af9-cbdb-4491-aea9-019a8fede448', name: 'Cafeteria' },
	{ id: 'da87f9c5-3cb1-484c-ab8c-f5da47346430', name: 'Camera' },
	{ id: 'ff316f75-4288-4252-8cde-3e71617bc896', name: 'Musical room' }
] as SchoolMasterData[];

export const schoolEducationMethods = [
	{ id: '0f6fc771-0831-479e-be9a-192829a9aa02', name: 'Steiner' },
	{ id: '58887de3-d1cb-47e6-abea-e33edf4a941d', name: 'High Scope' },
	{ id: '8d16fcda-ecef-4457-8215-5395ba8c5900', name: 'STEM' },
	{ id: 'def79c68-9418-4b4e-93e5-f35d3f7fcaf4', name: 'Glenn Doman' },
	{ id: 'df4d94e9-4442-4799-90f5-ca024b41b590', name: 'Shichida' },
	{ id: 'e3aa0851-7e09-4536-a7c4-5c85bf105276', name: 'Montessori' },
	{ id: 'e80f1539-627b-4652-a526-b6c5f5e9f233', name: 'Reggio Emilia' }
] as SchoolMasterData[];

export const schoolLanguages = [
	{ id: '21c2ea97-c5a8-4896-9944-20a0a46de4eb', name: 'Vietnamese' },
	{ id: 'a81b9be2-6c53-4fb7-ada9-89e1c053840c', name: 'English' }
] as SchoolMasterData[];

export const schoolTypes = [
	{ id: '0a98ea91-41db-43f5-815a-36a14bb7c2fb', name: 'Private' },
	{
		id: '6410151e-6cfa-4509-bc91-4e431d42933e',
		name: 'International Bilingual'
	},
	{ id: '8dfefa08-8fe0-4402-b9af-a33b706bebd2', name: 'Semi-public' },
	{ id: '9dfc5503-4393-4a39-a4bc-cd857bae8b6f', name: 'International' },
	{ id: 'de7ec505-b3b1-4154-bed3-4b8d4fffe3a0', name: 'Public' }
] as SchoolMasterData[];

export const schoolStatus = [
	{ id: '121316bd-4095-482e-958c-db79b42664d1', name: 'Unpublished' },
	{ id: '25e70cf8-6c9c-437d-a20a-1fd177e327e3', name: 'Deleted' },
	{ id: '2fe19b75-f64e-4201-a7f8-a8caf847b5fc', name: 'Published' },
	{ id: '49c4e382-655c-452e-a93d-9695d9523eca', name: 'Saved' },
	{ id: '712e4edc-961b-4684-b4d1-da1f244139ec', name: 'Approved' },
	{ id: 'd0df3dfc-85f2-4b06-b94f-6d8ec1e4932a', name: 'Submitted' },
	{ id: 'e212f97c-84e5-4c2f-9dd4-6c5018acc3ee', name: 'Rejected' }
] as SchoolMasterData[];
