export interface PageResult<T> {
	items?: T[];
	pageIndex?: number;
	pageSize?: number;
	totalRecords?: number;
	pageCount?: number;
}
