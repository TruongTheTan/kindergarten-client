export interface updateUseRequest {
	id: string;
	fullName: string;
	dob?: string;
	address: string;
	gender: number;
	phoneNumber: string;
	isActive: boolean;
	image?: File;
}
