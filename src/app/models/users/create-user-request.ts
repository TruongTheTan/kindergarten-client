export interface CreateUseRequest {
	fullName: string;
	dob?: 'dd/mm/yyyy';
	address: string;
	gender: number;
	email: string;
	phoneNumber: string;
	password: string;
	isActive: boolean;
	roleId?: string;
	image?: File;
}
