export interface UserDto {
	id: string;
	roleId: string;
	fullName: string;
	isActive: boolean;
	gender: number;
	dob?: string | null;
	enrollSchoolId?: string;
	createAt?: Date;
	updateAt?: Date;
	isDeleted: boolean;
	roleName: string;
	email: string;
	phoneNumber: string;
	userName: string;
	address: string;
	emailConfirmed: boolean;
	avatar?: Uint8Array;
}

export interface ExtendedUserDTO extends UserDto {
	userId?: string;
	schoolId?: string;
	enrolledSchool?: string;
}
