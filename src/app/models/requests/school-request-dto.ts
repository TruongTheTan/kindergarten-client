export interface SchoolRequestDto {
	id: string;
	fullName: string;
	email: string;
	phone: string;
	address?: string;
	content: string;
	response?: string;
	createAt?: Date;
	updateAt?: Date;
	userId?: string;
	schoolId: string;
	resolveCount: number;
	schoolRequestCategoryId: string;
	schoolRequestStatusId: string;
	user?: AppUserRequestDTO;
	school: RequestedSchoolDTO;
	selected?: boolean;
}

interface AppUserRequestDTO {
    id: string;
    fullName: string;
    isActive: boolean;
    gender: GenderEnums;
    dob?: Date;
    enrollSchoolId?: string;
    createAt?: Date;
    updateAt?: Date;
    isDeleted: boolean;
    roleName: string;
}

export interface RequestedSchoolDTO {
	id: string;
	schoolName: string;
	email: string;
	phone: string;
	feeFrom: number;
	feeTo: number;
	childSchoolReceiveAge?:string;
	schoolIntroduce?: string;
	specificAddress?: string;
	website?: string;
	rating: number;
}

export interface ICreateSchoolRequestDto {
	fullName: string;
	email: string;
	phone: string;
	address?: string;
	content: string;
	response?: string;
	schoolId: string;
	userId?: string;
	recaptchaToken?: string;
}
export interface ICreateUnenrollRequestDto extends ICreateSchoolRequestDto{
	schoolRequestId:string;
}

export interface IEditSchoolRequestDto {
	id:string,
    fullName: string;
    email: string;
    phone: string;
    address?: string;
    content: string;
    response?: string;
    schoolId: string;
    userId?: string;
}

enum GenderEnums {
    Male,
	Female
}

export enum SchoolRequestStatusEnum {
	Pending = 'edb2aa8a-cc4f-4c88-b401-d4f6b9983224',
	Approved = '7c9ae660-0355-4c6c-be76-92ca2d8d7dab',
	Rejected = '3e8fa7ad-7b06-4fb2-a044-7fb034d3e5b2'
}

export enum SchoolRequestCategoryEnum {
	Admin = '0a83d423-d833-4b35-a3fb-70f2e0718187',
	Consultant = '2b1ec8fa-a034-48f8-9a94-6c70c67b4c8a',
	Enroll = '38988031-df48-45c3-b73c-90276071de6a',
	UnEnroll = 'd5cbc197-7022-439f-9050-54a617888feb'
}
