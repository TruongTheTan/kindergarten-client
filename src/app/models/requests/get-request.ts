export interface GetRequest {
	id: number;
	title: string;
	status: boolean;
}
