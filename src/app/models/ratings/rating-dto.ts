export interface RatingDTO {
	learnProgramRating: number;
	facilitiesAndUtilitiesRating: number;
	extracurricularActivesRating: number;
	teacherAndStaffRating: number;
	hygieneAndNutrition: number;
	feedback: string;
	username: string;
	createAt: string;
	avatar: string;
}
