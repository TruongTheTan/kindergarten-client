import { CreateSchool } from './create-school';

export interface UpdateSchool extends CreateSchool {
	id: string;
	updatedBy: string;
}
