export interface CreateSchool {
	schoolName: string;
	email: string;
	phone: string;
	createdBy: string;
	schoolTypeId: string;
	statusId: string;
	educationMethodId: string;
	languageId: string;
	feeFrom: number;
	feeTo: number;
	childReceivingAgeId: string;
	schoolIntroduce: string;
	specificAddress: string;
	wardId: string;
	districtId: string;
	provinceId: string;
	facilityIDs: string[];
	utilityIDs: string[];
	images?: File[];
}
