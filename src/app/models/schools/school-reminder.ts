import { SchoolDTO } from './school-dto';

export interface SchoolReminder extends Omit<SchoolDTO, ExcludedProperties> {}

type ExcludedProperties =
	| 'schoolTypeId'
	| 'schoolTypeName'
	| 'childReceivingAgeId'
	| 'childReceivingName'
	| 'educationMethodId'
	| 'educationMethodName'
	| 'feeFrom'
	| 'feeTo'
	| 'schoolIntroduce'
	| 'languageId'
	| 'languageName'
	| 'statusId'
	| 'website'
	| 'wardId'
	| 'districtId'
	| 'provinceId'
	| 'wards'
	| 'facilitiesName'
	| 'utilitiesName'
	| 'images'
	| 'isDeleted';
