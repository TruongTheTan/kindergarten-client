export interface GetPublicSchoolRequest {
	schoolName?: string;
	provinceId?: string;
	districtId?: string;
	typeSchoolId?: string;
	addmissionAgeId?: string;
	feeFrom?: number;
	feeTo?: number;
	facilities?: string[];
	utlities?: string[];
	sortBy?: string;
	pageIndex: number;
	pageSize: number;
}
