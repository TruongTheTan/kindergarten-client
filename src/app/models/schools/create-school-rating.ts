export interface CreateSchoolRating {
	learnProgramRating?: number;
	facilitiesAndUtilitiesRating?: number;
	extracurricularActivesRating?: number;
	teacherAndStaffRating?: number;
	hygieneAndNutrition?: number;
	schoolId: string;
	userId: string;
	feedback: string;
}
