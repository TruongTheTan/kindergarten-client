import { SchoolMasterData } from '../school-master-data';
import { RatingDTO } from './rating-dto';

export interface PublicSchoolDTO {
	id: string;
	schoolName: string;
	schoolTypeId?: string;
	childReceivingAgeId?: string;
	educationMethodId?: string;
	feeFrom: number;
	feeTo: number;
	specificAddress: string;
	statusId?: string;
	website?: string;
	wardId?: string;
	districtId?: string;
	provinceId?: string;
	wards: SchoolMasterData;
	districts: SchoolMasterData;
	provinces: SchoolMasterData;
	ratings: RatingDTO[];
	images: string;
	facilities: SchoolMasterData[];
	utilities: SchoolMasterData[];
	schoolType: SchoolMasterData;
	schoolStatuses: SchoolMasterData[];
	receivingAge: SchoolMasterData;
	createdBy?: string;
}
