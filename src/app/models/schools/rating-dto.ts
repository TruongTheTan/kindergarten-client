export interface RatingDTO {
	learnProgramRating: number;

	facilitiesAndUtilitiesRating: number;

	extracurricularActivesRating: number;

	teacherAndStaffRating: number;

	hygieneAndNutrition: number;

	totalRate: number;
}
