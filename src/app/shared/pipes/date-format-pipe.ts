import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'dateFormat', standalone: true })
export class DateFormatPipe implements PipeTransform {
	transform(value: string, format: 'dd/mm/yyyy' | 'mm/dd/yyyy' = 'dd/mm/yyyy') {
		const date = new Date(value as string);
		const isTypeDate = date instanceof Date && !isNaN(date.getTime());

		if (isTypeDate) {
			const parsedDate = new Date(value as string);
			return format === 'dd/mm/yyyy'
				? this.formatDateToDayMonthYear(parsedDate)
				: this.formatDateToMonthDayYear(parsedDate);
		}
		return value;
	}

	private formatDateToDayMonthYear(parsedDate: Date) {
		const day = String(parsedDate.getDate()).padStart(2, '0');
		const month = String(parsedDate.getMonth() + 1).padStart(2, '0'); // Month (0-indexed)
		const year = parsedDate.getFullYear();

		return `${day}/${month}/${year}`;
	}

	private formatDateToMonthDayYear(parsedDate: Date) {
		const day = parsedDate.getDate().toString().padStart(2, '0'); // '06' (zero-padded)
		const month = parsedDate.toLocaleDateString('en-US', {
			month: 'short'
		});
		const year = parsedDate.getFullYear();

		return `${month}-${day}-${year}`; // Jun-06-2024
	}
}
