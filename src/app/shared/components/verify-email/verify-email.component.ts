import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router, RouterModule } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication.service';
import { CommonModule } from '@angular/common';

@Component({
	selector: 'app-verify-email',
	standalone: true,
	imports: [RouterModule, CommonModule],
	templateUrl: './verify-email.component.html',
	styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit {
	message = '';
	success = true;
	userId = '';
	code = '';
	constructor(authenticationService: AuthenticationService, route: ActivatedRoute) {
		this.userId = route.snapshot.paramMap.get('userId')!;
		this.code = route.snapshot.paramMap.get('code')!;

		authenticationService.confirmEmail(this.userId, this.code).subscribe({
			next: () => {
				this.message = 'Email is confirm success';
				this.success = true;
			},
			error: () => {
				this.message = 'Email is confirm fail';
				this.success = false;
			}
		});
	}

	ngOnInit() {}
}
