import { NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { PageResult } from '../../../models/page-result';

@Component({
	selector: 'app-show-entry',
	standalone: true,
	imports: [NgIf],
	templateUrl: './show-entry.component.html',
	styleUrl: './show-entry.component.scss',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShowEntryComponent<T> {
	@Input({ required: true }) pageResult = {} as PageResult<T>;
}
