import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'app-rating',
	standalone: true,
	templateUrl: './rating.component.html',
	styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {
	@Input() numberRating: number = 0;
	unRateStart!: number;
	constructor() {}
	ngOnInit() {}
	createRange(number: number) {
		let newArray: number[] = [];
		const value = Math.floor(number);
		if (this.isDecimal(this.numberRating)) {
			this.unRateStart = 5 - value - 1;
		} else {
			this.unRateStart = 5 - value;
		}
		if (isNaN(number) === false) {
			newArray = new Array(value).fill(0).map((n, index) => index + 1);
		}
		return newArray;
	}
	isDecimal(value: any): boolean {
		if (typeof value !== 'number' || isNaN(value)) {
			return false;
		}
		return value % 1 !== 0;
	}
}
