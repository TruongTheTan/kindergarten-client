import { CommonModule } from '@angular/common';
import { SchoolRequestService } from './../../../../services/school-request.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
@Component({
	selector: 'app-edit-school-request',
	standalone: true,
	imports: [CommonModule, ReactiveFormsModule],
	templateUrl: './edit-school-request.component.html',
	styleUrl: './edit-school-request.component.scss'
})
export class EditSchoolRequestComponent implements OnInit {
	@Input() userSchoolData: any;
	@Output() onSubmit = new EventEmitter<any>();
	@Output() onCancel = new EventEmitter<void>();
	form: FormGroup;
	isShow: boolean = false;
	constructor(
		private fb: FormBuilder,
		private route: ActivatedRoute,
		private schoolRequestService: SchoolRequestService
	) {
		this.form = this.fb.group({
			fullName: [{ value: '', disabled: true }],
			email: ['', [Validators.required, Validators.email]],
			mobileNo: [{ value: '', disabled: true }],
			dob: [{ value: '', disabled: true }],
			address: [{ value: '', disabled: true }],
			enrolledSchool: [{ value: '', disabled: true }],
			inquiry: ['', [Validators.required]]
		});
	}

	ngOnInit(): void {}

	open() {
		this.form.patchValue({
			fullName: this.userSchoolData.fullName,
			email: this.userSchoolData.email,
			dob: this.userSchoolData.dob ? this.userSchoolData.dob.split('T')[0] : '',
			mobileNo: this.userSchoolData.mobileNo,
			address: this.userSchoolData.address,
			enrolledSchool: this.userSchoolData.enrolledSchool
		});
		this.isShow = true;
	}

	submit() {
		if (this.form.valid) {
			grecaptcha.ready(() => {
				grecaptcha
					.execute('6Ldz_vopAAAAAOVeu2j6DXr_WWsP3X71192-IHVF', { action: 'submit' })
					.then((token: string) => {
						const formValue = this.form.value;
						const putBody = {
							id: this.userSchoolData.requestedId,
							fullName: this.userSchoolData.fullName,
							email: formValue.email,
							phone: this.userSchoolData.mobileNo,
							content: formValue.inquiry,
							address: this.userSchoolData.address,
							dob: this.userSchoolData.dob,
							userId: this.userSchoolData.userId,
							schoolId: this.userSchoolData.schoolId,
							recaptchaToken: token
						};
						if (this.userSchoolData.requestCategory === 'Enroll') {
							this.schoolRequestService
								.editEnrollToSchoolRequest(this.userSchoolData.requestedId, putBody)
								.subscribe({
									next: (response) => {
										console.log('Enroll Request submitted successfully:', response);
										this.onSubmit.emit();
										this.isShow = false;
									},
									error: (err) => {
										console.error('Error submitting request', err);
										alert('There was an error submitting your request. Please try again.');
									}
								});
						} else if (this.userSchoolData.requestCategory === 'Consultant') {
							this.schoolRequestService
								.editCounselingRequest(this.userSchoolData.requestedId, putBody)
								.subscribe({
									next: (response) => {
										console.log('Counseling Request submitted successfully:', response);
										this.onSubmit.emit();
										this.isShow = false;
									},
									error: (err) => {
										console.error('Error submitting request', err);
										alert('There was an error submitting your request. Please try again.');
									}
								});
						}
					})
					.catch((error: any) => {
						console.error('Error executing reCAPTCHA:', error);
						alert('There was an error with reCAPTCHA. Please try again.');
					});
			});
		} else {
			this.form.markAllAsTouched();
		}
	}

	close() {
		this.isShow = false;
	}

	cancel() {
		this.onCancel.emit();
		this.isShow = false;
	}
}
