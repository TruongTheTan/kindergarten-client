import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
	selector: 'app-modal',
	standalone: true,
	imports: [],
	templateUrl: './modal.component.html',
	styleUrl: './modal.component.scss',
})
export class ModalComponent {
	@Input() buttonColor = 'btn-primary';
	@Input({ required: true }) buttonText = '';
	@Input({ required: true }) modalTitle = '';
	@Input({ required: true }) modalConfirmQuestion = '';

	@Output() onButtonClickEvent = new EventEmitter();

	onButtonClick() {
		this.onButtonClickEvent.emit();
	}
}
