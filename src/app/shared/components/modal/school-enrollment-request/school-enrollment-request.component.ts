import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { ExtendedUserDTO } from '../../../../models/users/user-dto';
import { SchoolRequestService } from './../../../../services/school-request.service';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
	selector: 'app-school-enrollment-request',
	standalone: true,
	imports: [CommonModule, ReactiveFormsModule, CKEditorModule],
	templateUrl: './school-enrollment-request.component.html',
	styleUrl: './school-enrollment-request.component.scss'
})
export class SchoolEnrollmentRequestComponent implements OnInit {
	@Input() userSchoolData!: ExtendedUserDTO;

	@Output() onSubmit = new EventEmitter<any>();
	@Output() onCancel = new EventEmitter<void>();
	editor = ClassicEditor;
	form = this.fb.group({
		fullName: [{ value: '', disabled: true }],
		email: ['', [Validators.required, Validators.email]],
		phoneNumber: [{ value: '', disabled: true }],
		dob: [{ value: '', disabled: true }],
		address: [{ value: '', disabled: true }],
		enrolledSchool: [{ value: '', disabled: true }],
		inquiry: ['', [Validators.required]]
	});

	isShow: boolean = false;

	constructor(private fb: FormBuilder, private schoolRequestService: SchoolRequestService) {}

	ngOnInit(): void {}

	open() {
		this.isShow = true;
		this.form.patchValue({
			fullName: this.userSchoolData.fullName,
			email: this.userSchoolData.email,
			dob: this.userSchoolData.dob ? this.userSchoolData.dob.split('T')[0] : '',
			phoneNumber: this.userSchoolData.phoneNumber,
			address: this.userSchoolData.address,
			enrolledSchool: this.userSchoolData.enrolledSchool,
		});
	}

	submit() {
		if (this.form.valid) {
			grecaptcha.ready(() => {
				grecaptcha
					.execute('6Ldz_vopAAAAAOVeu2j6DXr_WWsP3X71192-IHVF', { action: 'submit' })
					.then((token: string) => {
						const formValue = this.form.value;

						const postBody = {
							fullName: this.userSchoolData.fullName!,
							email: formValue.email!,
							phone: this.userSchoolData.phoneNumber!,
							content: formValue.inquiry!,
							address: this.userSchoolData.address!,
							dob: this.userSchoolData.dob!,
							userId: this.userSchoolData.userId!,
							schoolId: this.userSchoolData.schoolId!,
							recaptchaToken: token
						};

						this.schoolRequestService.enrollToSchool(postBody).subscribe({
							next: (response) => {
								console.log('Enroll Request submitted successfully:', response);
								this.isShow = false;
							},
							error: (err) => {
								console.error('Error submitting request', err);
								alert('There was an error submitting your request. Please try again.');
							}
						});
					})
					.catch((error: any) => {
						console.error('Error executing reCAPTCHA:', error);
						alert('There was an error with reCAPTCHA. Please try again.');
					});
			});
		} else {
			this.form.markAllAsTouched();
		}
	}

	close() {
		this.isShow = false;
	}

	cancel() {
		this.onCancel.emit();
		this.isShow = false;
	}
}
