import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { SchoolDTO } from '../../../../models/schools/school-dto';
import { ExtendedUserDTO } from '../../../../models/users/user-dto';
import { SchoolRequestService } from '../../../../services/school-request.service';
import { AuthenticationUtils } from '../../../../utils/authentication-utils';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@Component({
	selector: 'app-request-counseling-modal',
	standalone: true,
	imports: [CommonModule, ReactiveFormsModule, CKEditorModule],
	templateUrl: './request-counseling-modal.component.html',
	styleUrl: './request-counseling-modal.component.scss'
})
export class RequestCounselingModalComponent {
	@Input() userData!: ExtendedUserDTO;
	@Input() schoolDetail!: SchoolDTO;
	editor = ClassicEditor;
	@Output() onSubmit = new EventEmitter<any>();
	@Output() onCancel = new EventEmitter<void>();

	showModal: boolean = false;

	form = this.fb.group({
		fullName: ['', Validators.required],
		email: ['', [Validators.required, Validators.email]],
		phoneNumber: ['', Validators.required],
		inquiry: ['']
	});

	constructor(
		private fb: FormBuilder,
		private route: ActivatedRoute,
		private schoolRequestService: SchoolRequestService,
		private authenticationUtils: AuthenticationUtils
	) {}

	open() {
		console.log(this.userData);

		this.form.patchValue(this.userData);
		this.showModal = true;
	}

	close() {
		this.showModal = false;
	}

	submit() {
		if (this.form.valid) {
			grecaptcha.ready(() => {
				grecaptcha
					.execute('6Ldz_vopAAAAAOVeu2j6DXr_WWsP3X71192-IHVF', { action: 'submit' })
					.then((token: string) => {
						const formValue = this.form.value;

						const postBody = {
							fullName: formValue.fullName!,
							email: formValue.email!,
							phone: formValue.phoneNumber!,
							content: formValue.inquiry!,
							schoolId: this.schoolDetail.id ?? '',
							userId: this.authenticationUtils.getUserIdFromCookie() || undefined,
							recaptchaToken: token
						};

						this.schoolRequestService.createCounselingRequest(postBody).subscribe({
							next: (response) => {
								console.log('Request submitted successfully:', response);
								this.onSubmit.emit();
								this.showModal = false;
							},
							error: (err) => {
								console.error('Error submitting request', err);
								alert('There was an error submitting your request. Please try again.');
							}
						});
					})
					.catch((error: any) => {
						console.error('Error executing reCAPTCHA:', error);
						alert('There was an error with reCAPTCHA. Please try again.');
					});
			});
		} else {
			this.form.markAllAsTouched();
		}
	}

	getSchoolImage(): string {
		const fallbackImage = 'https://i.insider.com/57b48832db5ce94f008b73fc?width=800&format=jpeg&auto=webp';
		// if (this.schoolDetail?.images && this.schoolDetail?.images.length > 0) {
		// 	return this.schoolDetail?.images[0];
		// } else {
		return fallbackImage;
		// }
	}

	cancel() {
		this.onCancel.emit();
		this.showModal = false;
	}
}
