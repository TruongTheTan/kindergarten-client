import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
	selector: 'app-confirm-modal',
	standalone: true,
	imports: [CommonModule],
	templateUrl: './confirm-modal.component.html',
	styleUrl: './confirm-modal.component.scss'
})
export class ConfirmModalComponent {
	@Input() buttonText = '';
	@Input() modalTitle = '';
	@Input() modalConfirmQuestion = '';

	@Output() onButtonClickEvent = new EventEmitter<void>();

	showModal: boolean = false;

	open() {
		this.showModal = true;
	}

	close(event:Event) {
		event.preventDefault();
		this.showModal = false;
	}

	onButtonClick() {
		this.onButtonClickEvent.emit();
		this.showModal = false;
	}
}
