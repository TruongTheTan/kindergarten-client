import { AfterViewInit, Component, effect, Input, OnInit, signal } from '@angular/core';
import { BreadcrumbComposite } from './breadcrumb-composite';

@Component({
	selector: 'app-breadcrumb',
	standalone: true,
	imports: [],
	templateUrl: './breadcrumb.component.html',
	styleUrl: './breadcrumb.component.scss'
})
export class BreadcrumbComponent implements AfterViewInit, OnInit {
	private breadcrumb = {} as HTMLOListElement;

	@Input() breadcrumbManagementName = signal('');
	@Input() breadcrumbComposite = signal({} as BreadcrumbComposite);

	constructor() {
		effect(() => {
			this.removeAllBreadcrumbs();
			this.addBreadcrumbManagementName();
			this.addBreadcrumb();
		});
	}

	ngOnInit(): void {
		this.breadcrumbManagementName.set('School Management');
	}

	ngAfterViewInit(): void {
		this.breadcrumb = document.getElementById('breadcrumbList')! as HTMLOListElement;
	}

	removeAllBreadcrumbs() {
		while (this.breadcrumb.firstChild) {
			this.breadcrumb.removeChild(this.breadcrumb.firstChild!);
		}
	}

	// Use for displaying management type (school management, parent management,...) on breadcrumb
	addBreadcrumbManagementName() {
		const newListItem = document.createElement('li');

		newListItem.innerHTML = `<li class="breadcrumb-item" >
			<a style="color: #4e73df; text-decoration: none; background-color: transparent">
				${this.breadcrumbManagementName()}
			</a>
		</li>`;

		this.breadcrumb.appendChild(newListItem);
	}

	addBreadcrumb() {
		const newListItem = document.createElement('li');

		newListItem.innerHTML = `<li class="breadcrumb-item active" aria-current="page">
			<span class="px-1">/</span>
			<a>${this.breadcrumbComposite().breadCrumbDisplayName}
			</a>
		</li>`;

		this.breadcrumb.appendChild(newListItem);
	}
}
