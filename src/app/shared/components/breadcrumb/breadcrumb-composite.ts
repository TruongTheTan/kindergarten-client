export interface BreadcrumbComposite {
	breadCrumbDisplayName: string;
	breadCrumbRouterPath: string;
}
