import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { PageResult } from '../../../models/page-result';

@Component({
	selector: 'app-paging',
	standalone: true,
	imports: [CommonModule],
	templateUrl: './paging.component.html',
	styleUrls: ['./paging.component.css']
})
export class PagingComponent<T> implements OnChanges {
	@Input() pageResult: PageResult<T> = {} as PageResult<T>;
	@Output() currentPageIndex = new EventEmitter<number>();

	public startIndex: number = 0;
	public finishIndex: number = 0;

	ngOnChanges(changes: SimpleChanges): void {
		this.startIndex = Math.max(this.pageResult.pageIndex! - 5, 1);
		this.finishIndex = Math.min(this.pageResult.pageIndex! + 5, this.pageResult.pageCount!);
	}

	changePage(index: number) {
		this.currentPageIndex.emit(index);
	}

	createRange(number: number) {
		let newArray: number[] = [];

		if (isNaN(number) === false) {
			newArray = new Array(number).fill(0).map((n, index) => index + 1);
		}
		return newArray;
	}

	onLinkClick(event: Event, page: number): void {
		event.preventDefault();
		this.changePage(page);
	}
}
