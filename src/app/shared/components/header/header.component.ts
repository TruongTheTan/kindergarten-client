import { CommonModule } from '@angular/common';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router, RouterLink, RouterModule } from '@angular/router';
import { Subscription } from 'rxjs';
import { NotificationDto } from '../../../models/notification/notification';
import { AuthenticationService } from '../../../services/authentication.service';
import { RequestHubService } from '../../../services/hubs/requestHub.service';
import { NotificationService } from '../../../services/notification.service';
import { AuthenticationUtils } from '../../../utils/authentication-utils';
import { ClickOutsideDirective } from '../../../utils/click-outside';

@Component({
	selector: 'app-header',
	standalone: true,
	imports: [CommonModule, RouterLink, RouterModule, ClickOutsideDirective],

	templateUrl: './header.component.html',
	styleUrl: './header.component.scss'
})
export class HeaderComponent implements OnInit, OnDestroy {
	@Input() isPublicSite = false;
	@Input() isDisplayMarginBottom = true;
	private audio: HTMLAudioElement;
	protected subscription?: Subscription | null;
	notifications: NotificationDto[] = [];
	notificationCount: number = 0;
	protected role = '';
	protected userName = '';
	protected isUserAuthenticated = false;
	protected base64Image = '';

	notificationType: string = '';
	constructor(
		private authService: AuthenticationService,
		private router: Router,
		private requestHubService: RequestHubService,
		private notificationService: NotificationService,
		private authenticationUtils: AuthenticationUtils
	) {
		this.base64Image += !localStorage.getItem('avatar')
			? 'data:image/jpeg;base64,' + localStorage.getItem('avatar')
			: 'https://cdn-icons-png.flaticon.com/512/9187/9187604.png';
		this.role = this.authenticationUtils.getRoleFromCookie();
		this.userName = this.authenticationUtils.getFullNameFromCookie()!;
		this.isUserAuthenticated = !!this.authenticationUtils.getTokenFromCookie();

		this.audio = new Audio();
		this.audio.src = '../../../../assets/notification_sound.mp3';
		this.audio.load();
	}

	ngOnInit(): void {
		if (this.role) {
			this.loadNotifications(1, 10);

			this.subscription = this.requestHubService.notification$.subscribe((notification) => {
				this.loadNotifications(1, 10);
				this.loadUnreadNotificationCount(notification);
			});

			this.loadUnreadNotificationCount(null);
		}
	}

	loadNotifications(pageNumber: number, pageSize: number): void {
		this.notificationService.getNotifications(pageNumber, pageSize).subscribe((response) => {
			if (response.isSuccess) {
				this.notifications = response.data?.items ?? [];
			} else {
				console.error(response.message);
			}
		});
	}

	private playNotificationSound(): void {
		this.audio.play().catch((error) => {
			console.error('Error playing notification sound:', error);
		});
	}

	getNotificationType(title: string) {
		return title.split(' ')[0];
	}

	loadUnreadNotifications(pageNumber: number, pageSize: number): void {
		this.notificationService.getUnreadNotifications(pageNumber, pageSize).subscribe((response) => {
			if (response.isSuccess) {
				this.notifications = response.data?.items ?? [];
			} else {
				console.error(response.message);
			}
		});
	}

	loadUnreadNotificationCount(notification: any): void {
		this.notificationService.getUnreadNotificationCount().subscribe((response) => {
			if (response.isSuccess) {
				this.notificationCount = response.data ?? 0;
				if (notification) {
					let notificationFirstElement = notification.message.split(' ')[0];
					if (
						notificationFirstElement === 'NewEnrollment' ||
						notificationFirstElement === 'Unenroll' ||
						notificationFirstElement === 'Resolved'
					) {
						this.playNotificationSound();
					}
				}
			} else {
				console.error(response.message);
			}
		});
	}

	deleteNotification(notificationId: string): void {
		this.notificationService.deleteNotification(notificationId).subscribe((response) => {
			if (response.isSuccess) {
				this.notifications = this.notifications.filter((n) => n.id !== notificationId);
				this.notificationCount--;
			} else {
				console.error(response.message);
			}
		});
	}

	markAsRead(notificationId: string): void {
		this.notificationService.markAsRead(notificationId).subscribe((response) => {
			if (response.isSuccess) {
				const notification = this.notifications.find((n) => n.id === notificationId);
				if (notification) {
					notification.isOpen = true;
				}
			} else {
				console.error(response.message);
			}
		});
	}

	markAllAsRead(): void {
		this.notificationService.markAllAsRead().subscribe((response) => {
			if (response.isSuccess) {
				this.notifications.forEach((n) => (n.isOpen = true));
			} else {
				console.error(response.message);
			}
		});
	}

	activeButton: string = 'all'; // Biến giữ trạng thái của nút được nhấn

	setActiveButton(button: string) {
		this.activeButton = button;
	}

	showOptions: boolean = false;

	toggleOptions(): void {
		this.showOptions = !this.showOptions;
	}

	showSubOptions: boolean = false;
	toggleSubOptions(notificationId: string): void {
		this.notifications = this.notifications.map((notification) => {
			if (notification.id === notificationId) {
				notification.showOptions = !notification.showOptions;
			} else {
				notification.showOptions = false; // Ẩn các menu tùy chọn khác
			}
			return notification;
		});
	}

	openInNewWindow(): void {
		alert('Opened in new window');
		this.showOptions = false;
	}

	navigate(requestId: string, notificationId: string) {
		if (this.role === 'SchoolOwner' || this.role === 'Admin' || this.role === 'SuperAdmin') {
			this.router.navigateByUrl('/content/school-request/detail?id=' + requestId);
			this.markAsRead(notificationId);
		} else if (this.role === 'Parents') {
			this.router.navigateByUrl('/public/user/my-request?id=' + requestId);
			this.markAsRead(notificationId);
		}
	}

	isOpen = false;

	closeDropdown() {
		this.isOpen = false;
	}

	toggleDropdown() {
		this.isOpen = !this.isOpen;
	}

	ngOnDestroy(): void {
		this.subscription?.unsubscribe();
	}

	onLogoutButtonClick() {
		this.authService.logout();
	}
}
