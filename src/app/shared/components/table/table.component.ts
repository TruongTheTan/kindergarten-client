import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { PageResult } from '../../../models/page-result';
import { SchoolRequestCategoryEnum, SchoolRequestStatusEnum } from '../../../models/requests/school-request-dto';
import { DateFormatPipe } from '../../pipes/date-format-pipe';
import { PagingComponent } from '../paging/paging.component';

@Component({
	selector: 'app-table',
	standalone: true,
	imports: [DateFormatPipe, CommonModule, PagingComponent],
	templateUrl: './table.component.html',
	styleUrl: './table.component.scss'
})
export class TableComponent<T> implements AfterViewInit, OnChanges {
	private searchInput = {} as HTMLInputElement;

	@Input() coloredColumn = '';
	@Input() tableName = '';
	@Input() isTableHasStatus = true;
	@Input() keys = [] as (keyof T)[];
	@Input() pageResult = {} as PageResult<T> | null;

	@Input() tableHeadings = [] as string[];
	@Input() isDisplayCreateButton = false;
	@Input() isDisplayActionButtons = false;
	@Input() formatDateType: 'dd/mm/yyyy' | 'mm/dd/yyyy' = 'mm/dd/yyyy';
	@Input() createPath = '';
	@Input() colorOfStatus = [] as { statusName: string; color: string }[];

	@Output() buttonClick = new EventEmitter<{
		data: T;
		eventName: string;
	}>();
	@Output() currentPage = new EventEmitter<number>();
	@Output() buttonSearchEvent = new EventEmitter<string>();

	startIndex: number = 0;

	constructor(private routes: Router) {}

	ngOnChanges(changes: SimpleChanges): void {
		if (this.pageResult) {
			this.startIndex = (this.pageResult!.pageIndex! - 1) * this.pageResult!.pageSize! + 1;
		}
	}

	ngAfterViewInit(): void {
		this.setEnterEventForInputSearch();
	}

	getCurrentPage($event: number) {
		this.currentPage.emit($event);
	}

	private setEnterEventForInputSearch() {
		this.searchInput = document.getElementById('searchInput')! as HTMLInputElement;

		this.searchInput.addEventListener('keypress', (event) => {
			if (event.key === 'Enter') {
				event.preventDefault();
				console.log(this.searchInput.value);

				this.buttonSearchEvent.emit(this.searchInput.value);
			}
		});
	}

	isDeleted(data: any) {
		const value = data['isDeleted'];
		return typeof value === 'boolean' ? value : value;
	}

	getStatusColor(statusName: string) {
		const e = this.colorOfStatus.find((e) => e.statusName === statusName);
		return `color: ${e?.color}`;
	}

	onEditButtonClick(data: T) {
		this.buttonClick.emit({ eventName: 'edit', data: data });
	}

	onDeleteButtonClick(data: T) {
		this.buttonClick.emit({ eventName: 'delete', data: data });
	}

	onCreateButtonClick() {
		this.routes.navigateByUrl(this.createPath);
	}

	onSearchButtonClick() {
		this.buttonSearchEvent.emit(this.searchInput.value);
	}

	navigateMarkAsResolve(data: any) {
		this.routes.navigateByUrl(`/content/school-request/detail/${data.id}`);
	}

	getStatusName(statusId: string): string {
		switch (statusId) {
			case SchoolRequestStatusEnum.Pending:
				return 'Pending';
			case SchoolRequestStatusEnum.Approved:
				return 'Approved';
			case SchoolRequestStatusEnum.Rejected:
				return 'Rejected';
			default:
				return statusId;
		}
	}

	getCategoryName(categoryId: string): string {
		switch (categoryId) {
			case SchoolRequestCategoryEnum.Enroll:
				return 'Enroll';
			case SchoolRequestCategoryEnum.UnEnroll:
				return 'Unenroll';
			case SchoolRequestCategoryEnum.Consultant:
				return 'Consultant';
			case SchoolRequestCategoryEnum.Admin:
				return 'Admin';
			default:
				return categoryId;
		}
	}
}
