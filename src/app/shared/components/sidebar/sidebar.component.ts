import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, EventEmitter, Output } from '@angular/core';
import { Router, RouterLink, RouterModule } from '@angular/router';
import { AuthenticationUtils } from '../../../utils/authentication-utils';

@Component({
	selector: 'app-sidebar',
	standalone: true,
	imports: [CommonModule, RouterLink, RouterModule],
	templateUrl: './sidebar.component.html',
	styleUrl: './sidebar.component.scss'
})
export class SidebarComponent implements AfterViewInit {
	protected sideBar = {} as HTMLElement;
	protected icons = {} as NodeListOf<HTMLElement>;

	private liTagElements = [] as HTMLLIElement[];
	private schoolManagementLiTag = {} as HTMLLIElement;
	private userManagementLiTag = {} as HTMLLIElement;
	private reminderManagementLiTag = {} as HTMLLIElement;
	private parentManagementLiTag = {} as HTMLLIElement;
	private requestManagementLiTag = {} as HTMLLIElement;

	@Output() onItemClick = new EventEmitter<string>();

	protected isCollapsed = false;
	protected isAdmin = false;

	constructor(private router: Router, private authenticationUtils: AuthenticationUtils) {
		const role = authenticationUtils.getRoleFromCookie();
		this.isAdmin = role === 'Admin' || role === 'SuperAdmin';
	}

	toggleCollapse(event: Event) {
		event.preventDefault();
		this.isCollapsed = !this.isCollapsed;
	}

	ngAfterViewInit(): void {
		this.getAllSideBarItems();
		this.getAllSidebarIcons();
		this.setSidebarItemClickEvent();
		this.setSidebarItemActiveClassByUrl(this.router.url.trim());
	}

	private setSidebarItemActiveClassByUrl(url: string) {
		const moduleName = url.split('/')[2].trim();

		if (moduleName === 'school' && url.includes('reminder')) {
			this.emitManagementModuleName(this.reminderManagementLiTag);
		} else {
			switch (moduleName) {
				case 'users':
					this.emitManagementModuleName(this.userManagementLiTag);
					break;

				case 'parent':
					this.emitManagementModuleName(this.parentManagementLiTag);
					break;

				case 'school-request':
					this.emitManagementModuleName(this.requestManagementLiTag);
					break;

				default:
					break;
			}
		}
	}

	// Get all li tags

	private getAllSideBarItems() {
		this.sideBar = document.getElementById('accordionSidebar')! as HTMLUListElement;

		this.schoolManagementLiTag = document.getElementById('schoolManagement') as HTMLLIElement;
		this.userManagementLiTag = document.getElementById('userManagement') as HTMLLIElement;
		this.reminderManagementLiTag = document.getElementById('reminderManagement') as HTMLLIElement;
		this.parentManagementLiTag = document.getElementById('parentManagement') as HTMLLIElement;
		this.requestManagementLiTag = document.getElementById('requestManagement') as HTMLLIElement;

		this.liTagElements = [
			this.schoolManagementLiTag,
			this.userManagementLiTag,
			this.reminderManagementLiTag,
			this.parentManagementLiTag,
			this.requestManagementLiTag
		];
	}

	private getAllSidebarIcons() {
		this.icons = this.sideBar.querySelectorAll('i');
	}

	protected toggleSideBar() {
		const isSidebarToggled = this.sideBar.classList.contains('toggled');

		if (isSidebarToggled) {
			this.sideBar.classList.remove('toggled');
			this.icons.forEach((icon) => icon.classList.remove('fs-4'));
		} else {
			this.sideBar.classList.add('toggled');
			this.icons.forEach((icon) => icon.classList.add('fs-4'));
		}
	}

	private setSidebarItemClickEvent() {
		this.liTagElements.forEach((liItem) =>
			liItem?.addEventListener('click', () => {
				this.emitManagementModuleName(liItem);
			})
		);
	}

	private emitManagementModuleName(liItem: HTMLElement) {
		const spanTag = liItem?.querySelector('span');
		this.onItemClick.emit(spanTag?.innerHTML.trim());

		if (spanTag?.innerHTML === 'Reminders') {
			setTimeout(() => {
				for (const liTag of this.liTagElements) {
					if (liTag !== liItem) {
						liTag.classList.remove('sidebar-item-background-selected');
						const childElements = liTag.querySelectorAll('*');

						childElements.forEach((child) => {
							child.classList.remove('sidebar-item-content-color');
						});
					}
				}
			}, 15);
		}
	}
}
