import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, Observable, of, tap } from 'rxjs';
import { ApiResponseHandler } from '../api/api-response-handler';
import { RequestApi } from '../api/request-api';
import { PageResult } from '../models/page-result';
import {
	ICreateSchoolRequestDto,
	ICreateUnenrollRequestDto,
	IEditSchoolRequestDto,
	SchoolRequestDto
} from '../models/requests/school-request-dto';
import { ApiResponse } from './../models/api-response';

@Injectable({ providedIn: 'root' })
export class SchoolRequestService {
	private readonly schoolEnrollRequestList = new BehaviorSubject({} as PageResult<SchoolRequestDto>);
	private readonly schoolCounselingRequestList = new BehaviorSubject({} as PageResult<SchoolRequestDto>);
	private readonly mySchoolRequestList = new BehaviorSubject({} as PageResult<SchoolRequestDto>);
	private readonly schoolRequest = new BehaviorSubject({} as SchoolRequestDto);

	schoolRequest$ = this.schoolRequest.asObservable();
	mySchoolRequestList$ = this.mySchoolRequestList.asObservable();
	schoolEnrollRequestList$ = this.schoolEnrollRequestList.asObservable();
	schoolCounselingRequestList$ = this.schoolCounselingRequestList.asObservable();

	constructor(private schoolRequestApi: RequestApi, private apiResponseHandler: ApiResponseHandler) {}

	getMyRequests(
		query: string,
		sortBy: string,
		sortDirection: string,
		page: number,
		size: number,
		fromDate: string | null,
		endDate: string | null
	) {
		this.schoolRequestApi
			.getMyRequest(query, sortBy, sortDirection, page, size, fromDate, endDate)
			.subscribe((apiResponse) => this.mySchoolRequestList.next(apiResponse.data!));
	}

	getEnrollmentSchoolRequests(
		query: string,
		category: string,
		status: string,
		sortBy: string,
		sortDirection: string,
		fromDate: string | null,
		endDate: string | null,
		page: number,
		size: number
	) {
		this.schoolRequestApi
			.getEnrollmentSchoolRequests(
				query,
				category,
				status,
				sortBy,
				sortDirection,
				fromDate,
				endDate,
				page,
				size
			)
			.subscribe((apiResponse) => this.schoolEnrollRequestList.next(apiResponse.data!));
	}

	getCounselingSchoolRequests(
		query: string,
		category: string,
		status: string,
		sortBy: string,
		sortDirection: string,
		page: number,
		size: number
	) {
		this.schoolRequestApi
			.getCounselingSchoolRequests(query, category, status, sortBy, sortDirection, page, size)
			.subscribe((apiResponse) => this.schoolCounselingRequestList.next(apiResponse.data!));
	}

	getSchoolRequest(requestId: string): Observable<SchoolRequestDto> {
		return this.schoolRequestApi.getSchoolRequest(requestId).pipe(
			map((apiResponse) => apiResponse.data!), // Transform the ApiResponse to the inner data
			tap({
				next: (data) => {
					this.schoolRequest.next(data);
				},
				error: (err) => this.apiResponseHandler.handleError(err)
			})
		);
	}

	getEnrolledSchoolRequestByEnrolledSchoolId(enrolledSchoolId: string): Observable<SchoolRequestDto> {
		return this.schoolRequestApi.getApprovedSchoolRequestByEnrolledSchoolId(enrolledSchoolId).pipe(
			map((apiResponse) => apiResponse.data!),
			tap({
				next: (data) => {
					this.schoolRequest.next(data);
				},
				error: (err) => this.apiResponseHandler.handleError(err)
			})
		);
	}

	resolveSchoolRequest(requestId: string, response: string, isApprove: boolean): Observable<ApiResponse<any>> {
		return this.schoolRequestApi.resolveSchoolRequest(requestId, response, isApprove).pipe(
			map((apiResponse) => {
				this.apiResponseHandler.handleSuccess(apiResponse);
				return apiResponse;
			}),
			catchError((err) => {
				this.apiResponseHandler.handleError(err);
				return of({} as ApiResponse<any>);
			})
		);
	}

	enrollToSchool(body: ICreateSchoolRequestDto): Observable<ApiResponse<any>> {
		return this.schoolRequestApi.enrollToSchool(body).pipe(
			map((apiResponse) => {
				this.apiResponseHandler.handleSuccess(apiResponse);
				return apiResponse;
			}),
			catchError((err) => {
				this.apiResponseHandler.handleError(err);
				return of({} as ApiResponse<any>);
			})
		);
	}

	createUnenrollSchoolRequest(body: ICreateUnenrollRequestDto): Observable<ApiResponse<any>> {
		return this.schoolRequestApi.createUnenrollSchoolRequest(body).pipe(
			map((apiResponse) => {
				this.apiResponseHandler.handleSuccess(apiResponse);
				return apiResponse;
			}),
			catchError((err) => {
				this.apiResponseHandler.handleError(err);
				return of({} as ApiResponse<any>);
			})
		);
	}

	unenrollToSchool(requestedId: string): Observable<ApiResponse<any>> {
		return this.schoolRequestApi.unenrollToSchool(requestedId).pipe(
			map((apiResponse) => {
				this.apiResponseHandler.handleSuccess(apiResponse);
				return apiResponse;
			}),
			catchError((err) => {
				if (err) {
					this.apiResponseHandler.handleError(err);
				}
				return of({} as ApiResponse<any>);
			})
		);
	}

	createCounselingRequest(body: ICreateSchoolRequestDto): Observable<ApiResponse<any>> {
		return this.schoolRequestApi.createCounselingRequest(body).pipe(
			map((apiResponse) => {
				this.apiResponseHandler.handleSuccess(apiResponse);
				return apiResponse;
			}),
			catchError((err) => {
				if (err) {
					this.apiResponseHandler.handleError(err);
				}
				return of({} as ApiResponse<any>);
			})
		);
	}

	editCounselingRequest(requestedId: string, body: IEditSchoolRequestDto): Observable<ApiResponse<any>> {
		return this.schoolRequestApi.editCounselingRequests(requestedId, body).pipe(
			map((apiResponse) => {
				this.apiResponseHandler.handleSuccess(apiResponse);
				return apiResponse;
			}),
			catchError((err) => {
				if (err) {
					this.apiResponseHandler.handleError(err);
				}
				return of({} as ApiResponse<any>);
			})
		);
	}

	editEnrollToSchoolRequest(requestedId: string, body: IEditSchoolRequestDto): Observable<ApiResponse<any>> {
		return this.schoolRequestApi.editEnrollToSchoolRequest(requestedId, body).pipe(
			map((apiResponse) => {
				this.apiResponseHandler.handleSuccess(apiResponse);
				return apiResponse;
			}),
			catchError((err) => {
				if (err) {
					this.apiResponseHandler.handleError(err);
				}
				return of({} as ApiResponse<any>);
			})
		);
	}

	deleteCounselingRequest(ids: string[]): Observable<ApiResponse<any>> {
		return this.schoolRequestApi.deleteCounselingRequests(ids).pipe(
			map((apiResponse) => {
				this.apiResponseHandler.handleSuccess(apiResponse);
				return apiResponse;
			}),
			catchError((err) => {
				if (err) {
					this.apiResponseHandler.handleError(err);
				}
				return of({} as ApiResponse<any>);
			})
		);
	}
}
