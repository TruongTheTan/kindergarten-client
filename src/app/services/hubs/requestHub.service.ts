import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as signalR from '@microsoft/signalr';
import { BehaviorSubject, Subject } from 'rxjs';
import { AuthenticationUtils } from '../../utils/authentication-utils';
import { environment } from '../../../environments/environment.development';

@Injectable({
	providedIn: 'root'
})
export class RequestHubService {
	private hubConnection: signalR.HubConnection;
	private notificationSubject = new Subject<any>();

	public notification$ = this.notificationSubject.asObservable();
	public connectionId$ = new BehaviorSubject<string>('');

	constructor(private http: HttpClient, private authenticationUtils: AuthenticationUtils) {
		const userId = authenticationUtils.getUserIdFromCookie();

		this.hubConnection = new signalR.HubConnectionBuilder()
			.withUrl(`${environment.apiUrl.replace('/api/', '')}/requestHub?userId=${userId}`)
			.build();

		this.hubConnection.on('ReceiveMessage', (message: string) => {
			this.notificationSubject.next({ message, date: new Date() });
		});

		this.startConnection();
	}

	private startConnection(): void {
		this.hubConnection
			.start()
			.then(() => {
				console.log('Connection started');
			})
			.catch((err) => {
				console.error('Error while starting connection:', err);
			});
	}
}
