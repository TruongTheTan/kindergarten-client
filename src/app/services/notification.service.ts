import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api-response';
import { NotificationDto } from '../models/notification/notification';
import { PageResult } from '../models/page-result';

@Injectable({
	providedIn: 'root'
})
export class NotificationService {
	private apiUrl = 'notifications';

	constructor(private http: HttpClient) {}

	getNotifications(pageNumber: number, pageSize: number): Observable<ApiResponse<PageResult<NotificationDto>>> {
		const params = new HttpParams()
			.set('pageNumber', pageNumber.toString())
			.set('pageSize', pageSize.toString());

		return this.http.get<ApiResponse<PageResult<NotificationDto>>>(`${this.apiUrl}`, { params });
	}

	getUnreadNotifications(
		pageNumber: number,
		pageSize: number
	): Observable<ApiResponse<PageResult<NotificationDto>>> {
		const params = new HttpParams()
			.set('pageNumber', pageNumber.toString())
			.set('pageSize', pageSize.toString());

		return this.http.get<ApiResponse<PageResult<NotificationDto>>>(`${this.apiUrl}/unread`, {
			params
		});
	}

	getUnreadNotificationCount(): Observable<ApiResponse<number>> {
		return this.http.get<ApiResponse<number>>(`${this.apiUrl}/count`);
	}

	deleteNotification(notificationId: string): Observable<ApiResponse<any>> {
		return this.http.delete<ApiResponse<boolean>>(`${this.apiUrl}/${notificationId}`);
	}

	markAsRead(notificationId: string): Observable<ApiResponse<any>> {
		return this.http.put<ApiResponse<boolean>>(`${this.apiUrl}/mark-as-read/${notificationId}`, {});
	}

	markAllAsRead(): Observable<ApiResponse<any>> {
		return this.http.put<ApiResponse<boolean>>(`${this.apiUrl}/mark-all-as-read`, {});
	}
}
