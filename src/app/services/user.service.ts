import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, map, Observable, tap } from 'rxjs';
import { ApiResponseHandler } from '../api/api-response-handler';
import { UserApi } from '../api/user-api';
import { PageResult } from '../models/page-result';
import { CreateUseRequest } from '../models/users/create-user-request';
import { Role } from '../models/users/role';
import { updateUseRequest } from '../models/users/update-user-request';
import { UserDto } from '../models/users/user-dto';

@Injectable({ providedIn: 'root' })
export class UserService {
	private readonly ListRole = new BehaviorSubject([] as Role[]);
	private readonly listUser = new BehaviorSubject({} as PageResult<UserDto>);
	public readonly userDetails = new BehaviorSubject({} as UserDto);
	public listRole$ = this.ListRole.asObservable();
	public listUser$ = this.listUser.asObservable();
	public userDetails$ = this.userDetails.asObservable();

	keyword: string = '';
	roleId: string = '';

	constructor(private userApi: UserApi, private router: Router) {}

	changeStatus(UserId: string, status: boolean) {
		console.log(1);
		return this.userApi.changeStatus(UserId, status);
	}

	getListRole() {
		this.userApi.getAllRole().subscribe((apiResponse) => this.ListRole.next(apiResponse.data!));
	}

	createUser(user: CreateUseRequest) {
		const formData = new FormData();

		for (const key in user) {
			const value = (user as any)[key];

			if (value instanceof Array) {
				value.forEach((item) => formData.append(key, item));
			} else {
				formData.append(key, value);
			}
		}
		this.userApi
			.createUser(formData)
			.subscribe(() => setTimeout(() => this.router.navigateByUrl('content/users/list'), 2000));
	}

	editUser(user: updateUseRequest): Observable<any> {
		const formData = new FormData();

		for (const key in user) {
			const value = (user as any)[key];

			if (value instanceof Array) {
				value.forEach((item) => formData.append(key, item));
			} else {
				formData.append(key, value);
			}
		}
		return this.userApi.updateUser(user.id, formData);
	}

	deleteUser(userId: string) {
		this.userApi.deleteUser(userId).subscribe();
	}

	getUserPaging(keyword: string, indexPage: number, size: number, roleId: string | null) {
		this.userApi
			.getUserPaging(keyword, indexPage, size, roleId)
			.subscribe((apiResponse) => this.listUser.next(apiResponse.data!));
	}

	assignRole(userId: string, roleId: string) {
		return this.userApi.assignRole(userId, roleId);
	}

	getUserById(userId: string) {
		console.log(userId);
		return this.userApi.getUserById(userId).pipe(
			map((apiResponse) => apiResponse.data!),
			tap((userDetails) => this.userDetails.next(userDetails))
		);
	}
	getParent(keyword: string, indexPage: number, size: number, roleId: string | null) {
		this.userApi
			.getParent(keyword, indexPage, size, roleId)
			.subscribe((apiResponse) => this.listUser.next(apiResponse.data!));
	}
}
