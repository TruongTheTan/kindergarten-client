import { inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationUtils } from './../utils/authentication-utils';

import { CreateUseRequest } from '../models/users/create-user-request';
import { AuthenticationApi } from './../api/authentication-api';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
	private readonly router = inject(Router);

	constructor(private authenticationApi: AuthenticationApi, private authenticationUtils: AuthenticationUtils) {}

	login(email: string, password: string) {
		this.authenticationApi.login(email, password).subscribe((apiResponse) => {
			if (apiResponse.data) {
				const token = apiResponse.data.token;
				const fullName = apiResponse.data.name;
				const avatar = apiResponse.data.avatar;

				localStorage.setItem('avatar', avatar);
				this.authenticationUtils.setTokenToCookie(token!);
				this.authenticationUtils.setFullNameToCookie(fullName);

				const role = this.authenticationUtils.getRoleFromCookie();
				this.redirectUser(role);
			}
		});
	}

	refreshToken(token: string) {
		this.authenticationApi.refreshToken(token).subscribe((apiResponse) => {
			if (apiResponse.data) {
				const token = apiResponse.data.token;
				this.authenticationUtils.setTokenToCookie(token!);
			}
		});
	}

	logout() {
		this.authenticationApi.logout().subscribe(() => {
			this.authenticationUtils.removeAllCookie();
			setTimeout(() => this.router.navigateByUrl('/auth/login'), 300);
		});
	}

	forgotPassword(email: string) {
		this.authenticationApi.forgotPassword(email).subscribe();
	}

	resetPassword(email: string, token: string, newPassword: string, confirmPassword: string) {
		this.authenticationApi.resetPassword(email, token, newPassword, confirmPassword).subscribe({
			next: () => this.router.navigateByUrl('/auth/login')
		});
	}

	private redirectUser(role: string) {
		if (role === 'Parents') {
			this.router.navigate(['/public/home']);
		} else if (role === 'Admin' || role === 'SuperAdmin' || role === 'SchoolOwner') {
			this.router.navigate(['/content/school/list']);
		} else {
			this.router.navigate(['/not-found']);
		}
	}

	registerUser(user: CreateUseRequest) {
		const formData = new FormData();

		for (const key in user) {
			const value = (user as any)[key];

			if (value instanceof Array) {
				value.forEach((item) => formData.append(key, item));
			} else {
				formData.append(key, value);
			}
		}
		this.authenticationApi
			.registerUser(formData)
			.subscribe(() => setTimeout(() => this.router.navigateByUrl('auth/login'), 2000));
	}
	confirmEmail(userId: string, token: string) {
		return this.authenticationApi.confirmEmail(userId, token);
	}
}
