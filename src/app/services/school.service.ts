import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { RatingApi } from '../api/rating-api';
import { Province } from '../models/locations/location';
import { PageResult } from '../models/page-result';
import { RatingDTO } from '../models/ratings/rating-dto';
import { GetPublicSchoolRequest } from '../models/schools/public-get-school-request';
import { PublicSchoolDTO } from '../models/schools/public-school-dto';
import { SchoolDTO } from '../models/schools/school-dto';
import { SchoolReminder } from '../models/schools/school-reminder';
import { UpdateSchool } from '../models/schools/update-school';
import { ApiResponseHandler } from './../api/api-response-handler';
import { SchoolApi } from './../api/school-api';
import { CreateSchool } from './../models/schools/create-school';
import { CreateSchoolRating } from './../models/schools/create-school-rating';

@Injectable({ providedIn: 'root' })
export class SchoolService {
	private readonly wardList = new BehaviorSubject([] as Province[]);
	private readonly schoolDetail = new BehaviorSubject({} as SchoolDTO);
	private readonly provinceList = new BehaviorSubject([] as Province[]);
	private readonly districtList = new BehaviorSubject([] as Province[]);
	private readonly ratingSchoolList = new BehaviorSubject({} as RatingDTO[]);
	private readonly schoolList = new BehaviorSubject({} as PageResult<SchoolDTO>);
	private readonly schoolReminderList = new BehaviorSubject({} as PageResult<SchoolReminder>);
	private readonly publicSchoolList = new BehaviorSubject({} as PageResult<PublicSchoolDTO>);

	wardList$ = this.wardList.asObservable();
	schoolList$ = this.schoolList.asObservable();
	provinceList$ = this.provinceList.asObservable();
	districtList$ = this.districtList.asObservable();
	schoolDetail$ = this.schoolDetail.asObservable();
	ratingSchoolList$ = this.ratingSchoolList.asObservable();
	schoolReminderList$ = this.schoolReminderList.asObservable();
	publicSchoolList$ = this.publicSchoolList.asObservable();

	constructor(
		private router: Router,
		private ratingAPI: RatingApi,
		private schoolApi: SchoolApi,
		private apiResponseHandler: ApiResponseHandler
	) {}

	getPublicSchool(request: GetPublicSchoolRequest) {
		const formData = new FormData();

		for (const key in request) {
			const value = (request as any)[key];

			if (value instanceof Array) {
				value.forEach((item) => formData.append(key, item));
			} else {
				formData.append(key, value);
			}
		}
		this.schoolApi
			.getSchoolPublic(formData)
			.subscribe((apiResponse) => this.publicSchoolList.next(apiResponse.data!));
	}

	getAllSchool(schoolName: string, size: number, page: number) {
		this.schoolApi
			.getAllSchool(schoolName, size, page)
			.subscribe((apiResponse) => this.schoolList.next(apiResponse.data!));
	}

	deleteSchool(schoolId: string) {
		this.schoolApi.deleteSchool(schoolId).subscribe((apiResponse) => {
			this.apiResponseHandler.handleSuccess(apiResponse);
			var schoolList = this.schoolList.getValue();
			this.schoolDetail.getValue().schoolStatusName = 'Deleted';

			const shool = schoolList.items!.find((s) => schoolId === s.id);

			shool!.schoolStatusName = 'Deleted';
			shool!.isDeleted = true;

			setTimeout(() => this.router.navigateByUrl('/content/school/list'), 1500);
		});
	}

	createSchool(createSchool: CreateSchool) {
		const formData = new FormData();

		for (const key in createSchool) {
			const value = (createSchool as any)[key];

			if (value instanceof Array) {
				value.forEach((item) => formData.append(key, item));
			} else {
				formData.append(key, value);
			}
		}

		this.schoolApi
			.createSchool(formData)
			.subscribe(() => setTimeout(() => this.router.navigateByUrl('/content/school/list'), 1500));
	}

	getAllProvince() {
		this.schoolApi.getAllProvince().subscribe((apiResponse) => this.provinceList.next(apiResponse.data!));
	}

	getAllDistrictByProvinceCode(provinceCode: string) {
		if (provinceCode !== '' && provinceCode !== null) {
			this.schoolApi
				.getAllDistrictByProvinceCode(provinceCode)
				.subscribe((apiResponse) => this.districtList.next(apiResponse.data!));
		}
	}

	getAllWardByDistrictCode(districtCode: string) {
		if (districtCode !== '' && districtCode !== null) {
			this.schoolApi
				.getAllWardByDistrictCode(districtCode)
				.subscribe((apiResponse) => this.wardList.next(apiResponse.data!));
		}
	}

	getDetailSchoolById(id: string, canLoadImages: boolean) {
		this.schoolApi
			.getDetailSchoolById(id, canLoadImages)
			.subscribe((apiResponse) => this.schoolDetail.next(apiResponse.data!));
	}

	editSchool(updateSchool: UpdateSchool) {
		const formData = new FormData();

		for (const key in updateSchool) {
			const value = (updateSchool as any)[key];

			if (value instanceof Array) {
				value.forEach((item) => formData.append(key, item));
			} else {
				formData.append(key, value);
			}
		}
		this.schoolApi
			.editSchool(formData)
			.subscribe(() => setTimeout(() => this.router.navigateByUrl('content/school/list'), 1500));
	}

	createSchoolRating(createSchoolRating: CreateSchoolRating) {
		this.schoolApi
			.createSchoolRating(createSchoolRating)
			.subscribe(() => setTimeout(() => window.history.go(-1), 1000));
	}

	getSchoolReminderList(schoolName: string, size: number, page: number) {
		this.schoolApi
			.getSchoolReminderList(schoolName.trim(), size, page)
			.subscribe((apiResponse) => this.schoolReminderList.next(apiResponse.data!));
	}

	approveSchool(schoolId: string) {
		this.schoolApi
			.approveSchool(schoolId)
			.subscribe(() => (this.schoolDetail.getValue().schoolStatusName = 'Approved'));
	}

	rejectSchool(schoolId: string) {
		this.schoolApi.rejectSchool(schoolId).subscribe(() => {
			this.schoolDetail.getValue().schoolStatusName = 'Rejected';
			setTimeout(() => this.router.navigateByUrl('/content/school/list'), 1500);
		});
	}

	publishSchool(schoolId: string) {
		this.schoolApi
			.publishSchool(schoolId)
			.subscribe(() => (this.schoolDetail.getValue().schoolStatusName = 'Published'));
	}

	unPublishSchool(schoolId: string) {
		this.schoolApi
			.unPublishSchool(schoolId)
			.subscribe(() => (this.schoolDetail.getValue().schoolStatusName = 'Unpublished'));
	}

	submitSchool(schoolId: string) {
		this.schoolApi.submitSchool(schoolId).subscribe(() => {
			this.schoolDetail.getValue().schoolStatusName = 'Submitted';
			setTimeout(() => this.router.navigateByUrl('/content/school/list'), 1500);
		});
	}

	getRatingSchool(schoolId: string) {
		this.ratingAPI
			.getRatingSchool(schoolId)
			.subscribe((apiResponse) => this.ratingSchoolList.next(apiResponse.data!));
	}
}
