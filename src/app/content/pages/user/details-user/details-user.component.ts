import { AsyncPipe, DatePipe, NgIf } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, RouterLink, RouterModule } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { ExtendedUserDTO, UserDto } from '../../../../models/users/user-dto';
import { UserService } from '../../../../services/user.service';
import { BreadcrumbComposite } from '../../../../shared/components/breadcrumb/breadcrumb-composite';
import { AuthenticationUtils } from '../../../../utils/authentication-utils';
import { ConfirmModalComponent } from '../../../../shared/components/modal/confirm-modal/confirm-modal.component';
import { UnenrollSchoolRequestModalComponent } from '../../../../shared/components/modal/unenroll-school-request-modal/unenroll-school-request-modal.component';
import { SchoolService } from '../../../../services/school.service';
import { SchoolDTO } from '../../../../models/schools/school-dto';
import { SchoolRequestService } from '../../../../services/school-request.service';
import { SchoolRequestDto } from '../../../../models/requests/school-request-dto';

@Component({
	selector: 'app-details-user',
	standalone: true,
	imports: [
		RouterLink,
		RouterModule,
		NgIf,
		AsyncPipe,
		ConfirmModalComponent,
		UnenrollSchoolRequestModalComponent
	],
	providers: [DatePipe],
	templateUrl: './details-user.component.html',
	styleUrls: ['./details-user.component.scss']
})
export class DetailsUserComponent {
	public breadcrumbComposite = {
		breadCrumbDisplayName: 'Detail User',
		breadCrumbRouterPath: '/content/users/detail'
	} as BreadcrumbComposite;


	@ViewChild(ConfirmModalComponent) modal!: ConfirmModalComponent;
	@ViewChild(UnenrollSchoolRequestModalComponent) unenrollModal!: UnenrollSchoolRequestModalComponent;
	isShoolOwner = false;
	private subscriptions = [] as Subscription[];
	protected schoolDetail = {} as SchoolDTO;
	protected userSchoolRequest = {} as SchoolRequestDto;
	protected enrolledSchoolId = ""; //lay tu HTML
	userDetail = {} as Observable<UserDto>;
	selectedRequestId: string | null = null;
	constructor(
		private userService: UserService,
		protected datePipe: DatePipe,
		private authenticationUtils: AuthenticationUtils,
		private schoolRequestService: SchoolRequestService,
		private route: ActivatedRoute
	) {
		this.isShoolOwner = !!(authenticationUtils.getRoleFromCookie() === 'SchoolOwner');
		this.userDetail = this.userService.userDetails$;
		console.log(this.userDetail);
		this.getUserInformation();
	}

	protected userSchoolData = {} as ExtendedUserDTO;

	protected isUserAuthenticated = false;
	currentId: string | null = null;
	private getUserInformation() {
		this.route.paramMap.subscribe((params) => {
			this.currentId = params.get('id'); // 'id' là tên tham số trong URL
			console.log(this.currentId); // Sử dụng ID theo yêu cầu của bạn

			if (this.currentId) {
				this.userService.getUserById(this.currentId).subscribe();

				const sub = this.userService.userDetails.subscribe((data) => {
					if (data) {
						this.userSchoolData = data;
						console.log(data);
						this.userSchoolData.schoolId = data.enrollSchoolId;
						this.userSchoolData.userId = this.currentId || undefined;
						// this.userSchoolData.enrolledSchool = this.schoolDetail.schoolName;

						if (data.enrollSchoolId) {
							this.schoolRequestService
								.getEnrolledSchoolRequestByEnrolledSchoolId(data.enrollSchoolId)
								.subscribe((responseData) => {
									this.userSchoolRequest = responseData;
								});
						}
					}
				});
				this.subscriptions.push(sub);
			}
		});
	}

	openUnenrollModal(enrolledId:string) {
		this.enrolledSchoolId = enrolledId;
		this.unenrollModal.open();
	}

	handleUnEnrollmentSubmit(data: any) {
		console.log('Form submitted: ', data);
	}

	ngOnDestroy(): void {
		this.subscriptions.forEach((subscription) => subscription.unsubscribe());
	}
	onBackButtonClick() {
		window.history.go(-1);
	}
}
