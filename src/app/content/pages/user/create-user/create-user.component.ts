import { AsyncPipe, CommonModule } from '@angular/common';
import { Component, effect, OnInit, signal } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { Observable } from 'rxjs';
import { CreateUseRequest } from '../../../../models/users/create-user-request';
import { Role } from '../../../../models/users/role';
import { UserService } from '../../../../services/user.service';
import { BreadcrumbComposite } from '../../../../shared/components/breadcrumb/breadcrumb-composite';
import { ModalComponent } from '../../../../shared/components/modal/modal.component';
import { CustomUserFormValidation } from '../../../../utils/custom-user-validation';
import { NgToastService } from 'ng-angular-popup';

@Component({
	selector: 'app-create-user',
	standalone: true,
	templateUrl: './create-user.component.html',
	styleUrls: ['./create-user.component.scss'],
	imports: [AsyncPipe, ReactiveFormsModule, RouterLink, ModalComponent, CommonModule]
})
export class CreateUserComponent implements OnInit {
	public breadcrumbComposite = {
		breadCrumbDisplayName: 'Create User',
		breadCrumbRouterPath: '/content/users/create'
	} as BreadcrumbComposite;

	protected buttonText = '';
	protected modalTitle = '';
	protected modalConfirmQuestion = '';
	protected isCancelButtonClick = signal(false);

	createUserForm = this.formBuilder.group(
		{
			email: ['', [Validators.required, Validators.email]],
			password: [
				'',
				[
					Validators.required,
					Validators.minLength(6),
					Validators.pattern(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*\W)(?!.* ).{8,16}$/)
				]
			],
			rePassword: ['', Validators.required],
			fullName: ['', Validators.required],
			birthDay: ['', Validators.required],
			address: ['', Validators.required],
			gender: ['1'],
			phone: ['', Validators.required],
			role: ['629de601-95bd-451f-a8d3-86373ee9dded', Validators.required],
			isActive: ['1'],
			image: [{} as File]
		},
		{ validators: [CustomUserFormValidation.checkRepassword()] }
	);
	listRole = {} as Observable<Role[]>;

	constructor(private userService: UserService, private formBuilder: FormBuilder, private router: Router) {
		userService.getListRole();
		this.listRole = userService.listRole$;
		this.setButtonState();
	}

	ngOnInit() {}

	onCancelButtonClick() {
		this.router.navigateByUrl('/content/users/list');
	}
	onSubmitForm() {
		if (this.createUserForm.valid) {
			const user = {
				fullName: this.createUserForm.controls.fullName.value,
				dob: this.createUserForm.controls.birthDay.value!,
				address: this.createUserForm.controls.address.value,
				gender: Number.parseInt(this.createUserForm.controls.gender.value!),
				email: this.createUserForm.controls.email.value,
				phoneNumber: this.createUserForm.controls.phone.value,
				password: this.createUserForm.controls.password.value,
				roleId: this.createUserForm.controls.role.value,
				isActive: Boolean(JSON.parse(this.createUserForm.controls.isActive.value!)),
				image: this.createUserForm.controls.image?.value
			} as CreateUseRequest;
			this.userService.createUser(user);
		}
	}
	triggerAllFormError() {
		this.createUserForm.valid === false ? this.createUserForm.markAllAsTouched() : '';
	}
	displaySelectedImage(event: any, elementId: string): void {
		const selectedImage: any = document.getElementById(elementId);
		const fileInput: HTMLInputElement = event.target as HTMLInputElement;

		if (fileInput.files && fileInput.files[0]) {
			const reader: FileReader = new FileReader();

			reader.onload = function (e: ProgressEvent<FileReader>) {
				if (selectedImage != null) {
					selectedImage.src = e.target?.result as string;
				}
			};
			this.createUserForm.patchValue({
				image: fileInput.files[0]
			});
			reader.readAsDataURL(fileInput.files[0]);
		}
	}
	private setButtonState() {
		effect(() => {
			if (this.isCancelButtonClick()) {
				this.buttonText = 'Leave';
				this.modalTitle = 'Leave create user';
				this.modalConfirmQuestion = 'Do you want to leave ?';
			} else {
				this.buttonText = 'Create';
				this.modalTitle = 'Create new user';
				this.modalConfirmQuestion = 'Do you want to create new user ?';
			}
		});
	}
}
