import { DatePipe } from '@angular/common';
import { Component, effect, OnInit, signal } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { updateUseRequest } from '../../../../models/users/update-user-request';
import { UserDto } from '../../../../models/users/user-dto';
import { UserService } from '../../../../services/user.service';
import { BreadcrumbComposite } from '../../../../shared/components/breadcrumb/breadcrumb-composite';
import { ModalComponent } from '../../../../shared/components/modal/modal.component';

@Component({
	selector: 'app-edit-user',
	standalone: true,
	imports: [ReactiveFormsModule, ModalComponent],
	providers: [DatePipe],
	templateUrl: './edit-user.component.html',
	styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
	public breadcrumbComposite = {
		breadCrumbDisplayName: 'Edit User',
		breadCrumbRouterPath: '/content/users/edit'
	} as BreadcrumbComposite;

	protected buttonText = '';
	protected modalTitle = '';
	protected modalConfirmQuestion = '';
	protected isCancelButtonClick = signal(false);
	protected subscription = {} as Subscription;
	protected userId!: string;
	protected Image!: SafeUrl | string;

	updateUserForm = this.formBuilder.group({
		fullName: ['', Validators.required],
		birthDay: ['2004/06/11', Validators.required],
		address: ['', Validators.required],
		gender: [''],
		phone: ['', Validators.required],
		isActive: [''],
		image: [{} as File]
	});

	constructor(
		private userService: UserService,
		private formBuilder: FormBuilder,
		private router: Router,
		private sanitizer: DomSanitizer,
		private datePipe: DatePipe
	) {
		this.setButtonState();
	}

	ngOnInit() {
		this.subscription = this.userService.userDetails$.subscribe((data) => {
			this.bindingValueToForm(data);
		});
	}

	onCancelButtonClick() {
		this.router.navigateByUrl('/content/users/list');
	}

	onSubmitForm() {
		if (this.updateUserForm.valid) {
			const user = {
				id: this.userId!,
				fullName: this.updateUserForm.controls.fullName.value,
				dob: this.updateUserForm.controls.birthDay.value,
				address: this.updateUserForm.controls.address.value,
				gender: Number.parseInt(this.updateUserForm.controls.gender.value!),
				phoneNumber: this.updateUserForm.controls.phone.value,
				isActive: Boolean(JSON.parse(this.updateUserForm.controls.isActive.value!)),
				image: this.updateUserForm.controls.image?.value
			} as updateUseRequest;
			this.userService.editUser(user).subscribe(() => {
				setTimeout(() => this.router.navigateByUrl('content/users/list'), 2000);
			});
		}
	}

	bindingValueToForm(data: UserDto) {
		try {
			this.updateUserForm.controls.fullName.setValue(data.fullName!);
			this.updateUserForm.patchValue({ birthDay: this.datePipe.transform(data.dob!, 'yyyy-MM-dd') });
			this.updateUserForm.controls.address.setValue(data.address!);
			this.updateUserForm.controls.gender.setValue(data.gender.toString());
			this.updateUserForm.controls.phone.setValue(data.phoneNumber!);
			this.updateUserForm.controls.isActive.setValue(data.isActive == true ? '1' : '0');
			this.userId = data.id;
			const selectedImage: any = document.getElementById('selectedAvatar');
			this.Image = this.imageUrl(data.avatar!);
		} catch (error) {}
	}

	displaySelectedImage(event: any, elementId: string): void {
		const selectedImage: any = document.getElementById(elementId);
		const fileInput: HTMLInputElement = event.target as HTMLInputElement;

		if (fileInput.files && fileInput.files[0]) {
			const reader: FileReader = new FileReader();

			reader.onload = function (e: ProgressEvent<FileReader>) {
				if (selectedImage != null) {
					selectedImage.src = e.target?.result as string;
				}
			};
			this.updateUserForm.patchValue({
				image: fileInput.files[0]
			});
			reader.readAsDataURL(fileInput.files[0]);
		}
	}

	imageUrl(imageData: Uint8Array): SafeUrl | string {
		if (imageData) {
			//const base64Image = this.arrayBufferToBase64(imageData);
			return this.sanitizer.bypassSecurityTrustUrl('data:image/png;base64,' + imageData);
		}
		return '';
	}

	triggerAllFormError() {
		this.updateUserForm.valid === false ? this.updateUserForm.markAllAsTouched() : '';
	}

	private setButtonState() {
		effect(() => {
			if (this.isCancelButtonClick()) {
				this.buttonText = 'Leave';
				this.modalTitle = 'Leave edit user';
				this.modalConfirmQuestion = 'Do you want to leave ?';
			} else {
				this.buttonText = 'Edit';
				this.modalTitle = 'Edit user';
				this.modalConfirmQuestion = 'Do you want to edit user ?';
			}
		});
	}

	ngOnDestroy(): void {
		this.bindingValueToForm({} as UserDto);
		this.subscription?.unsubscribe();
	}
}
