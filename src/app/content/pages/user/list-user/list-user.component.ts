import { AsyncPipe, CommonModule, NgOptimizedImage } from '@angular/common';
import { AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';
import { Observable, Subscription } from 'rxjs';
import { Role } from '../../../../models/users/role';
import { UserDto } from '../../../../models/users/user-dto';
import { UserService } from '../../../../services/user.service';
import { BreadcrumbComposite } from '../../../../shared/components/breadcrumb/breadcrumb-composite';
import { ModalComponent } from '../../../../shared/components/modal/modal.component';
import { PagingComponent } from '../../../../shared/components/paging/paging.component';
import { ShowEntryComponent } from '../../../../shared/components/show-entry/show-entry.component';
import { TableComponent } from '../../../../shared/components/table/table.component';
import { PageResult } from './../../../../models/page-result';
import { AuthenticationUtils } from '../../../../utils/authentication-utils';

@Component({
	selector: 'app-list-user',
	standalone: true,
	templateUrl: './list-user.component.html',
	styleUrls: ['./list-user.component.scss'],
	imports: [
		TableComponent,
		AsyncPipe,
		PagingComponent,
		ModalComponent,
		NgOptimizedImage,
		CommonModule,
		ShowEntryComponent
	]
})
export class ListUserComponent implements AfterViewInit, OnDestroy {
	public breadcrumbComposite = {
		breadCrumbDisplayName: 'List User',
		breadCrumbRouterPath: '/content/users/list'
	} as BreadcrumbComposite;

	protected pageResult = {} as Observable<PageResult<UserDto>>;
	currentRole: string = '';
	private searchInput = {} as HTMLInputElement;
	readonly UserHeaders = ['Avatar', 'Full name', 'Email', 'Username', 'Status', 'Email Verify', 'Role '];
	readonly UserKeys = [
		'avatar',
		'fullName',
		'email',
		'userName',
		'isActive',
		'emailConfirmed',
		'roleName'
	] as (keyof UserDto)[];

	startIndex: number = 1;
	keyword: string = '';
	roleId: string | null = null;
	listRole = {} as Observable<Role[]>;
	buttonText: string = 'Delete User';
	modalTitle: string = 'Delete User';
	deleteUserId: string = '';
	roleAssign: string = '';
	modalConfirmQuestion: string = 'Do you want delete user';
	subcriptionChangeStatus = {} as Subscription;
	subcriptionChangeRole = {} as Subscription;

	constructor(
		private userService: UserService,
		private routes: Router,
		private sanitizer: DomSanitizer,
		private toast: NgToastService,
		private authenUtil: AuthenticationUtils
	) {
		this.getUserPaging();
		userService.getListRole();
		this.listRole = userService.listRole$;
		this.currentRole = authenUtil.getRoleFromCookie();
		console.log(this.currentRole);
	}
	ngOnDestroy(): void {
		try {
			this.subcriptionChangeRole?.unsubscribe();
			this.subcriptionChangeStatus?.unsubscribe();
		} catch (e) {}
	}
	showDetail(userId: string) {
		this.routes.navigate(['content/users/detail/', userId]);
	}

	ngAfterViewInit(): void {
		this.setEnterEventForInputSearch();
	}

	calculateStartIndex(pageResult: PageResult<UserDto>) {
		this.startIndex = (pageResult.pageIndex! - 1) * pageResult.pageSize! + 1;
	}

	splitCamelCase(input: string) {
		return input.replace(/([a-z])([A-Z])/g, '$1 $2');
	}

	changeAssigRole($event: any) {
		console.log($event.target.value);
		this.roleAssign = $event.target.value;
	}

	submitRoleAssign(userId: string, currentRole: string) {
		if (this.roleAssign && this.roleAssign != '' && this.roleAssign != currentRole) {
			this.subcriptionChangeRole = this.userService.assignRole(userId, this.roleAssign).subscribe({
				next: () => {
					this.getUserPaging();
				},
				error: () => {
					this.getUserPaging();
				}
			});
			this.roleAssign = '';
		} else {
			this.toast.error({
				detail: `Fail`,
				summary: 'Role is current role',
				duration: 3000
			});
		}
	}

	onEditButtonClick(user: UserDto) {
		this.routes.navigate(['content/users/edit'], {
			queryParams: { id: user.id }
		});
	}
	SearchRole($event: any) {
		const RoleId = $event.target.value;
		if (RoleId && RoleId != 0) {
			this.roleId = RoleId;
		} else {
			this.roleId = null;
		}
		this.getUserPaging();
	}
	changeStatus(UserId: string, statusChange: any) {
		const status = statusChange.target.checked;
		this.subcriptionChangeStatus = this.userService.changeStatus(UserId, status).subscribe({
			next: () => {
				this.getUserPaging();
			},
			error: () => {
				statusChange.target.checked = !status;
				this.getUserPaging();
			}
		});
	}
	onDeleteButtonClick(user: UserDto) {
		this.deleteUserId = user.id;
	}
	deleteUser() {
		this.userService.deleteUser(this.deleteUserId);
		this.getUserPaging();
		this.deleteUserId = '';
	}
	onSearchButtonClick() {
		this.searchInput = document.getElementById('searchInput')! as HTMLInputElement;
		this.keyword = this.searchInput.value;
		this.getUserPaging();
	}
	onCreateButtonClick() {
		this.routes.navigate(['/content/users/create']);
	}

	getCurrentPage($event: number) {
		this.getUserPaging($event);
	}

	getUserPaging(pageIndex: number = 1) {
		this.userService.getUserPaging(this.keyword, pageIndex, 5, this.roleId);
		this.pageResult = this.userService.listUser$;
		console.log(this.pageResult);
	}
	private setEnterEventForInputSearch() {
		this.searchInput = document.getElementById('searchInput')! as HTMLInputElement;

		this.searchInput.addEventListener('keypress', (event) => {
			if (event.key === 'Enter') {
				event.preventDefault();
				this.keyword = this.searchInput.value;
				this.getUserPaging();
			}
		});
	}
	imageUrl(imageData: Uint8Array): SafeUrl | string {
		if (imageData) {
			return this.sanitizer.bypassSecurityTrustUrl('data:image/png;base64,' + imageData);
		}
		return '';
	}
}
