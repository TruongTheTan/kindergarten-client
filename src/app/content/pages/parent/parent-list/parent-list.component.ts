import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { PageResult } from '../../../../models/page-result';
import { UserDto } from '../../../../models/users/user-dto';
import { UserService } from '../../../../services/user.service';
import { BreadcrumbComposite } from '../../../../shared/components/breadcrumb/breadcrumb-composite';
import { PagingComponent } from '../../../../shared/components/paging/paging.component';
import { DateFormatPipe } from '../../../../shared/pipes/date-format-pipe';

@Component({
	selector: 'app-parent-list',
	standalone: true,
	imports: [PagingComponent, DateFormatPipe, CommonModule],
	templateUrl: './parent-list.component.html',
	styleUrl: './parent-list.component.scss'
})
export class ParentListComponent implements OnDestroy, AfterViewInit {
	public breadcrumbComposite = {
		breadCrumbDisplayName: 'Parent List',
		breadCrumbRouterPath: '/content/parent/list'
	} as BreadcrumbComposite;

	protected startIndex = 0;
	protected subscription: Subscription | undefined;
	protected pageResult = {} as PageResult<UserDto>;

	private searchInput = {} as HTMLInputElement;

	constructor(private parentService: UserService) {
		parentService.getParent('', 1, 5, '629de601-95bd-451f-a8d3-86373ee9dded');

		this.subscription = parentService.listUser$.subscribe((result) => {
			this.pageResult = result;
			this.startIndex = (this.pageResult!.pageIndex! - 1) * this.pageResult!.pageSize! + 1;
		});
	}

	ngAfterViewInit(): void {
		this.searchInput = document.getElementById('searchInput')! as HTMLInputElement;
		this.setEnterEventForInputSearch();
	}

	ngOnDestroy(): void {
		this.subscription?.unsubscribe();
	}

	protected getCurrentPage(currentPage: number) {
		this.parentService.getParent('', currentPage, 5, '629de601-95bd-451f-a8d3-86373ee9dded');
	}

	protected onSearchButtonClick() {
		this.parentService.getParent(this.searchInput.value, 1, 5, '629de601-95bd-451f-a8d3-86373ee9dded');
	}

	protected getAllRemindersOnInputEvent() {
		if (this.searchInput.value.trim() === '') {
			this.parentService.getParent('', 1, 5, '629de601-95bd-451f-a8d3-86373ee9dded');
		}
	}

	protected getStatusColorByName(statusName: string) {
		return statusName.trim() === 'Approved' ? 'color: #F2D21E' : '';
	}

	private setEnterEventForInputSearch() {
		this.searchInput.addEventListener('keypress', (event) => {
			if (event.key === 'Enter') {
				event.preventDefault();
				this.parentService.getParent(this.searchInput.value, 1, 5, '629de601-95bd-451f-a8d3-86373ee9dded');
			}
		});
	}
}
