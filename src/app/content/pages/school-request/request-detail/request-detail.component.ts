import { Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
	SchoolRequestCategoryEnum,
	SchoolRequestDto,
	SchoolRequestStatusEnum
} from '../../../../models/requests/school-request-dto';
import { SchoolRequestService } from '../../../../services/school-request.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmModalComponent } from '../../../../shared/components/modal/confirm-modal/confirm-modal.component';
import { BreadcrumbComposite } from '../../../../shared/components/breadcrumb/breadcrumb-composite';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
@Component({
	selector: 'app-request-detail',
	standalone: true,
	imports: [CKEditorModule, FormsModule, CommonModule, ConfirmModalComponent],
	templateUrl: './request-detail.component.html',
	styleUrl: './request-detail.component.scss'
})
export class RequestDetailComponent implements OnInit, OnDestroy {
	public breadcrumbComposite = {
		breadCrumbDisplayName: 'Request Detail',
		breadCrumbRouterPath: '/content/school-request/list'
	} as BreadcrumbComposite;
	editor = ClassicEditor;


	@ViewChild(ConfirmModalComponent) modal!: ConfirmModalComponent;
	subScription: Subscription = {} as Subscription;
	schoolRequest: SchoolRequestDto = {
		id: '',
		fullName: '',
		email: '',
		phone: '',
		address: '',
		content: '',
		resolveCount: 0,
		schoolRequestCategoryId: '',
		schoolRequestStatusId: '',
		schoolId: '',
		school: { id: '', schoolName: '', email: '', feeFrom: 0, feeTo: 0, phone: '', rating: 0 }
	};
	selectedStatus: string | undefined;
	schoolOwnerResponse: string = '';
	isSchoolRequestValid: boolean = false;
	isEditable: boolean = false;
	isEdit: boolean = false;

	constructor(private route: ActivatedRoute, private schoolRequestService: SchoolRequestService) {}
	ngOnDestroy(): void {
		this.subScription.unsubscribe();
	}

	ngOnInit(): void {
		this.subScription = this.route.queryParamMap.subscribe((param) => {
			const requestId = param.get('id');
			this.isSchoolRequestValid = false;
			if (requestId) {
				this.schoolRequestService.getSchoolRequest(requestId).subscribe({
					next: (request) => {
						this.schoolRequest = request;
						this.schoolOwnerResponse = this.schoolRequest.response ?? '';
						this.selectedStatus =
							request.schoolRequestStatusId === SchoolRequestStatusEnum.Pending
								? 'Pending'
								: request.schoolRequestStatusId === SchoolRequestStatusEnum.Approved
								? 'Approved'
								: 'Rejected';
						this.isSchoolRequestValid = true;
						this.isEditable = this.selectedStatus === 'Pending' && !request.updateAt;
					},
					error: (err) => {
						console.error(err);
						this.isSchoolRequestValid = false;
					}
				});
			}
		});
	}

	openConfirmModal() {
		this.modal.modalTitle = 'Mark request resolved';
		this.modal.modalConfirmQuestion = 'Are you sure you want to mark as resolved?';
		this.modal.buttonText = this.isEdit ? 'Mark as edit resolved' : 'Mark as resolved';
		this.modal.open();
	}

	markAsResolved() {
		if (this.schoolRequest) {
			this.schoolRequest.schoolRequestStatusId =
				this.selectedStatus === 'Approved'
					? SchoolRequestStatusEnum.Approved
					: this.selectedStatus === 'Rejected'
					? SchoolRequestStatusEnum.Rejected
					: SchoolRequestStatusEnum.Pending;

			this.schoolRequestService
				.resolveSchoolRequest(
					this.schoolRequest.id,
					this.schoolOwnerResponse,
					this.selectedStatus === 'Approved' ||
						this.getCategoryName(this.schoolRequest.schoolRequestCategoryId) === 'Consultant'
				)
				.subscribe({
					next: (response) => {
						console.log('Request resolved', response);
						this.isEditable = false;
					},
					error: (err) => console.error('Error resolving request', err)
				});
		}
	}

	editRequest() {
		this.isEditable = true;
		this.isEdit = true;
	}

	getStatusName(statusId: string | undefined): string {
		switch (statusId) {
			case SchoolRequestStatusEnum.Pending:
				return 'Pending';
			case SchoolRequestStatusEnum.Approved:
				return 'Approved';
			case SchoolRequestStatusEnum.Rejected:
				return 'Rejected';
			default:
				return 'Unknown';
		}
	}

	getCategoryName(categoryId: string | undefined): string {
		switch (categoryId) {
			case SchoolRequestCategoryEnum.Enroll:
				return 'Enroll';
			case SchoolRequestCategoryEnum.UnEnroll:
				return 'Unenroll';
			case SchoolRequestCategoryEnum.Consultant:
				return 'Consultant';
			case SchoolRequestCategoryEnum.Admin:
				return 'Admin';
			default:
				return 'Unknown';
		}
	}
}
