import { CommonModule } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { PageResult } from '../../../../models/page-result';
import { SchoolRequestDto, SchoolRequestStatusEnum } from '../../../../models/requests/school-request-dto';
import { SchoolRequestService } from '../../../../services/school-request.service';
import { BreadcrumbComposite } from '../../../../shared/components/breadcrumb/breadcrumb-composite';
import { ConfirmModalComponent } from '../../../../shared/components/modal/confirm-modal/confirm-modal.component';
import { PagingComponent } from '../../../../shared/components/paging/paging.component';
import { ShowEntryComponent } from '../../../../shared/components/show-entry/show-entry.component';

@Component({
	selector: 'app-request-counseling-list',
	standalone: true,
	imports: [CommonModule, PagingComponent, FormsModule, ConfirmModalComponent, ShowEntryComponent],
	templateUrl: './request-counseling-list.component.html',
	styleUrl: '../../../../shared/components/table/table.component.scss'
})
export class RequestCounselingListComponent {
	public breadcrumbComposite = {
		breadCrumbDisplayName: 'Counseling Request List',
		breadCrumbRouterPath: '/content/school-request/list'
	} as BreadcrumbComposite;

	@ViewChild(ConfirmModalComponent) modal!: ConfirmModalComponent;

	public schoolRequestList$!: Observable<PageResult<SchoolRequestDto>>;
	private searchInput!: HTMLInputElement;
	private subscription = new Subscription();
	public pageResult: PageResult<SchoolRequestDto> = {} as PageResult<SchoolRequestDto>;

	startIndex: number = 0;
	pageSize: number = 5;
	keyword: string = '';
	categoryFilter: string = '';
	statusFilter: string = '';
	sortBy: string = '';
	sortDirection: string = 'asc'; // or 'desc'

	readonly schoolRequestHeaders = ['Full Name', 'Email', 'Phone No.', 'Status', 'Date Created'];
	readonly schoolRequestStatuses = ['Pending', 'Approved'];
	constructor(private schoolRequestService: SchoolRequestService, private router: Router) {}

	ngOnInit(): void {
		this.getRequestListPaging();
	}

	ngAfterViewInit(): void {
		this.setEnterEventForInputSearch();
	}

	openConfirmModal() {
		this.modal.modalTitle = 'Delete Counselling Request';
		this.modal.modalConfirmQuestion = 'Are you sure you want to delete these Counseling request(s)?';
		this.modal.buttonText = 'Delete';
		this.modal.open();
	}

	toggleAll(event: Event): void {
		const checked = (event.target as HTMLInputElement).checked;
		this.pageResult.items?.forEach((item) => (item.selected = checked));
	}

	deleteSelected(): void {
		const selectedIds = this.pageResult.items?.filter((item) => item.selected).map((item) => item.id) || [];
		if (selectedIds.length > 0) {
			this.schoolRequestService.deleteCounselingRequest(selectedIds).subscribe(() => {
				this.getRequestListPaging();
			});
		}
	}

	onSearchButtonClick() {
		this.searchInput = document.getElementById('searchInput')! as HTMLInputElement;
		this.keyword = this.searchInput.value;
		this.getRequestListPaging();
	}

	onCategoryFilterChange(event: Event) {
		this.categoryFilter = (event.target as HTMLSelectElement).value;
		this.getRequestListPaging();
	}

	onStatusFilterChange(event: Event) {
		this.statusFilter = (event.target as HTMLSelectElement).value;
		this.getRequestListPaging();
	}

	onSort(column: string) {
		if (this.sortBy === column) {
			this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
		} else {
			this.sortBy = column;
			this.sortDirection = 'asc';
		}
		this.getRequestListPaging();
	}

	getRequestListPaging(pageIndex: number = 1) {
		this.schoolRequestService.getCounselingSchoolRequests(
			this.keyword,
			this.categoryFilter,
			this.statusFilter,
			this.sortBy,
			this.sortDirection,
			pageIndex,
			this.pageSize
		);
		this.subscription = this.schoolRequestService.schoolCounselingRequestList$.subscribe((data) => {
			this.pageResult = data ?? { items: [], pageIndex: 1, pageSize: 5, totalRecords: 0, pageCount: 0 };
			this.startIndex = (this.pageResult!.pageIndex! - 1) * this.pageResult!.pageSize! + 1;
		});
	}

	getCurrentPage(index: number) {
		this.getRequestListPaging(index);
	}

	getStatusName(statusId: string): string {
		switch (statusId) {
			case SchoolRequestStatusEnum.Pending:
				return 'Pending';
			case SchoolRequestStatusEnum.Approved:
				return 'Approved';
			case SchoolRequestStatusEnum.Rejected:
				return 'Rejected';
			default:
				return 'Unknown';
		}
	}

	markAsResolvedNavigate(id: string): void {
		this.router.navigateByUrl('/content/school-request/detail?id=' + id);
	}

	private setEnterEventForInputSearch(): void {
		this.searchInput = document.querySelector('#searchInput') as HTMLInputElement;
		this.searchInput.addEventListener('keypress', (event: KeyboardEvent) => {
			if (event.key === 'Enter') {
				this.onSearchButtonClick();
			}
		});
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}
}
