import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { PageResult } from '../../../../models/page-result';
import {
	SchoolRequestCategoryEnum,
	SchoolRequestDto,
	SchoolRequestStatusEnum
} from '../../../../models/requests/school-request-dto';
import { RequestHubService } from '../../../../services/hubs/requestHub.service';
import { SchoolRequestService } from '../../../../services/school-request.service';
import { BreadcrumbComposite } from '../../../../shared/components/breadcrumb/breadcrumb-composite';
import { PagingComponent } from '../../../../shared/components/paging/paging.component';
import { ShowEntryComponent } from '../../../../shared/components/show-entry/show-entry.component';
import { TableComponent } from '../../../../shared/components/table/table.component';

@Component({
	selector: 'app-request-list',
	standalone: true,
	imports: [CommonModule, TableComponent, PagingComponent, ShowEntryComponent],
	templateUrl: './request-list.component.html',
	styleUrl: '../../../../shared/components/table/table.component.scss',
})
export class RequestListComponent implements OnInit {
	public breadcrumbComposite = {
		breadCrumbDisplayName: 'Enrollment Request List',
		breadCrumbRouterPath: '/content/school-request/list'
	} as BreadcrumbComposite;

	public schoolRequestList$!: Observable<PageResult<SchoolRequestDto>>;
	private searchInput!: HTMLInputElement;
	private subscription = new Subscription();
	public pageResult: PageResult<SchoolRequestDto> = {} as PageResult<SchoolRequestDto>;

	protected startIndex = 0;

	pageSize: number = 5;
	keyword: string = '';
	categoryFilter: string = '';
	statusFilter: string = '';
	sortBy: string = '';
	sortDirection: string = 'asc'; // or 'desc'

	fromDate: string | null = null;
	endDate: string | null = null;

	readonly schoolRequestHeaders = ['Full Name', 'Email', 'Phone No.', 'Category', 'Status', 'Date Created'];
	readonly schoolRequestCategories = ['Enroll', 'UnEnroll'];
	readonly schoolRequestStatuses = ['Pending', 'Approved', 'Rejected'];

	constructor(
		private schoolRequestService: SchoolRequestService,
		private router: Router,
		private requestHubService: RequestHubService
	) {}

	ngOnInit(): void {
		this.getRequestListPaging();

		this.requestHubService.notification$.subscribe((mess) => {
			// debugger
			console.log('Notification received:', mess);
			this.getRequestListPaging();
		});
	}

	ngAfterViewInit(): void {
		this.setEnterEventForInputSearch();
	}

	onSearchButtonClick() {
		this.searchInput = document.getElementById('searchInput')! as HTMLInputElement;
		this.keyword = this.searchInput.value;
		this.getRequestListPaging();
	}

	onFromDateChange(event: Event) {
		this.fromDate = (event.target as HTMLInputElement).value;
		this.getRequestListPaging();
	}

	onEndDateChange(event: Event) {
		this.endDate = (event.target as HTMLInputElement).value;
		this.getRequestListPaging();
	}

	onCategoryFilterChange(event: Event) {
		this.categoryFilter = (event.target as HTMLSelectElement).value;
		this.getRequestListPaging();
	}

	onStatusFilterChange(event: Event) {
		this.statusFilter = (event.target as HTMLSelectElement).value;
		this.getRequestListPaging();
	}

	onSort(column: string) {
		if (this.sortBy === column) {
			this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
		} else {
			this.sortBy = column;
			this.sortDirection = 'asc';
		}
		this.getRequestListPaging();
	}

	getRequestListPaging(pageIndex: number = 1) {
		this.schoolRequestService.getEnrollmentSchoolRequests(
			this.keyword,
			this.categoryFilter,
			this.statusFilter,
			this.sortBy,
			this.sortDirection,
			this.fromDate ? this.fromDate : null,
			this.endDate ? this.endDate : null,
			pageIndex,
			this.pageSize
		);
		this.subscription = this.schoolRequestService.schoolEnrollRequestList$.subscribe((data) => {
			this.pageResult = data ?? { items: [], pageIndex: 1, pageSize: 5, totalRecords: 0, pageCount: 0 };
			this.startIndex = (this.pageResult!.pageIndex! - 1) * this.pageResult!.pageSize! + 1;
		});
	}

	getCurrentPage(index: number) {
		this.getRequestListPaging(index);
	}

	getCategoryName(categoryId: string): string {
		switch (categoryId) {
			case SchoolRequestCategoryEnum.Enroll:
				return 'Enroll';
			case SchoolRequestCategoryEnum.UnEnroll:
				return 'Unenroll';
			default:
				return 'Unknown';
		}
	}

	getStatusName(statusId: string): string {
		switch (statusId) {
			case SchoolRequestStatusEnum.Pending:
				return 'Pending';
			case SchoolRequestStatusEnum.Approved:
				return 'Approved';
			case SchoolRequestStatusEnum.Rejected:
				return 'Rejected';
			default:
				return 'Unknown';
		}
	}

	markAsResolvedNavigate(id: string): void {
		this.router.navigateByUrl('/content/school-request/detail?id=' + id);
	}

	private setEnterEventForInputSearch(): void {
		this.searchInput = document.querySelector('#searchInput') as HTMLInputElement;
		this.searchInput.addEventListener('keypress', (event: KeyboardEvent) => {
			if (event.key === 'Enter') {
				this.onSearchButtonClick();
			}
		});
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}
}
