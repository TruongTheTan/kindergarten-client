import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { PageResult } from '../../../../models/page-result';
import { SchoolReminder } from '../../../../models/schools/school-reminder';
import { BreadcrumbComposite } from '../../../../shared/components/breadcrumb/breadcrumb-composite';
import { PagingComponent } from '../../../../shared/components/paging/paging.component';
import { ShowEntryComponent } from '../../../../shared/components/show-entry/show-entry.component';
import { DateFormatPipe } from '../../../../shared/pipes/date-format-pipe';
import { SchoolUtils } from '../../../../utils/school-utils';
import { SchoolService } from './../../../../services/school.service';

@Component({
	selector: 'app-school-review-reminder',
	standalone: true,
	imports: [PagingComponent, DateFormatPipe, CommonModule, ShowEntryComponent],
	templateUrl: './school-review-reminder.component.html',
	styleUrl: './school-review-reminder.component.scss'
})
export class SchoolReviewReminderComponent implements OnDestroy, AfterViewInit {
	public breadcrumbComposite = {
		breadCrumbDisplayName: 'School Reminder',
		breadCrumbRouterPath: '/content/school/reminder'
	} as BreadcrumbComposite;

	protected startIndex = 0;
	protected subscription: Subscription | undefined;
	protected pageResult = {} as PageResult<SchoolReminder>;

	private searchInput = {} as HTMLInputElement;

	constructor(private schoolService: SchoolService) {
		schoolService.getSchoolReminderList('', 5, 1);

		this.subscription = schoolService.schoolReminderList$.subscribe((result) => {
			this.pageResult = result;
			this.startIndex = (this.pageResult!.pageIndex! - 1) * this.pageResult!.pageSize! + 1;
		});
	}

	ngAfterViewInit(): void {
		this.searchInput = document.getElementById('searchInput')! as HTMLInputElement;
		this.setEnterEventForInputSearch();
	}

	ngOnDestroy(): void {
		this.subscription?.unsubscribe();
	}

	protected getCurrentPage(currentPage: number) {
		this.schoolService.getSchoolReminderList('', 5, currentPage);
	}

	protected onSearchButtonClick() {
		this.schoolService.getSchoolReminderList(this.searchInput.value, 5, 1);
	}

	protected getAllRemindersOnInputEvent() {
		if (this.searchInput.value.trim() === '') {
			this.schoolService.getSchoolReminderList('', 5, 1);
		}
	}

	protected getStatusColor(statusName: string) {
		return SchoolUtils.getStatusColor(statusName);
	}

	private setEnterEventForInputSearch() {
		this.searchInput.addEventListener('keypress', (event) => {
			if (event.key === 'Enter') {
				event.preventDefault();
				this.schoolService.getSchoolReminderList(this.searchInput.value, 5, 1);
			}
		});
	}
}
