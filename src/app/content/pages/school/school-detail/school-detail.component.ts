import { AsyncPipe, CommonModule } from '@angular/common';
import { Component, computed, OnDestroy, OnInit, signal } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Observable, Subscription } from 'rxjs';
import { District, Province, Ward } from '../../../../models/locations/location';
import { schoolFacilities, schoolUtilities } from '../../../../models/school-master-data';
import { SchoolDTO } from '../../../../models/schools/school-dto';
import { SchoolService } from '../../../../services/school.service';
import { BreadcrumbComposite } from '../../../../shared/components/breadcrumb/breadcrumb-composite';
import { ModalComponent } from '../../../../shared/components/modal/modal.component';
import { AuthenticationUtils } from '../../../../utils/authentication-utils';
import { SchoolUtils } from '../../../../utils/school-utils';

@Component({
	selector: 'app-school-detail',
	standalone: true,
	imports: [CKEditorModule, ReactiveFormsModule, CommonModule, AsyncPipe, ModalComponent],
	templateUrl: 'school-detail.component.html',
	styleUrl: 'school-detail.component.scss'
})
export class SchoolDetailComponent implements OnInit, OnDestroy {
	editor = ClassicEditor;

	schoolUtils = SchoolUtils;

	readonly deleteModalObj: ModalObject = {
		buttonText: 'Delete',
		modalTitle: 'Delete school',
		buttonColor: 'btn-danger',
		modalConfirmQuestion: 'Do you want to delete this school ?',
		modalButtonClick: function (): void {}
	};

	readonly rejectModalObj: ModalObject = {
		buttonText: 'Reject',
		modalTitle: 'Reject school',
		buttonColor: 'btn-danger',
		modalConfirmQuestion: 'Do you want to reject this school ?',
		modalButtonClick: function (): void {
			console.log('asdf43r4fe');
		}
	};

	readonly approveModalObj: ModalObject = {
		buttonText: 'Approve',
		modalTitle: 'Approve school',
		buttonColor: 'btn-primary',
		modalConfirmQuestion: 'Do you want to approve this school ?',
		modalButtonClick: function (): void {
			console.log('asdf43r4fe');
		}
	};

	readonly publishModalObj: ModalObject = {
		buttonText: 'Publish',
		modalTitle: 'Publish school',
		buttonColor: 'btn-primary',
		modalConfirmQuestion: 'Do you want to publish this school ?',
		modalButtonClick: function (): void {
			console.log('asdf43r4fe');
		}
	};

	readonly unPublishModalObj: ModalObject = {
		buttonText: 'Un-publish',
		modalTitle: 'Un-publish school',
		buttonColor: 'btn-primary',
		modalConfirmQuestion: 'Do you want to unpublish this school ?',
		modalButtonClick: function (): void {
			console.log('asdf43r4fe');
		}
	};

	readonly submitModalObj: ModalObject = {
		buttonText: 'Submit',
		modalTitle: 'Submit school for approval',
		buttonColor: 'btn-primary',
		modalConfirmQuestion: 'Do you want to submit this school ?',
		modalButtonClick: function (): void {
			console.log('asdf43r4fe');
		}
	};

	public breadcrumbComposite = {
		breadCrumbDisplayName: 'School details',
		breadCrumbRouterPath: '/content/school/detail'
	} as BreadcrumbComposite;

	protected subscriptions: Subscription[] | null | undefined;

	protected schoolDetail = {} as SchoolDTO;
	protected provinceList = {} as Observable<Province[]>;
	protected districtList = {} as Observable<District[]>;
	protected wardList = {} as Observable<Ward[]>;

	protected utilities = schoolUtilities.map((f) => f.name);
	protected facilities = schoolFacilities.map((f) => f.name);

	protected userId = signal('');

	protected isAdmin = signal(false);
	protected isSchool = signal(false);
	protected isDeleteButtonClick = signal(false);
	protected isRejectButtonClick = signal(false);
	protected isApproveButtonClick = signal(false);
	protected isPublishButtonClick = signal(false);
	protected isUnPublishButtonClick = signal(false);
	protected isSubmitButtonClick = signal(false);

	protected modalObject = computed(() => {
		if (this.isDeleteButtonClick()) {
			return this.deleteModalObj;
		} else if (this.isRejectButtonClick()) {
			return this.rejectModalObj;
		} else if (this.isApproveButtonClick()) {
			return this.approveModalObj;
		} else if (this.isPublishButtonClick()) {
			return this.publishModalObj;
		} else if (this.isUnPublishButtonClick()) {
			return this.unPublishModalObj;
		} else {
			return this.submitModalObj;
		}
	});

	// Form
	protected schoolDetailForm = this.formBuilder.group({
		wardId: ['', Validators.required],
		districtId: ['', Validators.required],
		provinceId: ['', Validators.required]
	});

	constructor(
		private formBuilder: FormBuilder,
		private schoolService: SchoolService,
		private authenticationUtils: AuthenticationUtils
	) {
		this.loadAllProvinces();
		this.loadDistrictByProvince();
		this.loadWardByDistrict();
	}

	ngOnInit() {
		const subscription = this.schoolService.schoolDetail$.subscribe((data) => {
			this.schoolDetail = data;
			this.bindingValueToForm(data);
		});
		this.subscriptions?.push(subscription);

		const role = this.authenticationUtils.getRoleFromCookie();
		const userId = this.authenticationUtils.getUserIdFromCookie();

		this.isAdmin.set(role === 'Admin' || role === 'SuperAdmin');
		this.userId.set(userId);
	}

	ngOnDestroy(): void {
		this.bindingValueToForm({} as SchoolDTO);
		this.subscriptions?.forEach((sub) => sub.unsubscribe());
	}

	private bindingValueToForm(data: SchoolDTO) {
		this.schoolDetailForm.patchValue({
			wardId: data.wardId,
			districtId: data.districtId,
			provinceId: data.provinceId
		});
	}

	private loadAllProvinces() {
		this.schoolService.getAllProvince();
		this.provinceList = this.schoolService.provinceList$;
	}

	private loadDistrictByProvince() {
		this.districtList = this.schoolService.districtList$;

		const sub = this.schoolDetailForm.controls.provinceId.valueChanges.subscribe((value) =>
			this.schoolService.getAllDistrictByProvinceCode(value!)
		);
		this.subscriptions?.push(sub);
	}

	private loadWardByDistrict() {
		this.wardList = this.schoolService.wardList$;

		const sub = this.schoolDetailForm.controls.districtId.valueChanges.subscribe((value) =>
			this.schoolService.getAllWardByDistrictCode(value!)
		);
		this.subscriptions?.push(sub);
	}

	/******************************* Button events below **********************************/

	onButtonClickEvent() {
		if (this.isDeleteButtonClick()) {
			this.onDeleteButtonClick();
		} else if (this.isRejectButtonClick()) {
			this.onRejectButtonClick();
		} else if (this.isApproveButtonClick()) {
			this.onApproveButtonClick();
		} else if (this.isPublishButtonClick()) {
			this.onPublishButtonClick();
		} else if (this.isUnPublishButtonClick()) {
			this.onUnPublishButtonClick();
		} else {
			this.onSubmitButtonClick();
		}
	}

	private onDeleteButtonClick() {
		this.schoolService.deleteSchool(this.schoolDetail.id);
	}

	private onRejectButtonClick() {
		this.schoolService.rejectSchool(this.schoolDetail.id);
	}

	private onApproveButtonClick() {
		this.schoolService.approveSchool(this.schoolDetail.id);
	}

	private onPublishButtonClick() {
		this.schoolService.publishSchool(this.schoolDetail.id);
	}

	private onUnPublishButtonClick() {
		this.schoolService.unPublishSchool(this.schoolDetail.id);
	}

	private onSubmitButtonClick() {
		this.schoolService.submitSchool(this.schoolDetail.id);
	}

	protected getStatusColor(statusName: string) {
		return this.schoolUtils.getStatusColor(statusName);
	}
}

interface ModalObject {
	buttonText: string;
	modalTitle: string;
	buttonColor: string;
	modalConfirmQuestion: string;
	modalButtonClick: () => void;
}
