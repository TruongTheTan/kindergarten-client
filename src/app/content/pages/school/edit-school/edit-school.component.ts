import { AsyncPipe, CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, computed, OnDestroy, OnInit, signal } from '@angular/core';
import { FormArray, FormBuilder, FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Observable, Subscription } from 'rxjs';
import { District, Province, Ward } from '../../../../models/locations/location';
import {
	schoolEducationMethods,
	schoolFacilities,
	schoolLanguages,
	schoolTypes,
	schoolUtilities
} from '../../../../models/school-master-data';
import { SchoolDTO } from '../../../../models/schools/school-dto';
import { UpdateSchool } from '../../../../models/schools/update-school';
import { SchoolService } from '../../../../services/school.service';
import { BreadcrumbComposite } from '../../../../shared/components/breadcrumb/breadcrumb-composite';
import { ModalComponent } from '../../../../shared/components/modal/modal.component';
import { AuthenticationUtils } from '../../../../utils/authentication-utils';
import { CustomFormValidation } from '../../../../utils/custom-form-validation';

@Component({
	selector: 'app-edit-school',
	standalone: true,
	changeDetection: ChangeDetectionStrategy.OnPush,
	imports: [CKEditorModule, ReactiveFormsModule, CommonModule, AsyncPipe, ModalComponent],
	templateUrl: 'edit-school.component.html',
	styleUrl: 'edit-school.component.scss'
})
export class EditSchoolComponent implements OnInit, OnDestroy {
	editor = ClassicEditor;

	public breadcrumbComposite = {
		breadCrumbDisplayName: 'Edit School',
		breadCrumbRouterPath: '/content/school/edit'
	} as BreadcrumbComposite;

	protected isCancelButtonClick = signal(false);
	protected isEditButtonClick = signal(false);

	protected modalObject = computed(() => {
		const modal = {} as {
			buttonText: string;
			modalTitle: string;
			modalConfirmQuestion: string;
			buttonColor: string;
		};

		if (this.isCancelButtonClick()) {
			modal.buttonText = 'Leave';
			modal.modalTitle = 'Leave edit school';
			modal.modalConfirmQuestion = 'Do you want to leave ?';
			modal.buttonColor = 'btn-danger';
		} else {
			modal.buttonText = 'Edit';
			modal.modalTitle = 'Edit the school';
			modal.modalConfirmQuestion = 'Do you want to edit this school ?';
			modal.buttonColor = 'btn-primary';
		}
		return modal;
	});

	protected subscriptions: Subscription[] = [];

	protected schoolStatusName = '';
	protected provinceList = {} as Observable<Province[]>;
	protected districtList = {} as Observable<District[]>;
	protected wardList = {} as Observable<Ward[]>;

	protected types = schoolTypes;
	protected utilities = schoolUtilities;
	protected languages = schoolLanguages;
	protected facilities = schoolFacilities;
	protected educationMethods = schoolEducationMethods;

	// Form
	protected editSchoolForm = this.formBuilder.group(
		{
			id: [''],
			schoolName: ['', Validators.required],
			email: ['', [Validators.required, Validators.email]],
			phone: ['', Validators.required],
			schoolTypeId: ['', Validators.required],
			educationMethodId: ['', Validators.required],
			languageId: ['', Validators.required],
			feeFrom: ['', [Validators.required, Validators.min(1)]],
			feeTo: ['', [Validators.required, Validators.min(1)]],
			childReceivingAgeId: ['', Validators.required],
			schoolIntroduce: [''],
			specificAddress: ['', Validators.required],
			wardId: ['', Validators.required],
			districtId: ['', Validators.required],
			provinceId: ['', Validators.required],
			facilityIDs: this.formBuilder.array([] as string[]),
			utilityIDs: this.formBuilder.array([] as string[]),
			images: [[] as File[]],
			statusId: ['']
		},
		{
			validators: [CustomFormValidation.feeRangeValidator()]
		}
	);

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private formBuilder: FormBuilder,
		private schoolService: SchoolService,
		private authenticationUtils: AuthenticationUtils
	) {
		this.addCheckboxes();
		this.loadAllProvinces();
		this.loadDistrictByProvince();
		this.loadWardByDistrict();
	}

	ngOnInit() {
		const subscription = this.route.queryParams.subscribe((params) => {
			this.schoolService.getDetailSchoolById(params['id'] as string, false);

			const subscription = this.schoolService.schoolDetail$.subscribe((data) => {
				this.schoolStatusName = data.schoolStatusName!;
				this.bindingValueToForm(data);
			});

			this.subscriptions.push(subscription);
		});
		this.subscriptions.push(subscription);
	}

	ngOnDestroy(): void {
		this.bindingValueToForm({} as SchoolDTO);
		this.subscriptions.forEach((subscription) => subscription.unsubscribe());
	}

	triggerAllFormError() {
		this.editSchoolForm.valid === false ? this.editSchoolForm.markAllAsTouched() : '';
	}

	onModalButtonClick() {
		if (this.isEditButtonClick()) {
			this.onSubmitForm();
		} else {
			this.router.navigateByUrl('/content/school/list');
		}
	}

	private onSubmitForm() {
		if (this.editSchoolForm.invalid) {
		} else {
			this.addUtilityIdToFormArray();
			this.addFacilityIdToFormArray();

			const {
				id,
				schoolName,
				childReceivingAgeId,
				districtId,
				educationMethodId,
				email,
				feeFrom,
				feeTo,
				images,
				languageId,
				phone,
				provinceId,
				schoolIntroduce,
				schoolTypeId,
				specificAddress,
				wardId,
				facilityIDs,
				utilityIDs,
				statusId
			} = this.editSchoolForm.controls;

			const updateSchool = {
				id: id.value,
				childReceivingAgeId: childReceivingAgeId.value,
				districtId: districtId.value,
				educationMethodId: educationMethodId.value,
				email: email.value,
				feeFrom: +feeFrom.value!,
				feeTo: +feeTo.value!,
				languageId: languageId.value,
				phone: phone.value,
				provinceId: provinceId.value,
				schoolIntroduce: schoolIntroduce.value,
				schoolName: schoolName.value,
				schoolTypeId: schoolTypeId.value,
				specificAddress: specificAddress.value,
				wardId: wardId.value,
				images: images.value,
				facilityIDs: facilityIDs.value,
				utilityIDs: utilityIDs.value,
				statusId: statusId.value
			} as UpdateSchool;

			updateSchool.updatedBy = this.authenticationUtils.getUserIdFromCookie();
			updateSchool.createdBy = updateSchool.updatedBy;
			this.schoolService.editSchool(updateSchool);
		}
	}

	addEventChangeToInputImageField(event: Event) {
		const inputFileField = event.target as HTMLInputElement;

		for (let i = 0; i < inputFileField.files!.length; i++) {
			const imageFile = inputFileField.files?.item(i);
			this.editSchoolForm.controls.images.value?.push(imageFile!);
		}
	}

	private addCheckboxes() {
		this.facilities.forEach(() => {
			const control = new FormControl();
			this.editSchoolForm.controls.facilityIDs.push(control);
		});

		this.utilities.forEach(() => {
			const control = new FormControl();
			this.editSchoolForm.controls.utilityIDs.push(control);
		});
	}

	private addFacilityIdToFormArray() {
		// Get all selected checkboxes (return an array of id)
		const selectedItems = this.editSchoolForm.value
			.facilityIDs!.map((checked, index) => (checked ? this.facilities[index].id : null))
			.filter((value) => value !== null) as string[];

		const facilityIds = this.editSchoolForm.controls.facilityIDs;
		this.addCheckboxToFormArray(selectedItems, facilityIds);
	}

	private addUtilityIdToFormArray() {
		// Get all selected checkboxes (return an array of id)
		const selectedItems = this.editSchoolForm.value
			.utilityIDs!.map((checked, index) => (checked ? this.utilities[index].id : null))
			.filter((value) => value !== null) as string[];

		const utilityIds = this.editSchoolForm.controls.utilityIDs;
		this.addCheckboxToFormArray(selectedItems, utilityIds);
	}

	private addCheckboxToFormArray(selectedItems: string[], formArray: FormArray<FormControl<string | null>>) {
		const formIds = formArray.value;
		formIds.splice(0, formIds.length);
		selectedItems!.forEach((item) => formIds.push(item));
	}

	private loadAllProvinces() {
		this.schoolService.getAllProvince();
		this.provinceList = this.schoolService.provinceList$;
	}

	private loadDistrictByProvince() {
		this.districtList = this.schoolService.districtList$;

		const subscription = this.editSchoolForm.controls.provinceId.valueChanges.subscribe((value) =>
			this.schoolService.getAllDistrictByProvinceCode(value!)
		);
		this.subscriptions.push(subscription);
	}

	private loadWardByDistrict() {
		this.wardList = this.schoolService.wardList$;

		const subscription = this.editSchoolForm.controls.districtId.valueChanges.subscribe((value) =>
			this.schoolService.getAllWardByDistrictCode(value!)
		);
		this.subscriptions.push(subscription);
	}

	private bindingValueToForm(data: SchoolDTO) {
		let index = 0;
		const facilityIDsControls = (this.editSchoolForm.controls.facilityIDs as FormArray).controls;
		while (index < facilityIDsControls.length) {
			facilityIDsControls[index].setValue('');
			index++;
		}

		let index1 = 0;
		const utilityIDsControls = (this.editSchoolForm.controls.utilityIDs as FormArray).controls;
		while (index1 < utilityIDsControls.length) {
			utilityIDsControls[index1].setValue('');
			index1++;
		}

		this.editSchoolForm.patchValue({
			id: data.id,
			schoolName: data.schoolName,
			email: data.email,
			phone: data.phone,
			schoolTypeId: data.schoolTypeId,
			educationMethodId: data.educationMethodId,
			languageId: data.languageId,
			feeFrom: data.feeFrom?.toString(),
			feeTo: data.feeTo?.toString(),
			childReceivingAgeId: data.childReceivingAgeId,
			schoolIntroduce: data.schoolIntroduce,
			specificAddress: data.specificAddress,
			wardId: data.wardId,
			districtId: data.districtId,
			provinceId: data.provinceId,
			statusId: data.statusId
		});

		if (data.facilitiesName) {
			data.facilitiesName.forEach((facilityName) => {
				const elementFoundByName = this.facilities.find((x) => x.name === facilityName);
				if (elementFoundByName) {
					const facilitiesIndex = this.facilities.findIndex((x) => x.name === elementFoundByName.name);
					this.editSchoolForm.controls.facilityIDs.controls[facilitiesIndex].setValue(
						elementFoundByName.id || ''
					);
				}
			});
		}
		if (data.utilitiesName) {
			data.utilitiesName.forEach((utilitiesName) => {
				const elementFoundByName = this.utilities.find((x) => x.name === utilitiesName);
				if (elementFoundByName) {
					const utilitiesIndex = this.utilities.findIndex((x) => x.name === elementFoundByName.name);
					this.editSchoolForm.controls.utilityIDs.controls[utilitiesIndex].setValue(
						elementFoundByName.id || ''
					);
				}
			});
		}
	}
}
