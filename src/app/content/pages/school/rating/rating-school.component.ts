import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { RatingDTO } from '../../../../models/ratings/rating-dto';
import { SchoolService } from '../../../../services/school.service';

@Component({
	selector: 'app-rating-school',
	standalone: true,
	imports: [ReactiveFormsModule, CommonModule, FormsModule],
	styleUrls: ['./rating-school.component.scss'],
	templateUrl: './rating-school.component.html'
})
export class RatingSchoolComponent implements OnInit, OnDestroy {
	protected schoolId = '';
	protected subscription: Subscription | null | undefined;

	protected toDate: string = '';
	protected fromDate: string = '';
	protected feedbacksToShow: number = 3;
	protected displayedFeedbacks: RatingDTO[] = [];
	protected averageRatings: { [key: string]: number } = {};
	protected ratingSchoolList: RatingDTO[] = [];
	constructor(
		private route: ActivatedRoute,
		private schoolService: SchoolService,
		private sanitizer: DomSanitizer
	) {}

	ngOnInit() {
		this.route.queryParams.subscribe((params) => {
			this.schoolId = params['id'];
			this.getRatingData();
		});
	}

	getRatingData() {
		this.schoolService.getRatingSchool(this.schoolId);

		this.subscription = this.schoolService.ratingSchoolList$.subscribe((data) => {
			if (this.fromDate && this.toDate) {
				this.ratingSchoolList = data.filter((feedback) => {
					const feedbackDate = new Date(feedback.createAt);
					const from = new Date(this.fromDate);
					const to = new Date(this.toDate);

					return feedbackDate >= from && feedbackDate <= to;
				});
			} else {
				this.ratingSchoolList = data;
			}

			this.calculateAverageStar(this.ratingSchoolList);

			this.displayedFeedbacks =
				this.ratingSchoolList.length <= this.feedbacksToShow
					? this.ratingSchoolList
					: this.ratingSchoolList.slice(0, this.feedbacksToShow);
		});
	}

	ngOnDestroy(): void {
		this.subscription?.unsubscribe();
	}

	calculateAverageStar(data: RatingDTO[]) {
		const totalRatings = data.length;

		this.averageRatings = {
			learnProgramRating: data.reduce((sum, rating) => sum + rating.learnProgramRating, 0) / totalRatings,
			facilitiesAndUtilitiesRating:
				data.reduce((sum, rating) => sum + rating.facilitiesAndUtilitiesRating, 0) / totalRatings,
			extracurricularActivesRating:
				data.reduce((sum, rating) => sum + rating.extracurricularActivesRating, 0) / totalRatings,
			teacherAndStaffRating:
				data.reduce((sum, rating) => sum + rating.teacherAndStaffRating, 0) / totalRatings,
			hygieneAndNutrition: data.reduce((sum, rating) => sum + rating.hygieneAndNutrition, 0) / totalRatings
		};
	}
	convertToStars(average: number): string {
		const fullStars = Math.floor(average);
		const emptyStars = 5 - fullStars;
		return '★'.repeat(fullStars) + '☆'.repeat(emptyStars);
	}
	formatTimeDifference(dateString: string): string {
		const date = new Date(dateString);
		const now = new Date();
		const diffTime = Math.abs(now.getTime() - date.getTime());
		const diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24));

		if (diffDays > 7) {
			const weeks = Math.floor(diffDays / 7);
			return `${weeks} week${weeks > 1 ? 's' : ''} ago`;
		} else if (diffDays >= 1) {
			return `${diffDays} day${diffDays > 1 ? 's' : ''} ago`;
		} else {
			const diffHours = Math.floor(diffTime / (1000 * 60 * 60));
			if (diffHours >= 1) {
				return `${diffHours} hour${diffHours > 1 ? 's' : ''} ago`;
			} else {
				const diffMinutes = Math.floor(diffTime / (1000 * 60));
				return `${diffMinutes} minute${diffMinutes > 1 ? 's' : ''} ago`;
			}
		}
	}

	loadMoreFeedbacks() {
		this.feedbacksToShow += 3;
		this.displayedFeedbacks = this.ratingSchoolList.slice(0, this.feedbacksToShow);
	}

	getAvatarUrl(byteArray: string): SafeUrl | string {
		return this.sanitizer.bypassSecurityTrustUrl('data:image/png;base64,' + byteArray);
	}

	refreshData() {
		this.getRatingData();
	}
}
