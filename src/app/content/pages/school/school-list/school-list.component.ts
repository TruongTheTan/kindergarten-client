import { AsyncPipe, CommonModule } from '@angular/common';
import { AfterViewInit, Component } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { SchoolDTO } from '../../../../models/schools/school-dto';
import { BreadcrumbComposite } from '../../../../shared/components/breadcrumb/breadcrumb-composite';
import { ModalComponent } from '../../../../shared/components/modal/modal.component';
import { PagingComponent } from '../../../../shared/components/paging/paging.component';
import { ShowEntryComponent } from '../../../../shared/components/show-entry/show-entry.component';
import { DateFormatPipe } from '../../../../shared/pipes/date-format-pipe';
import { AuthenticationUtils } from '../../../../utils/authentication-utils';
import { SchoolUtils } from '../../../../utils/school-utils';
import { PageResult } from './../../../../models/page-result';
import { SchoolService } from './../../../../services/school.service';

@Component({
	selector: 'app-school-list',
	standalone: true,
	imports: [
		AsyncPipe,
		PagingComponent,
		DateFormatPipe,
		CommonModule,
		ModalComponent,
		RouterLink,
		ShowEntryComponent
	],
	templateUrl: './school-list.component.html',
	styleUrl: './school-list.component.scss'
})
export class SchoolListComponent implements AfterViewInit {
	public breadcrumbComposite = {
		breadCrumbDisplayName: 'School List',
		breadCrumbRouterPath: '/content/school/list'
	} as BreadcrumbComposite;

	protected startIndex = 0;
	protected subscription: Subscription | undefined;
	protected pageResult = {} as Observable<PageResult<SchoolDTO>>;
	roleName: string = '';

	schoolId: string = '';

	private searchInput = {} as HTMLInputElement;

	constructor(
		private schoolService: SchoolService,
		private routes: Router,
		private authenticationUtils: AuthenticationUtils
	) {
		this.roleName = authenticationUtils.getRoleFromCookie();
		this.pageResult = schoolService.schoolList$;
	}

	ngAfterViewInit(): void {
		this.searchInput = document.getElementById('searchInput')! as HTMLInputElement;
		this.setEnterEventForInputSearch();
	}

	protected calculateStartIndex(pageResult: PageResult<SchoolDTO>) {
		this.startIndex = (pageResult?.pageIndex! - 1) * pageResult?.pageSize! + 1;
	}

	protected getCurrentPage(currentPage: number) {
		this.schoolService.getAllSchool('', 5, currentPage);
	}

	protected onSearchButtonClick() {
		this.schoolService.getAllSchool(this.searchInput.value, 5, 1);
	}

	protected getAllSchoolOnInputEvent() {
		if (this.searchInput.value.trim() === '') {
			this.schoolService.getAllSchool('', 5, 1);
		}
	}

	protected getStatusColor(statusName: string) {
		return SchoolUtils.getStatusColor(statusName);
	}

	private setEnterEventForInputSearch() {
		this.searchInput.addEventListener('keypress', (event) => {
			if (event.key === 'Enter') {
				event.preventDefault();
				this.schoolService.getAllSchool(this.searchInput.value, 5, 1);
			}
		});
	}

	onEditButtonClick(school: SchoolDTO) {
		this.routes.navigate(['content/school/edit'], {
			queryParams: { id: school.id }
		});
	}

	onDeleteButtonClick(id: string) {
		this.schoolService.deleteSchool(id);
	}

	isDeleted(data: any) {
		const value = data['isDeleted'];
		return typeof value === 'boolean' ? value : value;
	}
}
