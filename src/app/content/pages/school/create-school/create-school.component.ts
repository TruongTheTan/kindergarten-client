import { AsyncPipe, CommonModule } from '@angular/common';
import { Component, effect, OnDestroy, signal } from '@angular/core';
import { FormArray, FormBuilder, FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { NgxSelectModule } from 'ngx-select-ex';
import { Observable, Subscription } from 'rxjs';
import { District, Province, Ward } from '../../../../models/locations/location';
import {
	schoolEducationMethods,
	schoolFacilities,
	schoolLanguages,
	schoolStatus,
	schoolTypes,
	schoolUtilities
} from '../../../../models/school-master-data';
import { CreateSchool } from '../../../../models/schools/create-school';
import { SchoolService } from '../../../../services/school.service';
import { BreadcrumbComposite } from '../../../../shared/components/breadcrumb/breadcrumb-composite';
import { ModalComponent } from '../../../../shared/components/modal/modal.component';
import { AuthenticationUtils } from '../../../../utils/authentication-utils';
import { CustomFormValidation } from '../../../../utils/custom-form-validation';

@Component({
	selector: 'app-create-school',
	standalone: true,
	imports: [CKEditorModule, ReactiveFormsModule, CommonModule, AsyncPipe, ModalComponent, NgxSelectModule],
	templateUrl: './create-school.component.html',
	styleUrl: './create-school.component.scss'
})
export class CreateSchoolComponent implements OnDestroy {
	editor = ClassicEditor;

	public breadcrumbComposite = {
		breadCrumbDisplayName: 'Create School',
		breadCrumbRouterPath: '/content/school/create'
	} as BreadcrumbComposite;

	protected role = '';
	protected subscriptions: Subscription[] = [];

	protected provinceList = {} as Observable<Province[]>;
	protected districtList = {} as Observable<District[]>;
	protected wardList = {} as Observable<Ward[]>;

	protected types = schoolTypes;
	protected utilities = schoolUtilities;
	protected languages = schoolLanguages;
	protected facilities = schoolFacilities;
	protected educationMethods = schoolEducationMethods;

	protected isSavedAsDraft = signal(false);
	protected isCancelButtonClick = signal(false);

	protected buttonText = '';
	protected modalTitle = '';
	protected modalConfirmQuestion = '';

	// Form
	protected createSchoolForm = this.formBuilder.group(
		{
			schoolName: ['', Validators.required],
			email: ['', [Validators.required, Validators.email]],
			phone: ['', Validators.required],
			schoolTypeId: ['', Validators.required],
			educationMethodId: ['', Validators.required],
			languageId: ['', Validators.required],
			feeFrom: ['', [Validators.required, Validators.min(1)]],
			feeTo: ['', [Validators.required, Validators.min(1)]],
			childReceivingAgeId: ['', Validators.required],
			schoolIntroduce: [''],
			specificAddress: ['', Validators.required],
			wardId: ['', Validators.required],
			districtId: ['', Validators.required],
			provinceId: ['', Validators.required],
			facilityIDs: this.formBuilder.array([] as string[]),
			utilityIDs: this.formBuilder.array([] as string[]),
			images: [[] as File[]]
		},
		{
			validators: [CustomFormValidation.feeRangeValidator()]
		}
	);

	constructor(
		private router: Router,
		private formBuilder: FormBuilder,
		private schoolService: SchoolService,
		private authenticationUtils: AuthenticationUtils
	) {
		this.addCheckboxes();
		this.loadAllProvinces();
		this.loadDistrictByProvince();
		this.loadWardByDistrict();
		this.setButtonState();
		this.role = authenticationUtils.getRoleFromCookie();
	}

	ngOnDestroy(): void {
		this.subscriptions.forEach((subscription) => subscription.unsubscribe());
	}

	onCancelButtonClick() {
		this.router.navigateByUrl('/content/school/list');
	}

	triggerAllFormError() {
		this.createSchoolForm.valid === false ? this.createSchoolForm.markAllAsTouched() : null;
	}

	onSubmitForm() {
		if (this.createSchoolForm.valid) {
			this.addUtilityIdToFormArray();
			this.addFacilityIdToFormArray();

			const {
				schoolName,
				childReceivingAgeId,
				districtId,
				educationMethodId,
				email,
				feeFrom,
				feeTo,
				images,
				languageId,
				phone,
				provinceId,
				schoolIntroduce,
				schoolTypeId,
				specificAddress,
				wardId,
				facilityIDs,
				utilityIDs
			} = this.createSchoolForm.controls;

			const createSchool = {
				childReceivingAgeId: childReceivingAgeId.value,
				districtId: districtId.value,
				educationMethodId: educationMethodId.value,
				email: email.value,
				feeFrom: +feeFrom.value!,
				feeTo: +feeTo.value!,
				languageId: languageId.value,
				phone: phone.value,
				provinceId: provinceId.value,
				schoolIntroduce: schoolIntroduce.value,
				schoolName: schoolName.value,
				schoolTypeId: schoolTypeId.value,
				specificAddress: specificAddress.value,
				wardId: wardId.value,
				images: images.value,
				facilityIDs: facilityIDs.value,
				utilityIDs: utilityIDs.value
			} as CreateSchool;

			const status = schoolStatus.find((s) =>
				this.isSavedAsDraft() ? s.name === 'Saved' : s.name === 'Submitted'
			);

			createSchool.statusId = status?.id!;
			createSchool.createdBy = this.authenticationUtils.getUserIdFromCookie();

			this.schoolService.createSchool(createSchool);
		}
	}

	addEventChangeToInputImageField(event: Event) {
		const inputFileField = event.target as HTMLInputElement;

		for (let i = 0; i < inputFileField.files!.length; i++) {
			const imageFile = inputFileField.files?.item(i);
			this.createSchoolForm.controls.images.value?.push(imageFile!);
		}
	}

	private addCheckboxes() {
		this.facilities.forEach(() => {
			const control = new FormControl();
			this.createSchoolForm.controls.facilityIDs.push(control);
		});

		this.utilities.forEach(() => {
			const control = new FormControl();
			this.createSchoolForm.controls.utilityIDs.push(control);
		});
	}

	private addFacilityIdToFormArray() {
		// Get all selected checkboxes (return an array of id)
		const selectedItems = this.createSchoolForm.value
			.facilityIDs!.map((checked, index) => (checked ? this.facilities[index].id : null))
			.filter((value) => value !== null) as string[];

		const facilityIds = this.createSchoolForm.controls.facilityIDs;
		this.addCheckboxToFormArray(selectedItems, facilityIds);
	}

	private addUtilityIdToFormArray() {
		// Get all selected checkboxes (return an array of id)
		const selectedItems = this.createSchoolForm.value
			.utilityIDs!.map((checked, index) => (checked ? this.utilities[index].id : null))
			.filter((value) => value !== null) as string[];

		const utilityIds = this.createSchoolForm.controls.utilityIDs;
		this.addCheckboxToFormArray(selectedItems, utilityIds);
	}

	private addCheckboxToFormArray(selectedItems: string[], formArray: FormArray<FormControl<string | null>>) {
		const formIds = formArray.value;

		formIds.splice(0, formIds.length);
		selectedItems!.forEach((item) => formIds.push(item));

		console.log('Table below');
		console.table(formIds);
	}

	/*************************************************** Locations select below ******************************/

	private loadAllProvinces() {
		this.schoolService.getAllProvince();
		this.provinceList = this.schoolService.provinceList$;
	}

	private loadDistrictByProvince() {
		this.districtList = this.schoolService.districtList$;

		const subscription = this.createSchoolForm.controls.provinceId.valueChanges.subscribe((value) => {
			this.createSchoolForm.controls.wardId.setValue('');
			this.createSchoolForm.controls.districtId.setValue('');

			this.schoolService.getAllDistrictByProvinceCode(value!);
			this.schoolService.getAllWardByDistrictCode(value!);
		});
		this.subscriptions.push(subscription);
	}

	private loadWardByDistrict() {
		this.wardList = this.schoolService.wardList$;

		const subscription = this.createSchoolForm.controls.districtId.valueChanges.subscribe((value) =>
			this.schoolService.getAllWardByDistrictCode(value!)
		);

		this.subscriptions.push(subscription);
	}

	private setButtonState() {
		effect(() => {
			if (this.isCancelButtonClick()) {
				this.buttonText = 'Leave';
				this.modalTitle = 'Leave create school';
				this.modalConfirmQuestion = 'Do you want to leave ?';
			} else if (this.isSavedAsDraft()) {
				this.buttonText = 'Save';
				this.modalTitle = 'Save school as draft';
				this.modalConfirmQuestion = 'Do you want to save new school as draft ?';
			} else {
				this.buttonText = 'Create';
				this.modalTitle = 'Create new school rating';
				this.modalConfirmQuestion = 'Do you want to create new school ?';
			}
		});
	}
}
