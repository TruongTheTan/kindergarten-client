import { Component, signal } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { BreadcrumbComposite } from '../../shared/components/breadcrumb/breadcrumb-composite';
import { FooterComponent } from '../../shared/components/footer/footer.component';
import { HeaderComponent } from '../../shared/components/header/header.component';
import { SidebarComponent } from '../../shared/components/sidebar/sidebar.component';
import { BreadcrumbComponent } from './../../shared/components/breadcrumb/breadcrumb.component';

@Component({
	selector: 'app-layout',
	standalone: true,
	imports: [RouterOutlet, FooterComponent, SidebarComponent, HeaderComponent, BreadcrumbComponent],
	templateUrl: './layout.component.html',
	styleUrl: './layout.component.scss'
})
export class LayoutComponent {
	protected breadcrumbManagementName = signal('');
	protected breadcrumbComposite = signal({} as BreadcrumbComposite);

	constructor() {}

	onActivate(component: Component) {
		const breadcrumbComposite = (component as any).breadcrumbComposite as BreadcrumbComposite;
		this.breadcrumbComposite.set(breadcrumbComposite);
	}

	onSidebarItemClick(itemInnerText: string) {
		this.breadcrumbManagementName.set(itemInnerText);
	}
}
