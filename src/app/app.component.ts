import { AsyncPipe } from '@angular/common';
import { Component, OnChanges, SimpleChanges } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NgToastModule } from 'ng-angular-popup';
import { ngxLoadingAnimationTypes, NgxLoadingModule } from 'ngx-loading';
import { Observable } from 'rxjs';
import { LoadingStore } from './../store/loading-store';

@Component({
	selector: 'app-root',
	standalone: true,
	imports: [RouterOutlet, NgToastModule, NgxLoadingModule, AsyncPipe],
	templateUrl: './app.component.html',
	styleUrl: './app.component.scss'
})
export class AppComponent implements OnChanges {
	protected isLoading = {} as Observable<boolean>;
	protected ngxLoadingAnimationTypes = ngxLoadingAnimationTypes.cubeGrid;

	constructor(private loadingStore: LoadingStore) {
		this.isLoading = this.loadingStore.isLoading$;
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.isLoading = this.loadingStore.isLoading$;
	}
}
