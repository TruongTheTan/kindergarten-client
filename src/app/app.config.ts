import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { ApplicationConfig } from '@angular/core';
import { provideAnimations } from '@angular/platform-browser/animations';
import { provideRouter } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { NgxLoadingModule } from 'ngx-loading';
import interceptorConfig from './api/interceptor';
import { routes } from './app.routes';

export const appConfig: ApplicationConfig = {
	providers: [
		CookieService,
		NgxLoadingModule,
		provideAnimations(),
		provideRouter(routes),
		provideHttpClient(withInterceptors([interceptorConfig]))
	]
};
