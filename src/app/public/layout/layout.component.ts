import { Component, signal } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { FooterComponent } from '../../shared/components/footer/footer.component';
import { HeaderComponent } from '../../shared/components/header/header.component';
import { HomePageComponent } from '../pages/home-page/home-page.component';
import { ListSchoolComponent } from '../pages/school/list-school/list-school.component';
import { SchoolDetailComponent } from '../pages/school/school-detail/school-detail.component';

@Component({
	selector: 'app-layout',
	standalone: true,
	imports: [RouterOutlet, FooterComponent, HeaderComponent],
	templateUrl: './layout.component.html',
	styleUrl: './layout.component.scss'
})
export class LayoutComponent {
	protected isRemoveContainerFluid = signal(false);

	getCurrentComponent(event: Component) {
		this.isRemoveContainerFluid.set(false);

		if (event instanceof HomePageComponent) {
			this.isRemoveContainerFluid.set(true);
		}
		if (event instanceof SchoolDetailComponent) {
			this.isRemoveContainerFluid.set(true);
		}
		if (event instanceof ListSchoolComponent) {
			this.isRemoveContainerFluid.set(true);
		}
	}
}
