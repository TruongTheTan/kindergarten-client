import { LabelType, NgxSliderModule, Options } from '@angular-slider/ngx-slider';
import { AsyncPipe } from '@angular/common';
import { Component, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSelectModule } from 'ngx-select-ex';
import { Observable, Subscription } from 'rxjs';
import { SearchStore } from '../../../../../store/search-store';
import { District, Province } from '../../../../models/locations/location';
import { PageResult } from '../../../../models/page-result';
import {
	childReceivingAge,
	schoolFacilities,
	schoolTypes,
	schoolUtilities
} from '../../../../models/school-master-data';
import { GetPublicSchoolRequest } from '../../../../models/schools/public-get-school-request';
import { PublicSchoolDTO } from '../../../../models/schools/public-school-dto';
import { SchoolService } from '../../../../services/school.service';
import { PagingComponent } from '../../../../shared/components/paging/paging.component';
import { ShowEntryComponent } from '../../../../shared/components/show-entry/show-entry.component';
import { ListSchoolItemComponent } from '../list-school-item/list-school-item.component';

@Component({
	selector: 'app-list-school',
	standalone: true,
	templateUrl: './list-school.component.html',
	styleUrls: ['./list-school.component.scss'],
	imports: [
		ListSchoolItemComponent,
		NgxSliderModule,
		ReactiveFormsModule,
		AsyncPipe,
		NgxSelectModule,
		FormsModule,
		AsyncPipe,
		PagingComponent,
		ShowEntryComponent
	]
})
export class ListSchoolComponent implements OnDestroy {
	protected schoolType = schoolTypes;

	publicSchoolForm = this.fb.group({
		schoolName: [''],
		provinceId: [''],
		districtId: [''],
		typeSchoolId: [''],
		admissionAgeId: [''],
		feeFrom: [0],
		feeTo: [10000000],
		facilities: this.fb.array([] as string[]),
		utilities: this.fb.array([] as string[]),
		sortBy: ['rating'],
		pageIndex: [1, []],
		pageSize: [4, []]
	});

	protected admissionAge = childReceivingAge;
	protected facilities = schoolFacilities;
	protected utilities = schoolUtilities;

	protected subscription: Subscription | undefined;
	protected pageResult = {} as PageResult<PublicSchoolDTO>;
	protected getRequest = {} as GetPublicSchoolRequest;
	protected cities = {} as Observable<Province[]>;
	protected districts = {} as Observable<District[]>;

	private subscriptions = [] as Subscription[];

	minValue: number = 0;
	maxValue: number = 100000000;

	options: Options = {
		floor: 0,
		ceil: 10000000,
		translate: (value: number, label: LabelType): string => {
			switch (label) {
				case LabelType.Low:
					return value + '';
				case LabelType.High:
					return value + '';
				default:
					return '';
			}
		}
	};

	constructor(private schoolService: SchoolService, private fb: FormBuilder, private searchStore: SearchStore) {
		this.addCheckboxes();
		schoolService.getAllProvince();
		this.cities = schoolService.provinceList$;
		this.districts = schoolService.districtList$;

		const sub = searchStore.schoolNameToSearch$.subscribe((data) => {
			if (data) {
				this.publicSchoolForm.controls.schoolName.setValue(data);
			}
		});

		const sub1 = searchStore.locationSearch$.subscribe((data) => {
			const provinceCode = data.provinceCode;

			if (provinceCode) {
				const districtCode = data.districtCode;

				this.publicSchoolForm.controls.provinceId.setValue(provinceCode);
				this.publicSchoolForm.controls.districtId.setValue(districtCode);
				this.getRequest.provinceId = provinceCode;
				this.getRequest.districtId = districtCode;
			}
		});

		this.getRequest.pageIndex = 1;
		this.getRequest.pageSize = 4;

		const sub2 = schoolService.publicSchoolList$.subscribe((data) => (this.pageResult = data));

		this.subscriptions.push(sub);
		this.subscriptions.push(sub1);
		this.subscriptions.push(sub2);
	}

	ngOnDestroy(): void {
		this.subscriptions.forEach((sub) => sub.unsubscribe());
	}

	onCityChange(selectOption: any) {
		if (this.checkUndefine(selectOption[0])) {
			const cityCode = selectOption[0].value.toString();

			if (selectOption[0].value !== '') {
				this.schoolService.getAllDistrictByProvinceCode(cityCode);
			}
		} else {
			this.publicSchoolForm.patchValue({
				provinceId: '',
				districtId: ''
			});
		}
	}

	onSliderChange(minValue: number, maxValue: number) {
		this.publicSchoolForm.patchValue({
			feeFrom: minValue,
			feeTo: maxValue
		});
		//this.onSubmit();
	}

	getCurrentPage($event: number) {
		this.publicSchoolForm.patchValue({
			pageIndex: $event
		});
		this.onSubmit();
	}

	onFieldClick() {
		//this.onSubmit();
	}

	onSubmit() {
		this.addUtilityIdToFormArray();
		this.addFacilityIdToFormArray();
		this.setGetRequestByFormValues();

		console.log(this.getRequest);
		this.schoolService.getPublicSchool(this.getRequest);
		const sub = this.schoolService.publicSchoolList$.subscribe((data) => (this.pageResult = data));

		this.subscriptions.push(sub);
	}

	setGetRequestByFormValues() {
		this.getRequest = {
			schoolName: this.publicSchoolForm.controls.schoolName.value,
			provinceId: this.checkUndefine(this.publicSchoolForm.controls.provinceId.value)
				? this.publicSchoolForm.controls.provinceId.value
				: '',
			districtId: this.checkUndefine(this.publicSchoolForm.controls.districtId.value)
				? this.publicSchoolForm.controls.districtId.value
				: '',
			typeSchoolId: this.publicSchoolForm.controls.typeSchoolId.value,
			addmissionAgeId: this.publicSchoolForm.controls.admissionAgeId.value,
			feeFrom: this.publicSchoolForm.controls.feeFrom.value,
			feeTo: this.publicSchoolForm.controls.feeTo.value,
			facilities: this.publicSchoolForm.controls.facilities.value,
			utlities: this.publicSchoolForm.controls.utilities.value,
			sortBy: this.publicSchoolForm.controls.sortBy.value,
			pageIndex: this.publicSchoolForm.controls.pageIndex.value,
			pageSize: this.publicSchoolForm.controls.pageSize.value
		} as GetPublicSchoolRequest;
	}

	onResetButtonClick() {
		this.publicSchoolForm = this.fb.group({
			schoolName: [''],
			provinceId: [''],
			districtId: [''],
			typeSchoolId: [''],
			admissionAgeId: [''],
			feeFrom: [0],
			feeTo: [10000000],
			facilities: this.fb.array([] as string[]),
			utilities: this.fb.array([] as string[]),
			sortBy: ['rating'],
			pageIndex: [1, []],
			pageSize: [4, []]
		});
		this.minValue = 0;
		this.maxValue = 100000000;
		this.options = {
			floor: 0,
			ceil: 10000000,
			translate: (value: number, label: LabelType): string => {
				switch (label) {
					case LabelType.Low:
						return value + '';
					case LabelType.High:
						return value + '';
					default:
						return '';
				}
			}
		};
		this.getRequest.districtId = undefined;
		this.getRequest.provinceId = undefined;

		this.onSubmit();
	}

	checkUndefine(value: any) {
		if (typeof value === 'undefined' || value === null) {
			return false;
		} else {
			return true;
		}
	}

	private addCheckboxes() {
		this.facilities.forEach(() => {
			const control = new FormControl();
			this.publicSchoolForm.controls.facilities.push(control);
		});

		this.utilities.forEach(() => {
			const control = new FormControl();
			this.publicSchoolForm.controls.utilities.push(control);
		});
	}

	private addFacilityIdToFormArray() {
		// Get all selected checkboxes (return an array of id)
		const selectedItems = this.publicSchoolForm.value
			.facilities!.map((checked, index) => (checked ? this.facilities[index].id : null))
			.filter((value) => value !== null) as string[];

		const facilityIds = this.publicSchoolForm.controls.facilities;
		this.addCheckboxToFormArray(selectedItems, facilityIds);
	}

	private addUtilityIdToFormArray() {
		// Get all selected checkboxes (return an array of id)
		const selectedItems = this.publicSchoolForm.value
			.utilities!.map((checked, index) => (checked ? this.utilities[index].id : null))
			.filter((value) => value !== null) as string[];

		const utilityIds = this.publicSchoolForm.controls.utilities;
		this.addCheckboxToFormArray(selectedItems, utilityIds);
	}

	private addCheckboxToFormArray(selectedItems: string[], formArray: FormArray<FormControl<string | null>>) {
		const formIds = formArray.value;
		formIds.splice(0, formIds.length);
		selectedItems!.forEach((item) => formIds.push(item));
	}
}
