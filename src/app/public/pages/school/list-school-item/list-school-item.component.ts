import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { PublicSchoolDTO } from '../../../../models/schools/public-school-dto';
import { RatingComponent } from '../../../../shared/components/rating/rating.component';

@Component({
	selector: 'app-list-school-item',
	standalone: true,
	imports: [RatingComponent, RouterModule],
	templateUrl: './list-school-item.component.html',
	styleUrls: ['./list-school-item.component.scss']
})
export class ListSchoolItemComponent implements OnChanges {
	@Input() schoolItem = {} as PublicSchoolDTO;

	totalRating: number = 0.0;

	formatter = new Intl.NumberFormat('vi-VN', {
		style: 'currency',
		currency: 'VND'
	});

	constructor(private sanitizer: DomSanitizer) {}

	ngOnChanges(changes: SimpleChanges): void {
		if (this.schoolItem.ratings.length > 0) {
			const totalRate = this.schoolItem.ratings.reduce(
				(accumulator, totalRate) => accumulator + totalRate.totalRate,
				0.0
			);
			this.totalRating = Number.parseFloat((totalRate / this.schoolItem.ratings.length).toFixed(1));
		} else {
			this.totalRating = 0;
		}
	}

	imageUrl(imageData: Uint8Array | string): SafeUrl | string {
		if (imageData) {
			return this.sanitizer.bypassSecurityTrustUrl('data:image/png;base64,' + imageData);
		}
		return '';
	}
}
