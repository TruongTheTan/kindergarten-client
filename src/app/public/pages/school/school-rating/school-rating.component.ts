import { CommonModule } from '@angular/common';
import { Component, computed, signal } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CreateSchoolRating } from '../../../../models/schools/create-school-rating';
import { SchoolService } from '../../../../services/school.service';
import { ModalComponent } from '../../../../shared/components/modal/modal.component';
import { AuthenticationUtils } from '../../../../utils/authentication-utils';

@Component({
	selector: 'app-school-rating',
	standalone: true,
	imports: [ModalComponent, CommonModule, FormsModule],
	templateUrl: './school-rating.component.html',
	styleUrl: './school-rating.component.scss'
})
export class SchoolRatingComponent {
	protected feedback = '';
	protected isInvalidForm = false;
	protected isCancelButtonClick = signal(false);

	protected buttonText = computed(() => (this.isCancelButtonClick() ? 'Leave' : 'Create'));

	protected modalTitle = computed(() =>
		this.isCancelButtonClick() ? 'Leave rating school' : 'Create new school rating'
	);

	protected modalConfirmQuestion = computed(() =>
		this.isCancelButtonClick() ? 'Do you want to leave ?' : 'Do you want to create a new rating ?'
	);

	protected teacherAndStaffSelectedValue = 0;
	protected learningProgramSelectedValue = 0;
	protected facilityAndUtilitySelectedValue = 0;
	protected hygieneAndNutritionSelectedValue = 0;
	protected extracurricularActivitiesSelectedValue = 0;

	constructor(
		private router: Router,
		private schoolService: SchoolService,
		private activatedRoute: ActivatedRoute,
		private authenticationUtils: AuthenticationUtils
	) {}

	onSubmitForm() {
		if (this.checkValidFormInput()) {
			const createSchoolRating: CreateSchoolRating = {
				feedback: this.feedback,
				schoolId: this.activatedRoute.snapshot.paramMap.get('id'),
				userId: this.authenticationUtils.getUserIdFromCookie(),
				learnProgramRating: this.learningProgramSelectedValue,
				facilitiesAndUtilitiesRating: this.facilityAndUtilitySelectedValue,
				extracurricularActivesRating: this.extracurricularActivitiesSelectedValue,
				teacherAndStaffRating: this.teacherAndStaffSelectedValue,
				hygieneAndNutrition: this.hygieneAndNutritionSelectedValue
			} as CreateSchoolRating;

			this.schoolService.createSchoolRating(createSchoolRating);
		}
	}

	onResetButtonClick() {
		this.feedback = '';
		this.isInvalidForm = false;
		this.learningProgramSelectedValue = 0;
		this.facilityAndUtilitySelectedValue = 0;
		this.extracurricularActivitiesSelectedValue = 0;
		this.teacherAndStaffSelectedValue = 0;
		this.hygieneAndNutritionSelectedValue = 0;
	}

	onCancelButtonClick() {
		this.router.navigateByUrl('/public/school/detail/' + this.activatedRoute.snapshot.paramMap.get('id'));
	}

	private checkValidFormInput() {
		const noLearningRating = this.learningProgramSelectedValue === 0;
		const noFacilityAndUtilityRating = this.facilityAndUtilitySelectedValue === 0;
		const noExtraRating = this.extracurricularActivitiesSelectedValue === 0;
		const noTeacherAndStaffRating = this.teacherAndStaffSelectedValue === 0;
		const noHygieneRating = this.hygieneAndNutritionSelectedValue === 0;

		// Check there are no rating stats (all are 0 star)
		const noRating =
			noLearningRating &&
			noFacilityAndUtilityRating &&
			noExtraRating &&
			noTeacherAndStaffRating &&
			noHygieneRating;

		const isValidCondition = noRating === true && this.feedback === '' ? false : true;
		isValidCondition ? (this.isInvalidForm = false) : (this.isInvalidForm = true);

		return isValidCondition;
	}
}
