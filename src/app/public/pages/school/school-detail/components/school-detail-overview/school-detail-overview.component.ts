import { Component, Input } from '@angular/core';
import { schoolFacilities, schoolUtilities } from '../../../../../../models/school-master-data';
import { SchoolDTO } from '../../../../../../models/schools/school-dto';

@Component({
	selector: 'app-school-detail-overview',
	standalone: true,
	imports: [],
	templateUrl: './school-detail-overview.component.html',
	styleUrl: './school-detail-overview.component.scss'
})
export class SchoolDetailOverviewComponent {
	@Input({ required: true }) schoolDetail!: SchoolDTO;

	protected utilities: (string | undefined)[] = schoolUtilities.map((f) => f.name);
	protected facilities: (string | undefined)[] = schoolFacilities.map((f) => f.name);

	getCheckedFacilities(facilityName: string) {
		return this.schoolDetail.facilitiesName?.includes(facilityName);
	}

	getCheckedUtilities(utilityName: string) {
		return this.schoolDetail.utilitiesName?.includes(utilityName);
	}
}
