import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { SchoolDTO } from '../../../../../../models/schools/school-dto';

@Component({
	selector: 'app-school-detail-rating',
	standalone: true,
	imports: [CommonModule],
	templateUrl: './school-detail-rating.component.html',
	styleUrl: './school-detail-rating.component.scss'
})
export class SchoolDetailRatingComponent {
	@Input() schoolDetail = {} as SchoolDTO;
}
