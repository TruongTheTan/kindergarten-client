import { CommonModule } from '@angular/common';
import { Component, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { Subscription } from 'rxjs';
import { RatingDTO } from '../../../../models/ratings/rating-dto';
import { SchoolDTO } from '../../../../models/schools/school-dto';
import { ExtendedUserDTO } from '../../../../models/users/user-dto';
import { FooterComponent } from '../../../../shared/components/footer/footer.component';
import { RequestCounselingModalComponent } from '../../../../shared/components/modal/request-counseling-modal/request-counseling-modal.component';
import { SchoolEnrollmentRequestComponent } from '../../../../shared/components/modal/school-enrollment-request/school-enrollment-request.component';
import { AuthenticationUtils } from '../../../../utils/authentication-utils';
import { SchoolService } from './../../../../services/school.service';
import { UserService } from './../../../../services/user.service';
import { SchoolDetailOverviewComponent } from './components/school-detail-overview/school-detail-overview.component';
import { SchoolDetailRatingComponent } from './components/school-detail-rating/school-detail-rating.component';
import { ConfirmModalComponent } from '../../../../shared/components/modal/confirm-modal/confirm-modal.component';
import { SchoolRequestService } from '../../../../services/school-request.service';
import { SchoolRequestDto } from '../../../../models/requests/school-request-dto';
import { UnenrollSchoolRequestModalComponent } from '../../../../shared/components/modal/unenroll-school-request-modal/unenroll-school-request-modal.component';

@Component({
	selector: 'app-school-detail',
	standalone: true,
	imports: [
		CommonModule,
		RequestCounselingModalComponent,
		SchoolEnrollmentRequestComponent,
		SchoolDetailOverviewComponent,
		SchoolDetailRatingComponent,
		UnenrollSchoolRequestModalComponent,
		FooterComponent,
		RouterLink
	],
	templateUrl: './school-detail.component.html',
	styleUrl: './school-detail.component.scss'
})
export class SchoolDetailComponent implements OnDestroy {
	@ViewChild(RequestCounselingModalComponent) modal!: RequestCounselingModalComponent;
	@ViewChild(SchoolEnrollmentRequestComponent) modal2!: SchoolEnrollmentRequestComponent;
	@ViewChild(UnenrollSchoolRequestModalComponent) unenrollModal!: UnenrollSchoolRequestModalComponent;
	@ViewChild(ConfirmModalComponent) modalConfirm!: ConfirmModalComponent;

	private subscriptions = [] as Subscription[];

	protected schoolDetail = {} as SchoolDTO;
	protected userSchoolData = {} as ExtendedUserDTO;
	protected userSchoolRequest = {} as SchoolRequestDto;
	protected userSchoolRating = {} as RatingDTO;

	protected isUserAuthenticated = false;

	protected formatter = new Intl.NumberFormat('vi-VN', {
		style: 'currency',
		currency: 'VND'
	});

	constructor(
		private userService: UserService,
		private schoolService: SchoolService,
		private schoolRequestService: SchoolRequestService,
		private authenticationUtils: AuthenticationUtils
	) {
		this.getSchoolDetails();
	}

	ngOnDestroy(): void {
		this.subscriptions.forEach((subscription) => subscription.unsubscribe());
	}

	private getSchoolDetails() {
		const sub = this.schoolService.schoolDetail$.subscribe((schoolDetail) => {
			this.schoolDetail = schoolDetail;
			this.getUserInformation();
		});
		this.subscriptions.push(sub);
	}

	private getUserInformation() {
		const currentUserId = this.authenticationUtils.getUserIdFromCookie();
		const currentUserRole = this.authenticationUtils.getRoleFromCookie();

		if (currentUserId && currentUserRole) {
			this.userService.getUserById(currentUserId).subscribe();

			const sub = this.userService.userDetails.subscribe((data) => {
				if (data) {
					this.userSchoolData = data;
					console.log(data);
					this.userSchoolData.schoolId = this.schoolDetail.id;
					this.userSchoolData.userId = currentUserId;
					this.userSchoolData.enrolledSchool = this.schoolDetail.schoolName;
					if (currentUserRole === 'Parents') {
						this.isUserAuthenticated = true;
						if (data.enrollSchoolId) {
							this.schoolRequestService
								.getEnrolledSchoolRequestByEnrolledSchoolId(data.enrollSchoolId)
								.subscribe((responseData) => {
									this.userSchoolRequest = responseData;
								});
						}
					}
				}
			});
			this.subscriptions.push(sub);
		}
	}

	onCreateButtonClick() {
		this.modal2.open();
	}

	openModal() {
		this.modal.open();
	}

	openUnenrollModal() {
		this.unenrollModal.open();
	}

	handleUnEnrollmentSubmit(data: any) {
		console.log('Form submitted: ', data);
	}

	handleModalSubmit(data: any) {
		console.log('Form submitted: ', data);
	}

	handleEnrollmentSubmit(data: any) {
		console.log('Form submitted: ', data);
	}

	handleModalCancel() {
		console.log('Form cancelled');
	}
}
