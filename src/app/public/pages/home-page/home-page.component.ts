import { AsyncPipe, CommonModule, NgOptimizedImage } from '@angular/common';
import { AfterViewInit, Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { INgxSelectOption, NgxSelectModule } from 'ngx-select-ex';
import { Observable } from 'rxjs';
import { District, Province } from '../../../models/locations/location';
import { SchoolService } from '../../../services/school.service';
import { FooterComponent } from '../../../shared/components/footer/footer.component';
import { SearchStore } from './../../../../store/search-store';

@Component({
	selector: 'app-home-page',
	standalone: true,
	imports: [CommonModule, FooterComponent, NgxSelectModule, FormsModule, AsyncPipe, NgOptimizedImage],
	templateUrl: './home-page.component.html',
	styleUrl: './home-page.component.scss'
})
export class HomePageComponent implements AfterViewInit {
	protected cityCode = '';
	protected districtCode = '';

	protected cities = {} as Observable<Province[]>;
	protected districts = {} as Observable<District[]>;

	protected searchInput = {} as HTMLInputElement;

	constructor(private schoolService: SchoolService, private router: Router, private searchStore: SearchStore) {
		schoolService.getAllProvince();

		this.cities = schoolService.provinceList$;
		this.districts = schoolService.districtList$;

		searchStore.setSearchSchoolNameValue('');
		searchStore.setLocationSearchValue({ provinceCode: '', districtCode: '' });
	}

	ngAfterViewInit(): void {
		this.searchInput = document.getElementById('searchSchoolName') as HTMLInputElement;
		this.setInputSearchSchoolNameEvent();
	}

	onCityChange(selectOption: INgxSelectOption[]) {
		const cityCode = selectOption[0].value.toString();

		if (selectOption[0].value !== '') {
			this.schoolService.getAllDistrictByProvinceCode(cityCode);
		}
	}

	private setInputSearchSchoolNameEvent() {
		this.searchInput.addEventListener('keypress', (event) => {
			if (event.key === 'Enter') {
				event.preventDefault();

				const valueToSearch = this.searchInput.value.trim();

				if (valueToSearch) {
					this.searchStore.setSearchSchoolNameValue(valueToSearch);
					this.searchStore.setLocationSearchValue({ provinceCode: '', districtCode: '' });
					this.router.navigateByUrl('/public/school/list-school');
				}
			}
		});
	}

	searchBySchoolNameButton() {
		const valueToSearch = this.searchInput.value.trim();

		if (valueToSearch) {
			this.searchStore.setSearchSchoolNameValue(valueToSearch);
			this.searchStore.setLocationSearchValue({ provinceCode: '', districtCode: '' });
			this.router.navigateByUrl('/public/school/list-school');
		}
	}

	searchByLocationButton() {
		if (this.cityCode) {
			this.searchStore.setSearchSchoolNameValue('');
			this.searchStore.setLocationSearchValue({
				provinceCode: this.cityCode,
				districtCode: this.districtCode
			});
			this.router.navigateByUrl('/public/school/list-school');
		}
	}
}
