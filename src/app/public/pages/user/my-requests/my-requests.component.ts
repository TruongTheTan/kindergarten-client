import { CommonModule } from '@angular/common';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { firstValueFrom, Subscription } from 'rxjs';
import { PageResult } from '../../../../models/page-result';
import {
	SchoolRequestCategoryEnum,
	SchoolRequestDto,
	SchoolRequestStatusEnum
} from '../../../../models/requests/school-request-dto';
import { SchoolRequestService } from '../../../../services/school-request.service';
import { ConfirmModalComponent } from '../../../../shared/components/modal/confirm-modal/confirm-modal.component';
import { EditSchoolRequestComponent } from '../../../../shared/components/modal/edit-school-request/edit-school-request.component';
import { PagingComponent } from '../../../../shared/components/paging/paging.component';
import { RatingComponent } from '../../../../shared/components/rating/rating.component';
import { DateFormatPipe } from '../../../../shared/pipes/date-format-pipe';
import { ApiResponseHandler } from './../../../../api/api-response-handler';
import { FormsModule } from '@angular/forms';

@Component({
	selector: 'app-my-requests',
	standalone: true,
	imports: [
		CommonModule,
		FormsModule,
		DateFormatPipe,
		ConfirmModalComponent,
		RatingComponent,
		PagingComponent,
		EditSchoolRequestComponent
	],
	templateUrl: './my-requests.component.html',
	styleUrls: ['./my-requests.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class MyRequestsComponent implements OnInit {
	title = 'schoolRequestApp';
	public pageResult: PageResult<SchoolRequestDto> = {} as PageResult<SchoolRequestDto>;
	selectedRequestId: string | null = null;
	page: number = 1;
	pageSize: number = 5; // Number of requests per page
	totalRecords: number = 0;
	totalPages: number = 0;
	isShowSelect: boolean = false;

	startIndex: number = 0;
	keyword: string = '';
	sortBy: string = '';
	sortDirection: string = 'desc'; // or 'asc'

	private subscription = new Subscription();
	private schoolRequestDetailSubscription = new Subscription();
	private searchInput!: HTMLInputElement;

	fromDate: string | null = null;
	endDate: string | null = null;

	userSchoolData = {};

	subScription: Subscription = {} as Subscription;
	selectedMail: SchoolRequestDto | null = null;

	@ViewChild(EditSchoolRequestComponent) editModal!: EditSchoolRequestComponent;
	@ViewChild(ConfirmModalComponent) modal!: ConfirmModalComponent;
	formatDateType: 'dd/mm/yyyy' | 'mm/dd/yyyy' = 'mm/dd/yyyy';
	constructor(
		private schoolRequestService: SchoolRequestService,
		private router: Router,
		private apiResponseHandler: ApiResponseHandler,
		private route: ActivatedRoute
	) {}

	ngOnInit(): void {
		this.subScription = this.route.queryParamMap.subscribe((param) => {
			const requestId = param.get('id');
			this.loadRequests();
		});
	}

	ngAfterViewInit(): void {
		this.setEnterEventForInputSearch();
	}

	loadRequests(pageIndex: number = 1) {
		this.schoolRequestService.getMyRequests(
			this.keyword,
			this.sortBy,
			this.sortDirection,
			pageIndex,
			this.pageSize,
			this.fromDate ? this.fromDate : null,
			this.endDate ? this.endDate : null
		);
		this.subscription = this.schoolRequestService.mySchoolRequestList$.subscribe((data) => {
			this.pageResult = data ?? { items: [], pageIndex: 1, pageSize: 5, totalRecords: 0, pageCount: 0 };
			this.startIndex = (this.pageResult!.pageIndex! - 1) * this.pageResult!.pageSize! + 1;

			if (this.pageResult.items && this.pageResult.items.length > 0) {
				this.selectedMail = this.pageResult.items[0]; // Set the first mail as selected by default
			}
		});
	}

	selectMail(request: SchoolRequestDto): void {
		this.selectedMail = request;
	}

	onCreateAtFilterChange(event: Event) {
		this.sortBy = 'createAt';
		this.sortDirection = (event.target as HTMLSelectElement).value;
		this.loadRequests();
	}

	onFromDateChange(event: Event) {
		this.fromDate = (event.target as HTMLInputElement).value;
		this.loadRequests();
	}

	onEndDateChange(event: Event) {
		this.endDate = (event.target as HTMLInputElement).value;
		this.loadRequests();
	}

	getCurrentPage(index: number) {
		this.loadRequests(index);
	}

	openConfirmModal(requestId: string) {
		this.selectedRequestId = requestId;
		this.modal.modalTitle = 'Unenroll School Request';
		this.modal.modalConfirmQuestion = 'Are you sure you want to unenroll school request?';
		this.modal.buttonText = 'Unenroll';
		this.modal.open();
	}

	async openEditModal(requestedId: string) {
		try {
			const response = await firstValueFrom(this.schoolRequestService.getSchoolRequest(requestedId));
			this.userSchoolData = {
				requestedId: requestedId,
				fullName: response.fullName,
				email: response.email,
				mobileNo: response.phone,
				address: response.address,
				dob: response.user?.dob,
				inquiry: response.content,
				enrolledSchool: response.school.schoolName,
				schoolId: response.schoolId,
				userId: response.userId,
				requestCategory: this.getCategoryName(response.schoolRequestCategoryId)
			};
			setTimeout(() => {
				console.log(this.userSchoolData);

				this.editModal.open();
			}, 1500);
		} catch (error: any) {
			this.apiResponseHandler.handleError(error);
		}
	}

	unenrollSchoolRequest(requestId: string | null) {
		if (requestId != null) {
			this.schoolRequestService.unenrollToSchool(requestId).subscribe(
				(response) => {
					console.log('Request resolved', response);
					this.loadRequests();
				},
				(err) => console.error('Error resolving request', err)
			);
		}
	}

	onSearchButtonClick() {
		this.searchInput = document.getElementById('searchInput')! as HTMLInputElement;
		this.keyword = this.searchInput.value;
		this.loadRequests();
	}

	private setEnterEventForInputSearch(): void {
		this.searchInput = document.querySelector('#searchInput') as HTMLInputElement;
		this.searchInput.addEventListener('keypress', (event: KeyboardEvent) => {
			if (event.key === 'Enter') {
				this.onSearchButtonClick();
			}
		});
	}

	getStatusName(statusId: string): string {
		switch (statusId) {
			case SchoolRequestStatusEnum.Pending:
				return 'Pending';
			case SchoolRequestStatusEnum.Approved:
				return 'Approved';
			case SchoolRequestStatusEnum.Rejected:
				return 'Rejected';
			default:
				return 'Unknown';
		}
	}

	getCategoryName(categoryId: string): string {
		switch (categoryId) {
			case SchoolRequestCategoryEnum.Enroll:
				return 'Enroll';
			case SchoolRequestCategoryEnum.UnEnroll:
				return 'Unenroll';
			case SchoolRequestCategoryEnum.Consultant:
				return 'Consultant';
			case SchoolRequestCategoryEnum.Admin:
				return 'Admin';
			default:
				return 'Unknown';
		}
	}

	handleEnrollmentSubmit(data: any) {
		console.log('Form submitted:', data);
		this.loadRequests();
	}
	// Pagination methods
	onPageChange(page: number) {
		this.page = page;
		this.loadRequests();
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
		this.subScription.unsubscribe();
		this.schoolRequestDetailSubscription.unsubscribe();
	}
}
