import { CommonModule } from '@angular/common';
import { Component, Input, OnDestroy, SimpleChanges } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthenticationUtils } from '../../../../../utils/authentication-utils';
import { updateUseRequest } from './../../../../../models/users/update-user-request';
import { UserDto } from './../../../../../models/users/user-dto';
import { UserService } from './../../../../../services/user.service';

@Component({
	selector: 'profile-edit',
	standalone: true,
	imports: [ReactiveFormsModule, CommonModule],
	templateUrl: './edit.component.html',
	styleUrl: './edit.component.scss'
})
export class EditComponent implements OnDestroy {
	@Input() userDto: UserDto = {} as UserDto;
	protected subscription?: Subscription | null;

	editProfileForm = this.formBuilder.group({
		email: [{ value: '', disabled: true }, [Validators.required, Validators.email]],
		fullName: ['', Validators.required],
		phoneNumber: ['', Validators.required],
		address: ['', Validators.required],
		gender: ['', Validators.required]
	});

	constructor(
		private formBuilder: FormBuilder,
		private userService: UserService,
		private authenticationUtils: AuthenticationUtils
	) {}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes['userDto']) {
			this.updateForm(changes['userDto'].currentValue);
		}
	}

	ngOnDestroy(): void {
		this.subscription?.unsubscribe();
	}

	private updateForm(userDto: UserDto): void {
		this.editProfileForm.patchValue({
			email: userDto.email,
			fullName: userDto.fullName,
			phoneNumber: userDto.phoneNumber,
			address: userDto.address,
			gender: userDto.gender.toString()
		});
	}

	onSubmit() {
		if (this.editProfileForm.valid) {
			const { email, fullName, phoneNumber, address, gender } = this.editProfileForm.controls;

			const updateUseRequest: updateUseRequest = {
				id: this.userDto.id,
				fullName: fullName.value ?? '',
				phoneNumber: phoneNumber.value ?? '',
				address: address.value ?? '',
				gender: parseInt(gender.value!) ?? '',
				isActive: true,
				dob: this.userDto.dob?.toString()
			};

			this.subscription = this.userService.editUser(updateUseRequest).subscribe(() => {
				this.authenticationUtils.setFullNameToCookie(updateUseRequest.fullName);
				setTimeout(() => location.reload(), 1000);
			});
		}
	}
}
