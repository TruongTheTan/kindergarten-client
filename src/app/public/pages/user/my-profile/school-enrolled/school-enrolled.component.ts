import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { SchoolDTO } from '../../../../../models/schools/school-dto';
import { UserDto } from '../../../../../models/users/user-dto';
import { SchoolService } from './../../../../../services/school.service';

@Component({
	selector: 'profile-school-enrolled',
	standalone: true,
	imports: [],
	templateUrl: './school-enrolled.component.html',
	styleUrl: './school-enrolled.component.scss'
})
export class SchoolEnrolledComponent implements OnInit {
	@Input() userDto: UserDto = {} as UserDto;
	schoolDto: SchoolDTO = {} as SchoolDTO;
	private subscription: Subscription = new Subscription();

	constructor(private sanitizer: DomSanitizer, private schoolService: SchoolService) {}

	ngOnInit() {
		this.loadSchoolDetails();
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes['userDto'] && changes['userDto'].currentValue !== changes['userDto'].previousValue) {
			this.loadSchoolDetails();
		}
	}

	private loadSchoolDetails() {
		if (this.userDto && this.userDto.enrollSchoolId) {
			this.schoolService.getDetailSchoolById(this.userDto.enrollSchoolId, true);
			this.subscription = this.schoolService.schoolDetail$.subscribe((schoolDetail) => {
				this.schoolDto = schoolDetail;
			});
		} else {
			console.warn('No enrollSchoolId found in userDto');
		}
	}
	imageUrl(imageData: Uint8Array | string): SafeUrl | string {
		if (imageData) {
			return this.sanitizer.bypassSecurityTrustUrl('data:image/png;base64,' + imageData);
		}
		return '';
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}
}
