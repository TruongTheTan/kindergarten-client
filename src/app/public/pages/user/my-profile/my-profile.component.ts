import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { RouterLink, RouterModule } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from '../../../../services/user.service';
import { AuthenticationUtils } from '../../../../utils/authentication-utils';
import { UserDto } from './../../../../models/users/user-dto';
import { AvatarComponent } from './avatar/avatar.component';
import { EditComponent } from './edit-profile/edit.component';
import { SchoolEnrolledComponent } from './school-enrolled/school-enrolled.component';
import { ViewComponent } from './view-profile/view.component';
@Component({
	selector: 'app-my-profile',
	standalone: true,
	imports: [
		CommonModule,
		RouterLink,
		RouterModule,
		AvatarComponent,
		ViewComponent,
		EditComponent,
		SchoolEnrolledComponent
	],
	templateUrl: './my-profile.component.html',
	styleUrl: './my-profile.component.scss'
})
export class MyProfileComponent implements OnInit {
	protected userDto!: UserDto;
	protected subscription = {} as Subscription;
	protected getSubcription = {} as Subscription;
	protected userId: string = this.authenticationUtils.getUserIdFromCookie();
	editComponent: boolean = false;

	constructor(private userService: UserService, private authenticationUtils: AuthenticationUtils) {}

	ngOnInit() {
		if (this.userId) {
			this.getSubcription = this.userService.getUserById(this.userId).subscribe();
			this.subscription = this.userService.userDetails$.subscribe((user) => {
				this.userDto = user;
			});
		}
	}
	toggleButton() {
		this.editComponent = !this.editComponent;
	}
	ngOnDestroy(): void {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
		this.getSubcription.unsubscribe();
	}
}
