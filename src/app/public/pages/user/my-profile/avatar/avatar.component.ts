import { UserDto } from './../../../../../models/users/user-dto';
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
	selector: 'profile-avatar',
	standalone: true,
	imports: [],
	templateUrl: './avatar.component.html',
	styleUrl: './avatar.component.scss',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarComponent {
	@Input() userDto: UserDto = {} as UserDto;
}
