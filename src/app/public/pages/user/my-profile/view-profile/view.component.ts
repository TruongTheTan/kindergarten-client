import { UserDto } from './../../../../../models/users/user-dto';
import { Component, Input } from '@angular/core';

@Component({
	selector: 'profile-view',
	standalone: true,
	imports: [],
	templateUrl: './view.component.html',
	styleUrl: './view.component.scss'
})
export class ViewComponent {
	@Input() userDto: UserDto = {} as UserDto;
}
