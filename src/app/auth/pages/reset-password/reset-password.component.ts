import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication.service';

@Component({
	selector: 'app-reset-password',
	standalone: true,
	imports: [ReactiveFormsModule, CommonModule],
	templateUrl: './reset-password.component.html',
	styleUrl: './reset-password.component.scss'
})
export class ResetPasswordComponent {
	resetPasswordForm = this.formBuilder.group({
		password: ['', Validators.required],
		confirmPassword: ['', Validators.required]
	});

	constructor(
		private formBuilder: FormBuilder,
		private authService: AuthenticationService,
		private route: ActivatedRoute
	) {}

	onSubmit() {
		if (this.resetPasswordForm.invalid) {
			console.log('Invalid form');
			return;
		}
		const { email, token } = this.route.snapshot.queryParams;

		const { password, confirmPassword } = this.resetPasswordForm.controls;
		console.log(email, token, password.value, confirmPassword.value);
		if (password.value !== confirmPassword.value) {
			console.log('Passwords do not match');
			throw new Error('Passwords do not match');
		}
		this.authService.resetPassword(email, token, password.value!, confirmPassword.value!);
	}
}
