import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { AuthenticationService } from '../../../services/authentication.service';

@Component({
	selector: 'app-forgot-password',
	standalone: true,
	imports: [CommonModule, ReactiveFormsModule],
	templateUrl: './forgot-password.component.html',
	styleUrl: './forgot-password.component.scss'
})
export class ForgotPasswordComponent {
	forgotPasswordForm = this.formBuilder.group({
		email: ['', [Validators.required, Validators.email]]
	});

	constructor(private formBuilder: FormBuilder, private authService: AuthenticationService) {}

	onSubmit() {
		if (this.forgotPasswordForm.valid) {
			const { email } = this.forgotPasswordForm.controls;
			this.authService.forgotPassword(email.value!);
		}
	}
}
