import { Component } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { CreateUseRequest } from '../../../models/users/create-user-request';
import { AuthenticationService } from '../../../services/authentication.service';
import { CustomUserFormValidation } from '../../../utils/custom-user-validation';

@Component({
	selector: 'app-register',
	standalone: true,
	imports: [ReactiveFormsModule],
	templateUrl: './register.component.html',
	styleUrl: './register.component.scss'
})
export class RegisterComponent {
	constructor(private authenService: AuthenticationService, private formBuilder: FormBuilder) {}
	registerForm = this.formBuilder.group(
		{
			email: ['', [Validators.required, Validators.email]],
			password: [
				'',
				[
					Validators.required,
					Validators.minLength(6),
					Validators.pattern(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*\W)(?!.* ).{8,16}$/)
				]
			],
			rePassword: ['', Validators.required],
			fullName: ['', Validators.required],
			birthDay: '2001/01/01',
			address: 'VietNam',
			gender: ['1'],
			phone: ['', Validators.required],
			role: null,
			isActive: ['1'],
			image: [{} as File]
		},
		{ validators: [CustomUserFormValidation.checkRepassword()] }
	);

	onSubmitForm() {
		if (this.registerForm.valid) {
			const user = {
				fullName: this.registerForm.controls.fullName.value,
				dob: this.registerForm.controls.birthDay.value!,
				address: this.registerForm.controls.address.value,
				gender: Number.parseInt(this.registerForm.controls.gender.value!),
				email: this.registerForm.controls.email.value,
				phoneNumber: this.registerForm.controls.phone.value,
				password: this.registerForm.controls.password.value,
				roleId: '629de601-95bd-451f-a8d3-86373ee9dded',
				isActive: Boolean(JSON.parse(this.registerForm.controls.isActive.value!)),
				image: this.registerForm.controls.image?.value
			} as CreateUseRequest;

			this.authenService.registerUser(user);
		}
	}

	onCancelButtonClick() {
		window.history.go(-1);
	}

	triggerAllFormError() {
		this.registerForm.valid === false ? this.registerForm.markAllAsTouched() : '';
	}
}
