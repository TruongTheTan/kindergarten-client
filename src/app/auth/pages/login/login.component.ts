import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { AuthenticationService } from '../../../services/authentication.service';
import { RouterModule } from '@angular/router';

@Component({
	selector: 'app-login',
	standalone: true,
	imports: [ReactiveFormsModule, CommonModule, RouterModule],
	templateUrl: './login.component.html',
	styleUrl: './login.component.scss'
})
export class LoginComponent {
	loginForm = this.formBuilder.group({
		email: ['', [Validators.required, Validators.email]],
		password: ['', Validators.required]
	});

	constructor(private formBuilder: FormBuilder, private authService: AuthenticationService) {}

	onSubmit() {
		if (this.loginForm.valid) {
			const { email, password } = this.loginForm.controls;
			this.authService.login(email.value!, password.value!);
		}
	}
}
