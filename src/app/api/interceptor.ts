import { HttpErrorResponse, HttpEvent, HttpInterceptorFn, HttpRequest, HttpResponse } from '@angular/common/http';
import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';
import { throwError } from 'rxjs';
import { catchError, finalize, map } from 'rxjs/operators';
import { environment } from '../../environments/environment.development';
import { LoadingStore } from '../../store/loading-store';
import { AuthenticationUtils } from '../utils/authentication-utils';
import { ApiResponse } from './../models/api-response';
import {
	approveSchoolAPI,
	publishSchoolAPI,
	rejectSchoolAPI,
	submitSchoolAPI,
	unPublishSchoolAPI
} from './api-path';
import { ApiResponseHandler } from './api-response-handler';

let totalRequests = 0;
const apiForMarkAsRead = ['mark-as-read', 'mark-all-as-read'];
const apiForApproving = [approveSchoolAPI, rejectSchoolAPI, publishSchoolAPI, unPublishSchoolAPI, submitSchoolAPI];

const interceptorConfig: HttpInterceptorFn = (request, next) => {
	const router = inject(Router);
	const toast = inject(NgToastService);
	const authenticationUtils = inject(AuthenticationUtils);

	// Return to login if token is expired
	const expirationError = checkTokenIsExpired(router, toast, authenticationUtils);

	if (expirationError) {
		return expirationError;
	}

	const loadingStore = inject(LoadingStore);
	const apiResponseHandler = inject(ApiResponseHandler);

	const modifiedRequest = request.clone({
		url: environment.apiUrl + request.url,
		setHeaders: {
			Authorization: `Bearer ${authenticationUtils.getTokenFromCookie()}`
		}
	});

	totalRequests++;
	loadingStore.showLoading();

	return next(modifiedRequest).pipe(
		map((event) => handleSuccessResponse(event, modifiedRequest, apiResponseHandler)),
		catchError((error: HttpErrorResponse) =>
			handleErrorResponse(toast, error, router, apiResponseHandler, authenticationUtils)
		),
		finalize(() => {
			totalRequests--;
			if (totalRequests === 0) {
				loadingStore.hideLoading();
			}
		})
	);
};

// export const interceptorRefreshToken: HttpInterceptorFn = (request, next) => {
// 	const httpClient = inject(HttpClient);
// 	const authenticationUtils = inject(AuthenticationUtils);
// 	const authenticationService = inject(AuthenticationService);

// 	return next(request).pipe(
// 		catchError((error: HttpErrorResponse) => {
// 			if (error.status === 401) {
// 				const token = authenticationUtils.getTokenFromCookie();

// 				return httpClient
// 					.post('authentication/refresh-token', { token: token }, { withCredentials: true })
// 					.pipe(
// 						switchMap((response: ApiResponse<TokenDto>) => {
// 							const newToken = response.data!.token;
// 							authenticationUtils.setTokenToCookie(newToken);

// 							const newRequest = request.clone({
// 								setHeaders: {
// 									Authorization: `Bearer ${newToken}`
// 								}
// 							});

// 							return next(newRequest);
// 						}),

// 						catchError((err) => {
// 							authenticationService.logout();
// 							return throwError(() => err);
// 						})
// 					);
// 			}
// 			return throwError(() => error);
// 		})
// 	);
// };

function checkTokenIsExpired(router: Router, toast: NgToastService, authenticationUtils: AuthenticationUtils) {
	if (authenticationUtils.checkTokenIsExpired()) {
		toast.warning({
			detail: 'Session is expired',
			summary: 'Your session has expired, please login to continue',
			duration: 3000
		});

		authenticationUtils.removeAllCookie();
		setTimeout(() => router.navigateByUrl('/auth/login'), 1500);
		return throwError(() => {});
	}
	return null;
}

function handleSuccessResponse(
	event: HttpEvent<any>,
	request: HttpRequest<any>,
	apiResponseHandler: ApiResponseHandler
) {
	if (event instanceof HttpResponse) {
		const url = request.url;
		const match = url.match(/\/api\/([^?]+)/);
		const result = match ? match[1] : '';

		const isApproveRequest = apiForApproving.includes(result.slice(0, result.length - 1));
		const isMarkAsReadRequest = apiForMarkAsRead.some((str) => url.includes(str));
		const isRemoveToastMessage = (request.method !== 'GET' || isApproveRequest) && !isMarkAsReadRequest;
		const isSuccessResponse = event.status >= 200 && event.status <= 299;

		if (isSuccessResponse && isRemoveToastMessage) {
			apiResponseHandler.handleSuccess(event.body as ApiResponse<unknown>);
		}
	}
	return event;
}

function handleErrorResponse(
	toast: NgToastService,
	error: HttpErrorResponse,
	router: Router,
	apiResponseHandler: ApiResponseHandler,
	authenticationUtils: AuthenticationUtils
) {
	const statusCode = error.status;

	if (statusCode === 401 || statusCode === 403) {
		toast.warning({ detail: 'Un-authorized', summary: 'You are not allow to continue.' });
		setTimeout(() => router.navigateByUrl('/auth/login'), 1500);

		if (statusCode === 401) {
			authenticationUtils.removeAllCookie();
		}
	} else {
		apiResponseHandler.handleError(error);
	}
	return throwError(() => error);
}

export default interceptorConfig;
