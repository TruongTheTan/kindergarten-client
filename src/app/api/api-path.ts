// User APIs below
const userControllerName = 'users';

export const listUserAPI = `${userControllerName}/get-all`;
export const createUserAPI = `${userControllerName}/create`;
export const updateUserAPI = `${userControllerName}/update`;
export const detailsUserAPI = `${userControllerName}/details`;
export const deleteUserAPI = `${userControllerName}/delete`;
export const pagingUserAPI = `${userControllerName}/paging`;
export const changeStatusUserAPI = `${userControllerName}/change-status`;
export const assignRoleAPI = `${userControllerName}/assign-role`;
export const changeImageUserAPI = `${userControllerName}/change-image`;
export const getParentAPI = `${userControllerName}/get-parent`;
export const verifyEmail = `${userControllerName}/verify-email`;
// Role APIS below
const roleControllerName = 'roles';
export const roleAPIS = `${roleControllerName}/get-all`;
// School APIs below
const schoolControllerName = 'schools';
export const schoolListAPI = `${schoolControllerName}/get-all`;
export const schoolDetailAPI = `${schoolControllerName}/details`;
export const createSchoolAPI = `${schoolControllerName}/create`;
export const updateSchoolAPI = `${schoolControllerName}/update`;
export const approveSchoolAPI = `${schoolControllerName}/approve`;
export const deleteSchoolAPI = `${schoolControllerName}/delete`;
export const schoolRemindersAPI = `${schoolControllerName}/reminders`;
export const rejectSchoolAPI = `${schoolControllerName}/reject`;
export const publishSchoolAPI = `${schoolControllerName}/publish`;
export const unPublishSchoolAPI = `${schoolControllerName}/unpublish`;
export const submitSchoolAPI = `${schoolControllerName}/submit`;
export const publicListSchool = `${schoolControllerName}/public/get-all`;

// Request APIs below
const requestsControllerName = `requests`;
export const schoolEnrollmentRequestsEndpointApi = `${requestsControllerName}/school-enrollment-requests`;
export const schoolCounselingRequestsEndpointApi = `${requestsControllerName}/counseling-requests`;
export const schoolRequestDetailEndpointApi = `${requestsControllerName}/school-request`;
export const myRequestsEndpointApi = `${requestsControllerName}/my-requests`;
export const enrollSchoolRequestsEndpointApi = `${requestsControllerName}/school-enrollment-request`;
export const unenrollSchoolRequestsEndpointApi = `${requestsControllerName}/school-unenrollment-request`;
export const counselingRequestsEndpointApi = `${requestsControllerName}/counseling-request`;
export const resolveSchoolRequestEndpointApi = `${requestsControllerName}/school-resolve-request`;
export const getSchoolEnrollmentRequestByEnrolledSchoolIdApi = `${requestsControllerName}/school-approved-enrollment-request`;
export const createUnenrollRequestApi = `${requestsControllerName}/school-unenrollment-request`;
// Rating APIs below
const ratingSchoolControllerName = 'ratings';
export const getRatingSchoolAPI = `${ratingSchoolControllerName}/get-school-rating`;
// Authentication APIs below
const authenticationControllerName = 'authentication';
export const loginAPI = `${authenticationControllerName}/login`;
export const resetPasswordAPI = `${authenticationControllerName}/reset-password`;
export const forgotPasswordAPI = `${authenticationControllerName}/forget-password`;
export const refreshTokenAPI = `${authenticationControllerName}/refresh-token`;
export const logoutAPI = `${authenticationControllerName}/logout`;
export const registerAPI = `${authenticationControllerName}/register`;

// Location API below
export const getListProvinceAPI = `locations/province/list`;
export const getListDistrictAPI = `locations/district/list`;
export const getListWardAPI = `locations/ward/list`;

// Rating API below
export const createSchoolRatingAPI = `ratings/create`;
