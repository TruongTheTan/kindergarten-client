import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from '../models/api-response';
import { TokenDto } from '../models/tokens/token-dto';
import {
	forgotPasswordAPI,
	loginAPI,
	logoutAPI,
	refreshTokenAPI,
	registerAPI,
	resetPasswordAPI,
	verifyEmail
} from './api-path';

@Injectable({
	providedIn: 'root'
})
export class AuthenticationApi {
	constructor(private http: HttpClient) {}

	login(email: string, password: string) {
		return this.http.post<ApiResponse<TokenDto>>(
			loginAPI,
			{
				email,
				password
			},
			{ withCredentials: true }
		);
	}

	refreshToken(token: string) {
		return this.http.post<ApiResponse<TokenDto>>(refreshTokenAPI, { token }, { withCredentials: true });
	}

	logout() {
		return this.http.post<ApiResponse<any>>(logoutAPI, {}, { withCredentials: true });
	}

	forgotPassword(email: string) {
		return this.http.post<ApiResponse<any>>(forgotPasswordAPI, { email }, { withCredentials: true });
	}

	resetPassword(email: string, token: string, newPassword: string, confirmPassword: string) {
		return this.http.post<ApiResponse<any>>(
			resetPasswordAPI,
			{ email, token, newPassword, confirmPassword },
			{ withCredentials: true }
		);
	}

	registerUser(user: FormData) {
		return this.http.post<ApiResponse<unknown>>(registerAPI, user);
	}
	confirmEmail(UserId: string, code: string) {
		return this.http.get<ApiResponse<boolean>>(verifyEmail + `?userId=${UserId}&code=${code}`);
	}
}
