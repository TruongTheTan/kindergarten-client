import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from '../models/api-response';
import { District, Province, Ward } from '../models/locations/location';
import { PageResult } from '../models/page-result';
import { CreateSchoolRating } from '../models/schools/create-school-rating';
import { PublicSchoolDTO } from '../models/schools/public-school-dto';
import { SchoolDTO } from '../models/schools/school-dto';
import {
	approveSchoolAPI,
	createSchoolAPI,
	createSchoolRatingAPI,
	deleteSchoolAPI,
	getListDistrictAPI,
	getListProvinceAPI,
	getListWardAPI,
	publicListSchool,
	publishSchoolAPI,
	rejectSchoolAPI,
	schoolDetailAPI,
	schoolListAPI,
	schoolRemindersAPI,
	submitSchoolAPI,
	unPublishSchoolAPI,
	updateSchoolAPI
} from './api-path';

@Injectable({
	providedIn: 'root'
})
export class SchoolApi {
	constructor(private http: HttpClient) {}

	getSchoolPublic(formData: FormData) {
		return this.http.post<ApiResponse<PageResult<PublicSchoolDTO>>>(publicListSchool, formData);
	}

	getAllSchool(schoolName: string, size: number, page: number) {
		const apiEndpoint = `${schoolListAPI}?schoolName=${schoolName}&size=${size}&page=${page}`;
		return this.http.get<ApiResponse<PageResult<SchoolDTO>>>(apiEndpoint);
	}

	deleteSchool(schoolId: string) {
		return this.http.delete<ApiResponse<boolean>>(`${deleteSchoolAPI}?id=${schoolId}`);
	}

	createSchool(formData: FormData) {
		return this.http.post<ApiResponse<boolean>>(createSchoolAPI, formData);
	}

	getAllProvince() {
		return this.http.get<ApiResponse<Province[]>>(getListProvinceAPI);
	}

	getAllDistrictByProvinceCode(provinceCode: string) {
		return this.http.get<ApiResponse<District[]>>(`${getListDistrictAPI}?provinceCode=${provinceCode}`);
	}

	getAllWardByDistrictCode(districtCode: string) {
		return this.http.get<ApiResponse<Ward[]>>(`${getListWardAPI}?districtCode=${districtCode}`);
	}

	getDetailSchoolById(id: string, canLoadImages: boolean) {
		return this.http.get<ApiResponse<SchoolDTO>>(
			`${schoolDetailAPI}?schoolId=${id}&canLoadImages=${canLoadImages}`
		);
	}

	editSchool(formData: FormData) {
		return this.http.put<ApiResponse<boolean>>(updateSchoolAPI, formData);
	}

	createSchoolRating(createSchoolRating: CreateSchoolRating) {
		return this.http.post<ApiResponse<boolean>>(createSchoolRatingAPI, createSchoolRating);
	}

	getSchoolReminderList(schoolName: string, size: number, page: number) {
		const apiEndpoint = `${schoolRemindersAPI}?schoolName=${schoolName}&size=${size}&page=${page}`;
		return this.http.get<ApiResponse<PageResult<SchoolDTO>>>(apiEndpoint);
	}

	approveSchool(schoolId: string) {
		return this.http.get<ApiResponse<PageResult<SchoolDTO>>>(`${approveSchoolAPI}/?schoolId=` + schoolId);
	}

	rejectSchool(schoolId: string) {
		return this.http.get<ApiResponse<PageResult<SchoolDTO>>>(`${rejectSchoolAPI}/?schoolId=` + schoolId);
	}

	publishSchool(schoolId: string) {
		return this.http.get<ApiResponse<PageResult<SchoolDTO>>>(`${publishSchoolAPI}/?schoolId=` + schoolId);
	}

	unPublishSchool(schoolId: string) {
		return this.http.get<ApiResponse<PageResult<SchoolDTO>>>(`${unPublishSchoolAPI}/?schoolId=` + schoolId);
	}

	submitSchool(schoolId: string) {
		return this.http.get<ApiResponse<PageResult<SchoolDTO>>>(`${submitSchoolAPI}/?schoolId=` + schoolId);
	}
}
