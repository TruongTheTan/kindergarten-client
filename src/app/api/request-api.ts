import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api-response';
import { PageResult } from '../models/page-result';
import { ICreateSchoolRequestDto, ICreateUnenrollRequestDto, SchoolRequestDto } from '../models/requests/school-request-dto';
import {
	counselingRequestsEndpointApi,
	enrollSchoolRequestsEndpointApi,
	getSchoolEnrollmentRequestByEnrolledSchoolIdApi,
	myRequestsEndpointApi,
	resolveSchoolRequestEndpointApi,
	schoolCounselingRequestsEndpointApi,
	schoolEnrollmentRequestsEndpointApi,
	schoolRequestDetailEndpointApi,
	unenrollSchoolRequestsEndpointApi
} from './api-path';

@Injectable({
	providedIn: 'root'
})
export class RequestApi {
	constructor(private http: HttpClient) {}

	getEnrollmentSchoolRequests(
		query: string,
		category: string,
		status: string,
		sortBy: string,
		sortDirection: string,
		fromDate: string | null,
		endDate: string | null,
		page: number,
		size: number
	) {
		let url = `${schoolEnrollmentRequestsEndpointApi}?Query=${query}&SchoolRequestCategory=${category}&SchoolRequestStatus=${status}&SortBy=${sortBy}&SortDirection=${sortDirection}&PageIndex=${page}&PageSize=${size}`;

		if (fromDate) {
			url += `&FromDate=${fromDate}`;
		}

		if (endDate) {
			url += `&EndDate=${endDate}`;
		}

		return this.http.get<ApiResponse<PageResult<SchoolRequestDto>>>(url);
	}

	getCounselingSchoolRequests(
		query: string,
		category: string,
		status: string,
		sortBy: string,
		sortDirection: string,
		page: number,
		size: number
	) {
		return this.http.get<ApiResponse<PageResult<SchoolRequestDto>>>(
			`${schoolCounselingRequestsEndpointApi}?Query=${query}&SchoolRequestCategory=${category}&SchoolRequestStatus=${status}&SortBy=${sortBy}&SortDirection=${sortDirection}&PageIndex=${page}&PageSize=${size}`
		);
	}

	getSchoolRequest(requestId: string): Observable<ApiResponse<SchoolRequestDto>> {
		return this.http.get<ApiResponse<SchoolRequestDto>>(`${schoolRequestDetailEndpointApi}/${requestId}`);
	}

	getMyRequest(
		query: string,
		sortBy: string,
		sortDirection: string,
		page: number,
		size: number,
		fromDate: string | null,
		endDate: string | null
	) {
		let url = `${myRequestsEndpointApi}?Query=${query}&SortBy=${sortBy}&SortDirection=${sortDirection}&PageIndex=${page}&PageSize=${size}`;

		if (fromDate) {
			url += `&FromDate=${fromDate}`;
		}

		if (endDate) {
			url += `&EndDate=${endDate}`;
		}

		return this.http.get<ApiResponse<PageResult<SchoolRequestDto>>>(url);
	}

	getApprovedSchoolRequestByEnrolledSchoolId(
		enrolledSchoolId: string
	): Observable<ApiResponse<SchoolRequestDto>> {
		return this.http.get<ApiResponse<SchoolRequestDto>>(
			`${getSchoolEnrollmentRequestByEnrolledSchoolIdApi}/${enrolledSchoolId}`
		);
	}

	createCounselingRequest(body: ICreateSchoolRequestDto) {
		return this.http.post<ApiResponse<boolean>>(counselingRequestsEndpointApi, body);
	}

	editCounselingRequests(requestId: string, body: ICreateSchoolRequestDto) {
		return this.http.put<ApiResponse<boolean>>(`${counselingRequestsEndpointApi}/${requestId}`, body);
	}

	editEnrollToSchoolRequest(requestId: string, body: ICreateSchoolRequestDto) {
		return this.http.put<ApiResponse<boolean>>(`${enrollSchoolRequestsEndpointApi}/${requestId}`, body);
	}

	enrollToSchool(body: ICreateSchoolRequestDto) {
		return this.http.post<ApiResponse<boolean>>(enrollSchoolRequestsEndpointApi, body);
	}

	createUnenrollSchoolRequest(body: ICreateUnenrollRequestDto) {
		return this.http.post<ApiResponse<boolean>>(unenrollSchoolRequestsEndpointApi, body);
	}

	unenrollToSchool(requestedId: string) {
		return this.http.patch<ApiResponse<boolean>>(`${unenrollSchoolRequestsEndpointApi}/${requestedId}`, {});
	}

	resolveSchoolRequest(requestId: string, response: string, isApprove: boolean) {
		const body = { response, isApprove };
		return this.http.put<ApiResponse<any>>(`${resolveSchoolRequestEndpointApi}/${requestId}`, body);
	}

	deleteCounselingRequests(ids: string[]) {
		const options = {
			body: ids
		};
		return this.http.delete<ApiResponse<any>>(`${counselingRequestsEndpointApi}`, options);
	}
}
