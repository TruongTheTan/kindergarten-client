import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { getRatingSchoolAPI } from './api-path';
import { ApiResponse } from '../models/api-response';
import { RatingDTO } from '../models/ratings/rating-dto';

@Injectable({
	providedIn: 'root'
})
export class RatingApi {
	constructor(private http: HttpClient) {}
	getRatingSchool(schoolId: string) {
		return this.http.get<ApiResponse<RatingDTO[]>>(`
			${getRatingSchoolAPI}?schoolId=${schoolId}`);
	}
}
