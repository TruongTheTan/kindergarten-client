import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api-response';
import { PageResult } from '../models/page-result';
import { Role } from '../models/users/role';
import { UserDto } from '../models/users/user-dto';
import {
	assignRoleAPI,
	changeStatusUserAPI,
	createUserAPI,
	deleteUserAPI,
	detailsUserAPI,
	getParentAPI,
	pagingUserAPI,
	roleAPIS,
	updateUserAPI
} from './api-path';

@Injectable({
	providedIn: 'root'
})
export class UserApi {
	constructor(private http: HttpClient) {}
	changeStatus(UserId: string, status: boolean) {
		return this.http.patch<ApiResponse<boolean>>(
			changeStatusUserAPI + `?UserId=${UserId}&IsActive=${status}`,
			''
		);
	}
	createUser(user: FormData): Observable<ApiResponse<unknown>> {
		return this.http.post<ApiResponse<unknown>>(createUserAPI, user);
	}

	deleteUser(userId: string) {
		return this.http.delete<ApiResponse<boolean>>(deleteUserAPI + `/${userId}`);
	}

	updateUser(userId: string, user: FormData) {
		return this.http.put<ApiResponse<boolean>>(updateUserAPI + `/${userId}`, user);
	}

	getUserById(UserId: string) {
		return this.http.get<ApiResponse<UserDto>>(detailsUserAPI + `/${UserId}`);
	}

	assignRole(UserId: string, RoleId: string) {
		return this.http.put<ApiResponse<boolean>>(assignRoleAPI + `?UserId=${UserId}&RoleId=${RoleId}`, '');
	}

	getUserPaging(keyword: string, indexPage: number, size: number, roleId: string | null) {
		if (roleId == null) {
			return this.http.get<ApiResponse<PageResult<UserDto>>>(
				`${pagingUserAPI}?Keyword=${keyword}&PageSize=${size}&PageIndex=${indexPage}`
			);
		} else {
			return this.http.get<ApiResponse<PageResult<UserDto>>>(
				`${pagingUserAPI}?Keyword=${keyword}&PageSize=${size}&PageIndex=${indexPage}&RoleId=${roleId}`
			);
		}
	}

	getAllRole() {
		return this.http.get<ApiResponse<Role[]>>(roleAPIS);
	}

	getParent(keyword: string, indexPage: number, size: number, roleId: string | null) {
		if (roleId == null) {
			return this.http.get<ApiResponse<PageResult<UserDto>>>(
				`${getParentAPI}?Keyword=${keyword}&PageSize=${size}&PageIndex=${indexPage}`
			);
		} else {
			return this.http.get<ApiResponse<PageResult<UserDto>>>(
				`${getParentAPI}?Keyword=${keyword}&PageSize=${size}&PageIndex=${indexPage}&RoleId=${roleId}`
			);
		}
	}
}
