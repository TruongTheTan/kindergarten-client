import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';
import { ApiResponse } from '../models/api-response';

@Injectable({
	providedIn: 'root'
})
export class ApiResponseHandler {
	constructor(private toast: NgToastService) {}

	handleSuccess(apiResponse: ApiResponse<unknown>) {
		if (apiResponse.message) {
			this.toast.success({
				detail: `Success`,
				summary: apiResponse.message,
				duration: 3000
			});
		}
	}

	handleError(error: HttpErrorResponse) {
		console.log(error);
		const errorObject = error.error;

		let message = '';
		let statusCode = 0;

		if (errorObject instanceof ProgressEvent) {
			message = error.statusText;
			statusCode = error.status > 0 ? error.status : 500;
		} else if (this.isApiResponseInterface(errorObject)) {
			const apiResponse = errorObject as ApiResponse<unknown>;
			message = apiResponse.message!;
			statusCode = apiResponse.statusCode!;
		} else {
			const apiResponse = errorObject as ApiResponse<unknown>;
			message = (apiResponse as any).Message;
			statusCode = (apiResponse as any).StatusCode;
		}

		this.toast.error({
			detail: `Fail`,
			summary: message,
			duration: 3500
		});
	}

	private isApiResponseInterface(obj: any): obj is ApiResponse<any> {
		const apiResponseKeys = ['data', 'isSuccess', 'message', 'statusCode'] as (keyof ApiResponse<any>)[];
		return Object.keys(obj).every((key) => apiResponseKeys.includes(key as any));
	}
}
